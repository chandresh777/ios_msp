//
//  GlobalConstant.swift
//  MSP-eCommerce
//
//  Created by msp on 25/07/18.
//  Copyright © 2018 msp. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import Alamofire
import SwiftyUserDefaults

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.

//SCREEN SIZEs
let screenSize: CGRect = UIScreen.main.bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height

let NOTIFICATIONTIMER : CGFloat = 3.0
let REMOTENOTIFICATIONKEY = "UIApplicationLaunchOptionsRemoteNotificationKey"
var Gifurlads = "http://202.131.107.107/vr/CMA/assets/uploads/sponsor/1540897041_87sponsor1.png"
var GifurladsRedirect = ""
let device_type = "2"


//Set Drawer Width
let DRAWERWIDTH = screenWidth * 0.85

let imgPlaceHolder = UIImage(named: "imgPlaceHolder")

var isPostInGetParams = false

@available(iOS 11.0, *)
let bottomBarOfIPhone = UIApplication.shared.keyWindow!.safeAreaInsets.bottom == 0 ? 5 : UIApplication.shared.keyWindow!.safeAreaInsets.bottom

@available(iOS 11.0, *)
let topBarOfIPhone = UIApplication.shared.keyWindow!.safeAreaInsets.top

/*
 FONTs
 FontName :
 FontName :
 FontName :
 */
struct colorset {
    
    static var themeColor = UIColor.init(red: 51.0/255.0, green: 109.0/255.0, blue: 160.0/255.0, alpha: 1.0)
    static var backroundOfView = UIColor.white

    static var statusBarColor = UIColor.init(hexColorString: "#CCCECF")
    static var navigationBar = UIColor.init(hexColorString: "#F2F2F2")

    static var whitetextColor = UIColor.white
    static var blacktextcolor = UIColor.black
    static var lightBlackTextColor = UIColor.init(hexColorString: "#484848")
    
    static var imgTintColor = UIColor.black
    
    static var btnThemeColor = UIColor.black
    
    static var sepretorColor = UIColor.lightGray.withAlphaComponent(0.3)
    
    static var textFieldLine = UIColor.lightGray

    static var drawerBackground = UIColor.init(red: 0.0/255.0, green: 123.0/255.0, blue: 184/255.0, alpha: 1.0)

    static var buttonBGcolor = UIColor.init(hexColorString: "5248FB")
    
//    //Theme color
//    static var lightBlue = UIColor.init(red: 172.0/255.0, green: 204.0/255.0, blue: 224.0/255.0, alpha: 1.0)
//    static var darkBlue = UIColor.init(red: 51.0/255.0, green: 109.0/255.0, blue: 160.0/255.0, alpha: 1.0)
//
//    //Dummy
//    //static var lightBlue = UIColor.init(hexColorString: "#EF5350")
//    //static var darkBlue = UIColor.init(hexColorString: "#B71c1c")
//    ////
//
//    static var navigationColor = UIColor.white
//
//    //Dummy
////    static var navigationColor = UIColor.init(hexColorString: "#B71C1C")
//
//    //Image view backround color
//    static var themeImgBackround = colorset.darkBlue
//
//    //Backround color of view
//    static var backroundOfView = UIColor.white
//    static var commonBackroundView = UIColor.init(hexColorString: "#f2f2f2")
//    static var backroundOfDataView = UIColor.white
//
//    //Dummy
////    static var backroundOfView = UIColor.init(hexColorString: "#FFEBEE")
////    static var commonBackroundView = UIColor.init(hexColorString: "#FFCDD2")
////    static var backroundOfDataView = UIColor.init(hexColorString: "#FFEBEE")
//    /////
//
//    //text color
//    static let textBlackColor = UIColor.init(hexColorString: "#000000")
//    static var textGrayColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)
//    static var lightBG = UIColor.init(red: 232.0/255.0, green: 232.0/255.0, blue: 232.0/255.0, alpha: 1.0)
//
//    //Use in some place (Rare case)
//    static var redTextColor = UIColor.init(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
//    static var BluetxtColor = colorset.darkBlue
//    static var txtGreen = UIColor.init(red: 74.0/255.0, green: 182.0/255.0, blue: 83.0/255.0, alpha: 1.0)
//    static var txtWhiteColor = UIColor.white
//
//    //Border color
//    static var borderColor = UIColor.init(red: 183.0/255.0, green: 183.0/255.0, blue: 183.0/255.0, alpha: 1.0)
//    static var borderStarColor = UIColor.init(red: 246.0/255.0, green: 203.0/255.0, blue: 94.0/255.0, alpha: 1.0)
//
//
//    //Table sepretor color
//    static var tblSepretorColor = UIColor.init(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
//
//    //Category tree color
//    static var afterExpandTree = UIColor.init(red: 194.0/255.0, green: 209.0/255.0, blue: 225.0/255.0, alpha: 1.0)
//    static var secondLevelTree = UIColor.init(red: 214.0/255.0, green: 224.0/255.0, blue: 235.0/255.0, alpha: 1.0)

}

struct imagePickerType {
    static let Gallery = "Gallery"
    static let Camera = "Camera"
}

struct FONT
{
    static let Regular = "RobotoCondensed-Regular"
    static let Bold = "RobotoCondensed-Bold"
}

struct FONT_SIZE
{
    static let EIGHTEEN : CGFloat = 18.0
    static let SEVENTEEN : CGFloat = 18.0
    static let SIXTEEN : CGFloat =  16.0
    static let FIFTEEN : CGFloat =  15.0
    static let THIRTEEN : CGFloat =  13.0
    static let FOURTEEN : CGFloat =  14.0
    static let TWELVE : CGFloat = 12.0
    static let ELEVEN : CGFloat = 11.0
    static let TEN : CGFloat =  10.0
    static let NINE : CGFloat =  9.0
    static let EIGHT : CGFloat =  8.0
    static let SEVEN : CGFloat =  7.0
    static let TWENTYFOUR : CGFloat =  24
    static let THIRTYSIX : CGFloat =  36
    static let EIGTY : CGFloat =  80
}

struct DEIVCETYPE
{
    static let FOURINCH : CGFloat = 320.0 // 4 inch screen display
}

/**
 * Time Ago Strings
 */
struct DateInTimeAgo {
    
    struct YEAR
    {
        static let YEARSAGO = "years ago"
        static let ONEYEARAGO = "1 year ago"
        static let LASTYEAR = "Last year"
    }
    struct MONTH
    {
        static let MONTHSAGO = "months ago"
        static let ONEMONTHAGO = "1 month ago"
        static let LASTMONTH = "Last month"
    }
    struct DAYS
    {
        static let DAYSAGO = "days ago"
        static let ONEDAYAGO = "1 day ago"
        static let YEASTERDAY = "Yesterday"
    }
    struct WEEKS
    {
        static let WEEKSAGO = "weeks ago"
        static let ONEWEEKAGO = "1 week ago"
        static let LASTWEEK = "Last week"
    }
    struct HOURS
    {
        static let HOURSAGO = "hours ago"
        static let ONEHOURAGO = "1 hour ago"
        static let LASTHOUR = "An hour ago"
    }
    struct MINUTS
    {
        static let MINUTSAGO = "minutes ago"
        static let ONEMINUTSAGO = "1 minute ago"
        static let LASTMINUT = "A minute ago"
    }
    struct SECONDS
    {
        static let SECONDSAGO = "seconds ago"
        static let JUSTNOW = "Just now"
    }
}

/**
 * Convert NSDATA TO JSON
 */
func NSDATATOJSON(data: NSData) -> Any? {
    do {
        return try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.allowFragments)
    } catch let myJSONError {
        debugPrint(myJSONError)
    }
    return nil
}

/**
 *   ERROR Code with Msgs
 */
struct ERROR
{
    struct errorCODE
    {
        static let noInternet = -1009 // internet issue
        static let serverIssue = 4  // server issue
        static let timeOut = -1001 //Time out
        static let CouldNotConnectToServer = -1004 //Time out
        
    }
    
    struct errorMSG
    {
        //errorMsgInternetConnUnavailable
        //static let noInterTconnection = Translation.getTranslationName(MsgTranslation.errorMsgInternetConnUnavailable)
        static let noInterTconnection = "Internet is not available"
        static let serverIssue = "Oops, Something went wrong, Please try again Later"
        static let timeOut = "Oops, Something went wrong, Please try again Later"
    }
    
    
    
    
}
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool
{
    switch (lhs, rhs)
    {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}


// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool
{
    switch (lhs, rhs)
    {
    case let (l?, r?):
        return l >= r
    default:
        return !(lhs < rhs)
    }
}

/*!
 * MARK: StoryBoard
 * Constant to identify StoryBoard
 * mainStoryBoard: to get app's Main storyboard
 */
struct StoryBoard
{
    static let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
    static let detailStoryBoard = UIStoryboard(name: "Detail", bundle: nil)
}


/*!
 * MARK: AppName
 * Constant to identify AppName
 * appName: to get app Name
 */
struct AppName
{
    static let appName = "Conference"
}
/**
 - Special characters
 */
struct SPECIALCHARACTERS {
    
    static let EQUAL = "="
    static let SLASH_N = "\n"
}

/*!
 * MARK: Controller
 * Constant to identify viewcontroller
 */

struct FORMAT
{
    static let dateFormat : String! = "MM/dd/yyyy HH:MM"
}

let nullInterval : Double = 1234567890 // to check null interval


/**
 * MARK: STATIC Texts
 */
struct STATIC_TEXTS
{
    static let versionText = "v"
    static let txtRetry = "Retry"
    //static let txtCancel = "Cancel"
}


/**
 * General messages
 */
struct GENERAL_MSG
{
}

/**
 *  XIB names // UIFiles
 */
struct XibName
{
    
}

/**
 *  Reusable call
 */
struct REUSERCELL {
    
    static let cell = "cell"
}

func version() -> String {
    let dictionary = Bundle.main.infoDictionary!
    let version = dictionary["CFBundleShortVersionString"] as! String
    let build = dictionary["CFBundleVersion"] as! String
    return "\(version) build \(build)"
}

/**
 * Device INFO (DEVICE_TYPE,DEVICE_TOKEN,DEVICE_DETAILS)
 */
struct DEVICE_INFO
{
    static let DEVICE_TYPE = 1 //for iPhone = 1, Android = 2
    static var DEVICE_TOKEN = ""
    static let DEVICE_DETAILS = UIDevice.current.model+","+UIDevice.current.systemVersion+","+UIDevice.current.systemName
    
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
   
    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)

    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )

    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
}
/**
 * App INFO (APP_VERSION,APP_BUILD_NO)
 */
struct APP_INFO
{
    static let version: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
    static let buildNumber: AnyObject? = Bundle.main.infoDictionary!["CFBundleVersion"] as AnyObject?
    
    static let APP_VERSION = version as! String
    static let APP_BUILD_NO = buildNumber as! String
}
/**
 * API Request paramter
 */
struct APIsRequestParams
{
    //
    //    static let DEVICETYPE = 1 //for iPhone
    
    /**
     * MARK: APIVersionCheckParamKey
     * keys of CheckAppVersion api parameters
     * appVersion: for app version
     * deviceType: for device Type (Android Or iOS)
     * deviceToken: for deviceToken
     * messageUpdateDate: for date of Changes in Messages of Api
     * customerId: for Id of Customer
     * deviceDetails: details of device in Which app runs
     */
    
    struct APPVersion {
        static let type : String = "type"
    }
    
    struct LoginResponse {
        static let mobile_no : String = "mobile_no"
        static let deviceId : String = "deviceId"
        static let pushtoken : String = "pushtoken"
        static let device_type : String = "device_type"
        static let otp_number : String = "otp_number"
    }
      struct updateProfile {
        
        
        
        static let user_id : String = "user_id"
        static let specialize_id : String = "specialize_id"
        static let category_id : String = "category_id"
        static let email : String = "email"
        static let gender : String = "gender"
        static let mobile_no : String = "mobile_no"
        static let address : String = "address"
        static let last_name : String = "last_name"
        static let first_name : String = "first_name"
        static let profile_image : String = "profile_image"
         static let weight : String = "weight"
         static let height : String = "height"
        
    }
    
   
    
    struct getLinkAdsSponsorParam{
        static let user_id : String = "user_id"
        static let sponsor_id : String = "sponsor_id"
        static let conference_id : String = "conference_id"
        static let is_flag : String = "is_flag"
       
    
    }

    struct updateStepParam{
        static let user_id : String = "user_id"
        static let steps : String = "steps"
        static let conference_id : String = "conference_id"
        static let calorie : String = "calorie"
        
        
    }
    struct getconferenceDateParam {
       
        static let conference_id : String = "conference_id"
    }
    struct getsessionDetail {
        
        static let conference_id : String = "conference_id"
         static let session_id : String = "session_id"
    }
    
    struct setBookMarkParamsession {
        static let conference_id : String = "conference_id"
        static let session_id : String = "session_id"
         static let user_id : String = "user_id"
         static let is_like : String = "is_like"
   
    }
    struct getsessionList {
        static let conference_id : String = "conference_id"
        static let date : String = "date"
       
        
    }
  
    struct getSpeakerDeatil {
        static let conference_id : String = "conference_id"
        static let user_id : String = "user_id"
         static let speaker_id : String = "speaker_id"
        
    }
    
    struct setBookMarkParamsSpeaker {
        static let conference_id : String = "conference_id"
        static let speaker_id : String = "speaker_id"
        static let user_id : String = "user_id"
        static let is_like : String = "is_like"
        
    }
    
    struct setnoteSpeaker {
        static let conference_id : String = "conference_id"
        static let speaker_id : String = "speaker_id"
        static let user_id : String = "user_id"
        static let note_id : String = "note_id"
        static let note : String = "note"
        
        
    }
    struct myfavouriteparam {
        static let conference_id : String = "conference_id"
        
        static let user_id : String = "user_id"
       
        
        
    }
    struct dashboardparam {
        static let conference_id : String = "conference_id"
        
        static let user_id : String = "user_id"
        
        
        
    }
    
    
    
    //    "note_id:8 (if we need to edit)
    //    user_id:95
    //    conference_id:1
    //    note:helllo123
    //    speker_id:1"""
    
    
//    struct getsessionList {
//        static let conference_id : String = "conference_id"
//        static let date : String = "date"
//
//
//    }
//    "user_id:95
//    conference_id:1
//    speaker_id:88
//    is_like:1"
//
}


struct defaultLangID
{
    static let langID : Int! = 1
}

/**
 * Pagination Limit
 */
struct PaginationLimit
{
    static let  pageLimit = 10
}



//MARK GET authentic HEADER
/**
 * getHeader // to create header
 * Authenticate // _headerKey
 * create header using UserID and authenticationID
 * header format userID:AuthenticationID
 * after create format convert in base64 string
 */
let Authenticate = "Authenticate"
let AuthID = "123a2dadf7925dc3a2917eff9ba9d656"
let authText = "H3!loL0@d$"
/*
 Authentication process
 MD5(H3!loL0@d$_{userID})
 userID_MD5
 convert to base 64
 */
func Authentication()-> [String : String]
{
    let userID = 0//getUserObject()?.userId ?? CONSTANTs.ZERO
    _ = String(format: "%@_%d", authText,userID)
    let convertedMD5String : String = ""//Utility.shared().MD5(convertString)!
    let userIDAndMD5String = String(format: "%d_%@", userID,convertedMD5String)
    let string = userIDAndMD5String
    
    let utf8str = string.data(using: String.Encoding.utf8)
    let base64Encoded = utf8str?.base64EncodedString()
    debugPrint(base64Encoded ?? "")
    return [Authenticate : base64Encoded! as String] as [String : String]
}
/**
 * struct STATUS
 */
struct STATUS
{
    static let FAIL = "fail"
    static let SUCCESS = "success"
    static let ERROR = "error"
    static let INVALID = "invalid"
    static let CHANGE = "change"
    static let AlreadyLogin = "alreadylogin"
    static let unverified = "unverified"
    
}


// MARK: - API LIST
struct API
{
//    static var baseUrlForDev:String = "http://202.131.107.107/vr/CMA/api/v1/"
    static var baseUrlForDev:String = "http://202.131.107.107/vr/CMA/api/v2/"
    static var baseUrlForLive:String = ""
    
    static var baseUrl = baseUrlForDev
    static var checkVersion : String = baseUrl + "Settings/getLatestVersion"
    static var loginAPI : String = baseUrl + "Users/checkLogin"
    
    static var profileCategoryAPI : String = baseUrl + "Users/getcategory"
    static var profileSubCategoryAPI : String = baseUrl + "Users/getspecialization"
  static var updateProfile : String = baseUrl +  "Users/updateprofile"
    static var getConferenceList : String = baseUrl + "Users/getConferenceList"
    
      static var getLinkAdsSponsorUrl : String = baseUrl + "Users/getLinkAdsSponsor"
    static var updateStepUrl : String = baseUrl + "Users/updateStep"

    
   static var getConferenceSessionDate : String = baseUrl + "Users/getSessionDate"
     static var getSessionDetailsUrl : String = baseUrl + "Users/getSessionDetails"
     static var saveBookmarkForSessionURL : String = baseUrl + "Users/saveBookmarkForSession"
    
    static var getSpeakersnURL : String = baseUrl + "Users/getSpeakers"
    static var getSpeakerDetailsUrl : String = baseUrl + "Users/getSpeakerDetails"
    
   
    static var saveBookmarkForSpeakerUrl : String = baseUrl + "Users/saveBookmarkForSpeaker"
    
    static var getOrganizersUrl : String = baseUrl + "Users/getOrganizers"
     static var getOrganizersDetailUrl : String = baseUrl + "Users/getOrganizersDetails"
    
    static var saveNoteForSessionUrl : String = baseUrl + "Users/saveNoteForSession"
    static var saveNoteForSpeakerUrl : String = baseUrl + "Users/saveNoteForSpeaker"
   static var getAttendeesUrl : String = baseUrl +  "Users/getAttendees"
    
     static var getMyFavoritesUrl : String = baseUrl +  "Attendees/getMyFavorites"
    
    //Swapnil
    static var getAllCmsPage : String = baseUrl + "About/getAllCmsPage"
    static var getSocialMediaUrls : String = baseUrl + "Settings/getSocialMediaUrls"
    static var getEntertainment : String = baseUrl + "Attendees/getEntertainment"
    
   static var getDashboardUrl : String = baseUrl + "Attendees/getDashboard"
    
    static var getSponsorsUrl : String = baseUrl + "Users/getSponsors"
    static var getSponsorsDetails : String = baseUrl + "Users/getSponsorsDetails"
    static var selfieList : String = baseUrl + "Selfies/selfieList"
    static var sendOTP : String = baseUrl + "Users/sendOTP"
    static var verifyOtp : String = baseUrl + "Users/verifyOtp"
    
    static var getFrameList : String = baseUrl + "Frames/getFrameLists"
    
    static var getConferenceDetails : String = baseUrl + "Conferences/getConferenceDetails"

    
}

/*!
 * MARK: Status
 * appIsUnderConstrution: status for app UnderConstrution
 * noVersionAvailable: status for no version Available
 * versionAvailable: status for version Available
 */
struct Status
{
    static let appIsUnderConstrution = 0
    static let noVersionAvailable = 1
    static let versionAvailable = 2
}

/*!
 *  MARK: VersionUrl
 *  versionUpdateurl: send the blank string as a url when no update is availale
 */
struct VersionUrl
{
    static let versionUpdateurl = ""
}


/*!
 *  MARK: Blank
 *  blankString: indicate the blank string
 */
struct Blank
{
    static let blankString = ""
}

/*
 *  MARK: ProductType
 *  blankString: indicate the blank string
 */
struct ProductType
{
    static let configure = "configurable"
    static let simple = "simple"
}

/*!
 *  MARK: UserDefaultKey
 *  checkAppVersionKey: Key of checkAppVersion Api response stored in user defaults
 *  lastLoggedInKey: Key of lastLoggedIn Api response stored in user defaults
 *  registeredUserKey: Key of registerUser Api response stored in user defaults
 */
struct UserDefaultKey
{
    static let basicInformation = "BasicInformation"
    static let AppDevice_Token = "AppDevice_Token"
    static let userLoginInfo = "UserLoginInfo"
     static let GetCMSPage = "GetCMSPage"

}

/*!
 *  MARK: ModelUserDefaultKey
 *  checkAppVersionKey: Key of checkAppVersion Api response stored in user defaults
 *  lastLoggedInKey: Key of lastLoggedIn Api response stored in user defaults
 *  registeredUserKey: Key of registerUser Api response stored in user defaults
 */

/*
 *  getUserObject // get stored user Object
 */
//func getUserObject() -> BasicDetails?
//{
//    return getObjectFromNSUser(UserDefaultKey.basicInformation) as? BasicDetails
//}
//
//func getLoginUserObject() -> LoginResponse?
//{
//    return getObjectFromNSUser(UserDefaultKey.userLoginInfo) as? LoginResponse
//}
//
func getDeviceToken() -> String?
{
    return getObjectFromNSUser(UserDefaultKey.AppDevice_Token) as? String
    //    return getObjectFromNSUser(UserDefaultKey.userLoginInfo) as? LoginResponse
}

func getAPIHeaders(isAuthorized : Bool) -> [String:String]
{
//    if isAuthorized {
//        if getCustomerID() == ""{
//            return ["Content-Type" : "application/json",
//                    "Authorization" : (getUserObject()?.general.accessToken) ?? ""]
//        }
//        else{
//            return ["Content-Type" : "application/json",
//                    "Authorization" : "Bearer " + (getLoginUserObject()?.bearerToken)!]
//
////            return ["Content-Type" : "application/json",
////                    "Authorization" : "Bearer " + (getLoginUserObject()?.bearerToken)!]
//        }
//    }
    return [:]
}



/*!
 *  MARK: ModelDecodeKey
 *  messageDecodeKey: Key of checkAppVersion Api response stored in user defaults
 *  countryDecodeKey: Key of lastLoggedIn Api response stored in user defaults
 *  versionDecodeKey: Key of registeredUser  Api response stored in user defaults
 *  key defined for init method
 */
struct ModelDecodeKey
{
    static let messageDecodeKey = "messageDictionary"
    static let countryDecodeKey = "countryDictionary"
    static let versionDecodeKey = "ServerResult"
    
}

/*!
 *  MARK: ModelEncodeKey
 *  messageDecodeKey: Key of checkAppVersion Api response stored in user defaults
 *  countryDecodeKey: Key of lastLoggedIn Api response stored in user defaults
 *  versionDecodeKey: Key of registeredUser  Api response stored in user defaults
 *  key defined for init method
 */
struct ModelEncodeKey
{
    static let messageDecodeKey = "messageDictionary"
    static let countryDecodeKey = "countryDictionary"
    static let versionDecodeKey = "ServerResult"
}


/*!
 * MARK: CrashApiKey
 * crashApp Constant
 */
struct CrashApiKey
{
    static let errotTextKey = "errorText"
}


/*!
 * MARK: UserDefaultLoginStatus
 * loginSuccess:  Status 1 for logged in User
 * isLogin: Key to save login Status to User Defaults
 * loginFail: Status 0 for not logged  in User
 */
struct UserDefaultLoginStatus
{
    static let loginSuccess = 1
    static let isLogin: String = "isLogin"
    static let loginFail:Int = 0
}


/*!
 * MARK: showStatus
 * statusSuccess: if get success then 1
 * statusFailure: if get failure then 0
 * loginType: for login type
 * deviceType: for device Type (Android Or iOS)
 */
struct showStatus
{
    static let statusSuccess = 1
    static let statusFailure = 0
    static let loginType = 1
    static let deviceType = 1
}


/*!
 * MARK: struct "StringConstant" is used to show String Constants used in project
 */
struct StringConstant
{
    static let zero = "0"
    static let one = "1"
    static let two = "2"
}


/*!
 * MARK: Limit
 * minimumPasswordLimit: minimum Password Limit
 * maximumPassWordLimit: maximum PassWord Limit
 * minimumUserNameLimit: minimum UserName Limit
 * maximumUserNameLimit: maximum UserName Limit
 */
struct Limit
{
    static let minimumPasswordLimit = 6
    static let maximumPassWordLimit = 20
    
    struct registrationLimits
    {
        static let NormalLimit = 50
        static let MobileNoLimit = 14
        static let EmalLimit = 30
        static let ISRLimit = 10
        static let OfficeNoLimit = 14
    }
    
    struct ProfileLimits
    {
        static let CompanyName = 50
        static let Name = 50
        static let Address = 500
        static let AboutUS = 500
        static let CompanyRegistration = 25
        static let PanNO = 10
        static let NormalLimit = 100
        static let MobileNoLimit = 9
        static let OfficeNoLimit = 14
        static let EmalLimit = 30
    }
}
/**
 *  Get selected Notification button
 */
struct NOTIFY
{
    static let test = "test"
    static let tabBadgeNotificaiton = "NotificationIdentifier"
    
}

/*!
 * MARK: Regex
 * strEmailRegex: Email Format
 * strUrlRegEx: Url Format
 * strPhoneNumberRegex: Phone number format
 */
struct Regex
{
    static let strEmailRegex = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@" + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" +
        "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+"
    static let strUrlRegEx = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&amp;=]*)?"
    static let strPhoneNumberRegex = "^((\\+)|(00)|(\\*)|())[0-9]{10,14}((\\#)|())$"
}


/*!
 * MARK: struct "AppMethod" having Methods To Check Internet Connection & null String
 */
struct AppMethod
{
    /*!
     * MARK: system info
     */
    static let DEVICEID = UIDevice.current.identifierForVendor?.uuidString
    static let SYSTEMVERSION = Float(UIDevice.current.systemVersion)
    static let SCREENSIZE = UIScreen.main.bounds.size
    static let SCREENWIDTH = UIScreen.main.bounds.size.width
    static let SCREENHEIGHT = UIScreen.main.bounds.size.height
    static let SCREENSCALE = UIScreen.main.scale
    static let ORIENTATION = UIApplication.shared.statusBarOrientation
    
    
    /*!
     * MARK: "IsInternet()" is used to check Internet Connection
     * @return : if there is internet or not , Type: Bool
     */
    static func IsInternet() -> Bool
    {
        //return GLOBAL.checkInternetStatus()
        
        if Float(UIDevice.current.systemVersion) >= 9.0
        {
            var zeroAddress = sockaddr_in6()
            zeroAddress.sin6_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
            zeroAddress.sin6_family = sa_family_t(AF_INET6)
            
            guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress,
                                                                   {
                                                                    $0.withMemoryRebound(to: sockaddr.self, capacity: 1)
                                                                    {
                                                                        SCNetworkReachabilityCreateWithAddress(nil, $0)
                                                                    }
                                                                    
            }) else
            {
                return false
            }
            var flags : SCNetworkReachabilityFlags = []
            if SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) == false
            {
                return false
            }
            
            let isReachable = flags.contains(.reachable)
            let needsConnection = flags.contains(.connectionRequired)
            return (isReachable && !needsConnection)
        }
        else
        {
            var zeroAddress = sockaddr_in()
            zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
            zeroAddress.sin_family = sa_family_t(AF_INET)
            
            guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress,
                                                                   {
                                                                    $0.withMemoryRebound(to: sockaddr.self, capacity: 1)
                                                                    {
                                                                        SCNetworkReachabilityCreateWithAddress(nil, $0)
                                                                    }
            }) else
            {
                return false
            }
            
            var flags : SCNetworkReachabilityFlags = []
            if SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) == false
            {
                return false
            }
            let isReachable = flags.contains(.reachable)
            let needsConnection = flags.contains(.connectionRequired)
            return (isReachable && !needsConnection)
        }
    }
    
    
    /*!
     * MARK: "isNullString(_ string: String!)" is used to check NullString
     * @param string : accept a string as a parameter, to check if it is a null string or what , Type: String
     * @return : if it is null or not , Type: Bool
     */
    static func isNullString(_ string: String!) -> Bool
    {
        let str = string
        
        //str = string.stringByReplacingOccurrencesOfString(" ", withString: "")
        if (str == nil
            || str == "<null>"
            || (str == "<NULL>")
            || (str == "(null)")
            || (str == "(NULL)")
            || (str == "null")
            || (str == "NULL")
            || str?.count == 0
            || (str == "")
            || (str == " ")
            || (str == "\n")
            || str?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0
            )
        {
            return true
        }
        return false
    }
}


/*!
 * MARK: struct "AppString" is used to show common strings used in project
 */
struct AppString
{
    static let AppName = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as! String
    static let Loading : String! = "Loading..."
}
/*!
 * MARK: struct "IntConstant" is used to show Int Constants used in project
 */
struct CONSTANTs
{
    static let ZERO = 0 // general
    static let ONE = 1  // general
    static let MINUS_ONE = -1  // general
    static let TWO = 2  // general
    static let THREE = 3// general
    static let FOUR = 4 // general
    static let FIVE = 5 // general
    static let SIX = 6  // general
    static let SEVEN = 7// general
    static let EIGHT = 8// general
    static let NINE = 9// general
    static let TEN = 10 //
    static let TWENTYSEVEN = 27 //
    static let POINTFIVE = 0.5 //
    
    
    static let FOURTY = 40 //row height of registration other texts fields
    static let FIFTY = 50 //row height of registration other texts fields
    
    static let SIXTY = 60 //row height of registration fields
    static let SEVENTY = 70 //row height of registration other texts fields
    static let NINGHTY = 90 //row height of registration other texts fields
    static let HUNDRED = 100 //row height of registration other texts fields
    static let TWOFOURTY = 240 //row height of registration other texts fields
    static let ONEFOURTY = 140 //row height of registration other texts fields
    
    static let ONEFIFTY = 150 //row height of registration other texts fields
    static let ONESEVENTY = 170 //row height of registration other texts fields
    
    static let TWOHUNDRED = 200 //row height of registration other texts fields
    static let ONESIXTY = 160 //row height of registration other texts fields
    static let THIRTYFIVE = 35 //row height of registration other texts fields
    static let ONEEIGTHY = 180 //row height of registration other texts fields
    static let ONEEIGHTSIX = 186 //row height of registration other texts fields
    
    static let THREESIXZERO = 360 //row height of registration other texts fields
    static let TEXTLENTH = 500 //row height of registration other texts fields
    static let TWOZEROSIX = 206 //row height of registration other texts fields
    static let TWOTHOUSAND = 2000 //row height of registration other texts fields
    
    struct ProfileCellHeight // constants used in profile screen
    {
        static let FIFTY = 50 //row height of registration fields
        static let ONETHIRTYFIVE = 135 //row height of registration other texts fields
        static let ONETHIRTY = 130 //row height of registration other texts fields
        static let SIXTYFOUR = 64 //row height upload documents
    }
    
    struct HeightsBookingScreen // constants used in profile screen
    {
        static let SectionHeight = 50 //section height
        static let RowHeight = 45 //row height
        static let RowHeightTextView = 135 //row height
        static let splitRowHeight = 90 //row height
    }
    
    struct viewShipperScreen // constants used in profile screen
    {
        static let ROWHEIGHT1 = 100 //section height
        static let ROWHEIGHT2 = 215 //row height
        static let ROWHEIGHT3 = 186 //row height
        static let ROWZERO = 0 //row height
        
    }
    
    struct TransporterProfileCellHeight // constants used in transporter's profile screen
    {
        static let FIFTY = 50 //row height
        static let EIGHTY = 80 //row height Header
        static let TWOONETWO = 212 //row height of Performance cell
        static let ONEEIGHTSIX = 186 //row height of address cell
        static let TWOSEVENZERO = 270 //row height Contact details
    }
    
    
}
struct FLOAT_CONSTANTs
{
    static let THREEPOINTZERO = 3.0
    static let ZEROPOINTZERO = 0.0
    
}
/**
 * SET COLOR CODE
 */
struct COLORCODE
{
    static let WHITECOLOR = "#ffffff" // for Tab title color
    static let BLACKCOLOR = UIColor.init(hexColorString: "#000000")
    
}


/**
 * MARK: struct "DateFormat" is used to show different date formats
 */
struct DateFormat
{
    static let dateFormatPass : String! = "yyyy/MM/dd"
    static let dateFormatWithTime = "dd/MM/yyyy hh:mm"
    static let dateFormatMonthsName = "dd MMM yyyy"
    static let dateFormatForConfirmBId = "MM/dd/yyyy HH:mm"
    static let dateFormatSet : String! = "MM/dd/yyyy"
    
    static let serverDateFormat : String! = "yyyy-MM-dd HH:mm:ss"
    static let localDateFormat : String! = "MMM dd, hh:mm a"
    
}

/**
 * MARK: struct "responseDictConstant" is used to show all constant key received from api
 */
struct responseDictConstant
{
    static let strResponseDict : String! = "d"
    static let statusKey : String! = "status"
    static let msg : String! = "msg"
    static let resultKey : String! = "result"
    static let codeKey : String! = "code"
    static let dataKey : String! = "data"
    
    
}

/*!
 * MARK: "var topMostController: UIViewController?" is a variable which gets the top most viewcontroller
 * here it is used to show on which controller the activity Indicator will be shown
 */
public var topMostController: UIViewController?
{
    var presentedVC = UIApplication.shared.keyWindow?.rootViewController
    while let pVC = presentedVC?.presentedViewController
    {
        presentedVC = pVC
    }
    return presentedVC
}


/**
 * MARK: "showLoader(_ loadingText : String)" shows the activity indicator.
 * @param loadingText : Text of Activity Indicator, Type: String
 */


let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
let loaddingTextLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 50))
var txtloadingText = ""
// Black Background view for loader
var blackBG : UILabel? = nil

var multiColorLoader = BLMultiColorLoader()
func showLoader(_ loadingText : String)
{
    hideLoader()
    //txtloadingText = loadingText
    UIApplication.shared.keyWindow!.isUserInteractionEnabled = false
    blackBG?.isUserInteractionEnabled = false
    let when = DispatchTime.now() + 0.1// change 2 to desired number of seconds
    DispatchQueue.main.asyncAfter(deadline: when) {
        
        blackBG?.isHidden = false
        
        multiColorLoader = BLMultiColorLoader(frame: CGRect(x: CGFloat(50), y: CGFloat(50), width: CGFloat(50), height: CGFloat(50)))
        multiColorLoader.center = ((UIApplication.shared.delegate?.window)!)!.center
        multiColorLoader.lineWidth = 6.0
//        multiColorLoader.colorArray = [UIColor.init(hexColorString: COLORCODE.APP_ORANGE_COLOR)]
        multiColorLoader.colorArray = [colorset.themeColor]
        multiColorLoader.startAnimation()
        UIApplication.shared.keyWindow!.addSubview(multiColorLoader)
        
        blackBG = UILabel(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        blackBG?.backgroundColor = .black
        blackBG?.alpha = 0.2
        UIApplication.shared.keyWindow!.addSubview(blackBG!)
    }
    //   AppDelegate.sharedInstance().window?.addSubview(blackBG!)
}
func hideLoader()
{
    UIApplication.shared.keyWindow!.isUserInteractionEnabled = true
    blackBG?.isUserInteractionEnabled = true
    blackBG?.isHidden = true
    multiColorLoader.stopAnimation()
    
    if multiColorLoader.isDescendant(of: UIApplication.shared.keyWindow!)
    {
        multiColorLoader.removeFromSuperview()
    }
    
    blackBG?.removeFromSuperview()
    if multiColorLoader != NSNull()
    {
        multiColorLoader.removeFromSuperview()
    }
    // loaddingTextLabel.isHidden = true
    
    let when = DispatchTime.now() + 0.1// Change 2 to desired number of seconds
    DispatchQueue.main.asyncAfter(deadline: when) {
        // Your code with delay
        multiColorLoader.stopAnimation()
        multiColorLoader.removeFromSuperview()
        blackBG?.removeFromSuperview()
        // loaddingTextLabel.removeFromSuperview()
    }
    //    DispatchQueue.main.async {
    //
    //
    //    }
    
    // GMDCircleLoader.hide(from: (UIApplication.shared.delegate?.window)!, animated: true)
    //    if blackBG != nil
    //    {
    //
    //    }
    
}



/*!
 * MARK: "showAlert(_ title: String,message :
 *        String,cancelButtonTitle:String,otherButtonTitles:NSArray,destructiveButtonTitle:
 *        String,showInViewController: UIViewController,tapBlock: @escaping UIAlertControllerCompletionBlock)" shows
 *        the Alert Controller.
 *  @param title: shows the title of Alert Controller, Type: String
 *  @param message: displays the message of Alert Controller, Type: String
 *  @param cancelButtonTitle: title of cancel button of Alert Controller, Type: String
 *  @param otherButtonTitles: titles of other buttons of Alert Controller, Type: NSArray
 *  @param destructiveButtonTitle: title of destructive button of Alert Controller, Type:
 *                                 String
 *  @param showInViewController: Indicates in which viewcontroller you want to display the alert controller,
 *                               Type: UIViewController
 *  @param tapBlock: write action while clicking on particular button , Type: UIAlertControllerCompletionBlock
 */
func showAlert(_ title: String,message : String,cancelButtonTitle:String,otherButtonTitles:NSArray,destructiveButtonTitle: String, showInViewController: UIViewController, tapBlock: @escaping UIAlertControllerCompletionBlock)
    
{
    let tapBlockNew: UIAlertControllerCompletionBlock! = tapBlock
    
    UIAlertController.showAlert(in: showInViewController,
                                withTitle: title,
                                message: message,
                                cancelButtonTitle: cancelButtonTitle,
                                destructiveButtonTitle: "",
                                otherButtonTitles: otherButtonTitles as [AnyObject],
                                tap: {(controller, action, buttonIndex) in
                                    
                                    tapBlockNew(controller, action, buttonIndex)
    })
}


/*!
 * MARK: "showAlertHere(_ message : String,cancelButtonTitle:String, OKButtonTitle:String,showInViewController: UIViewController, tapBlock:  @escaping StatusBlock)" shows
 *        the Alert Controller.
 *  @param message: displays the message of Alert Controller, Type: String
 *  @param cancelButtonTitle: title of cancel button of Alert Controller, Type: String
 *  @param OKButtonTitle: title of ok buttons of Alert Controller, Type: String
 *  @param showInViewController: Indicates in which viewcontroller you want to display the alert controller,
 *                               Type: UIViewController
 *  @param tapBlock: write action while clicking on particular button , Type: StatusBlock
 */
typealias StatusBlock = (_ index : Int) -> Void

func showAlertHere(_ message : String,cancelButtonTitle:String, OKButtonTitle:String,showInViewController: UIViewController, tapBlock:  @escaping StatusBlock)
{
    
    
    
    let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
    //Make cancel action
    let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .default) { action in
        tapBlock(CONSTANTs.ONE) //for cancel/ok
        //NSLog("OK action occured.")
    }
    //Make retry action
    let retryAction = UIAlertAction(title: OKButtonTitle, style: .default) { action in
        tapBlock(CONSTANTs.TWO) // for retry
        //NSLog("OK action occured.")
    }
    
    let titleFont : UIFont = Utility.getFont(fontName: FONT.Regular, sizeOfFont: FONT_SIZE.SIXTEEN)
    let myString  = message
    var myMutableString = NSMutableAttributedString()
    myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedStringKey.font:titleFont])
    myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: COLORCODE.BLACKCOLOR, range: NSRange(location:0,length:myString.count))
    alertController.setValue(myMutableString, forKey: "attributedTitle")
    
    
    
    cancelAction.setValue(COLORCODE.BLACKCOLOR, forKey: "titleTextColor")
    retryAction.setValue(COLORCODE.BLACKCOLOR, forKey: "titleTextColor")
    
    // Add the action.
    alertController.addAction(cancelAction)
    alertController.addAction(retryAction)
    // Show alert
    UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
}
func btnTapRetry1(sender: UIButton)
{
    
    // self.removeFromSuperview()
    // delegate?.retryApi(self)
}

func attributeTextForUIalertAction(strTxt : String)-> NSMutableAttributedString
{
    let actionfont : UIFont = Utility.getFont(fontName: FONT.Regular, sizeOfFont: FONT_SIZE.FOURTEEN)
    
    let attributedText = NSMutableAttributedString(string: strTxt)
    let range = NSRange(location: 0, length: attributedText.length)
    attributedText.addAttribute(NSAttributedStringKey.kern, value: 1.5, range: range)
    attributedText.addAttribute(NSAttributedStringKey.font, value: actionfont, range: range)
    
    return attributedText
    
}

/*!
 * MARK: "setUserDefault(_ ObjectToSave : AnyObject?  , KeyToSave : String)" is used to set Data In User Defaults
 * @param ObjectToSave : object you want to save to user defaults , Type: AnyObject
 * @param KeyToSave : key to uniquly identify particular data , Type: String
 */
func setUserDefault(_ ObjectToSave : AnyObject?  , KeyToSave : String)
{
    let defaults = UserDefaults.standard
    
    defaults.set(ObjectToSave, forKey: KeyToSave)
    
    UserDefaults.standard.synchronize()
}


/*!
 * MARK: "getUserDefault(_ KeyToReturnValye: String)" is used to get Object From NSUser
 * @param KeyToReturnValye : accept a key as a parameter,key to uniquly identify particular data , Type: String
 * @return : data from User defaults , Type: AnyObject
 */
func getUserDefault(_ KeyToReturnValye : String) -> AnyObject?
{
    let defaults = UserDefaults.standard
    
    if valueAlreadyExist(key: KeyToReturnValye) == true
    {
        if let obj = defaults.object(forKey: KeyToReturnValye)
        {
            return obj as AnyObject?
        }
        else
        {
            return nil
        }
    }
    else
    {
        return nil
    }
}
/**
 *  valueAlreadyExist()
 *  check value exist in UserDefault or not
 */
func valueAlreadyExist(key: String) -> Bool {
    return UserDefaults.standard.object(forKey: key) != nil
}

/**
 * MARK: "setDataInNSUser(_ object:AnyObject, key: String)" is used to set Data In User Defaults
 * @param object : object you want to save to user defaults , Type: AnyObject
 * @param key : key to uniquly identify particular data , Type: String
 * @return : Void = returns void
 * This method saves data in user defaults by performing data encoding
 */
func setDataInNSUser(_ object:AnyObject, key: String) -> Void
{
    Defaults.archive(DefaultsKey<Any>(key), object)
}

/*!
 * MARK: "getObjectFromNSUser(_ key: String)" is used to get Object From NSUser
 * @param key : accept a key as a parameter,key to uniquly identify particular data , Type: String
 * @return : data from User defaults , Type: Any
 * This method gets data from user defaults by performing data decoding
 */
func getObjectFromNSUser(_ key: String) -> Any?
{
    return Defaults.unarchive(DefaultsKey<Any>(key))
}

func getUserProfileDataList() -> LoginData?
{
    return getObjectFromNSUser(UserDefaultKey.userLoginInfo) as? LoginData
}
func getCMSPageListData() -> GetCMSData?{
    return getObjectFromNSUser(UserDefaultKey.GetCMSPage) as? GetCMSData
}
/*!
 * MARK: "removeObjectFromDefault(_ key: String)" is used to remove Object from default
 * @param key : accept a key as a parameter,key to uniquly identify particular data , Type: String
 * This method remove data from user defaults
 */
func removeObjectFromDefault(_ key: String)
{
    Defaults.remove(DefaultsKey<Any>(key))
}


/**
 * Check Image is nil or not
 */
func imageIsNullOrNot(imageName : UIImage)-> Bool
{
    
    let size = CGSize(width: 0, height: 0)
    if (imageName.size.width == size.width)
    {
        return false
    }
    else
    {
        return true
    }
}
/**
 - get height of image (calculation)
 - paramter : strImageSize // size of image
 - paramter : widthOfCell // height based on width
 - return : float value , return the height of image after calcuation
 */
func getHeightOfImage(strImageSize : String, widthOfCell : CGFloat) -> CGFloat
{
    var imgHeight : CGFloat = CGFloat(CONSTANTs.ZERO)
    
    
    if strImageSize.isNull == true // in case if image size comes nill thn will return cellWidth size for height
    {
        return widthOfCell
    }
    
    if strImageSize != "0"
    {
        let arrSize = strImageSize.components(separatedBy: "x")
        let widthOfImg = arrSize[CONSTANTs.ZERO]
        let heightOfImg = arrSize[CONSTANTs.ONE]
        
        
        if widthOfImg == heightOfImg
        {
            imgHeight = widthOfCell
        }
        else
        {
            let w: CGFloat = CGFloat((widthOfImg as NSString).doubleValue)
            let h: CGFloat = CGFloat((heightOfImg as NSString).doubleValue)
            
            let imageSize = CGSize(width: CGFloat(w), height: CGFloat(h))
            let imageViewWidth = widthOfCell
            let correctImageViewHeight: CGFloat = (imageViewWidth / imageSize.width) * imageSize.height
            imgHeight = correctImageViewHeight
        }
    }
    else
    {
        imgHeight = widthOfCell
    }
    
    return imgHeight
}

func RGBColor(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
    return UIColor(red: red / 255, green: green / 255, blue: blue / 255, alpha: 1)
}

struct MAX_HEIGHT
{
    static let TEN_THOUSAND : CGFloat = 10000
    
}

var LABEL = UILabel()
func getLabelSize(withText text: String, font: UIFont, maxWidth: CGFloat) -> CGSize {
    
    let strText: String = text
    var size : CGSize = CGSize.zero
    LABEL.numberOfLines = CONSTANTs.ZERO
    LABEL.font = font
    LABEL.lineBreakMode = NSLineBreakMode.byWordWrapping
    LABEL.text = strText
    size = LABEL.sizeThatFits(CGSize(width: maxWidth, height: MAX_HEIGHT.TEN_THOUSAND))
    return size
    
}

func getWidthFromString(withText text: String, font: UIFont, maxHeight: CGFloat) -> CGFloat {
    
    let strText: String = text
    var size : CGSize = CGSize.zero
    LABEL.numberOfLines = CONSTANTs.ZERO
    LABEL.font = font
    LABEL.lineBreakMode = NSLineBreakMode.byWordWrapping
    LABEL.text = strText
    size = LABEL.sizeThatFits(CGSize(width: MAX_HEIGHT.TEN_THOUSAND, height: maxHeight))
    return size.width
    
}

struct ValidationMsg
{
    //Add Address Validation Message
    static let addAddressNamePrefix = "Please provide prefix name"
    static let addAddressFirstName = "Please provide first name"
    static let addAddressMiddleName = "Please provide middle name"
    static let addAddressLastName = "Please provide last name"
    static let addAddressNameSuffix = "Please provide suffix name"
    static let addAddressCompany = "Please provide company name"
    static let addAddressStreet = "Please provide street address"
    static let addAddressCity = "Please provide city name"
    static let addAddressState = "Please select state"
    static let addAddressCountry = "Please select country"
    static let addAddressZipCode = "Please provide zip code"
    static let addAddressPhoneNum = "Please provide phone number"
    static let selecttermsandconditions = "Please select terms and conditions"
    static let addAddressVatNum = "Please provide vat number"
    static let addAddressPhoneNumMaxLength = "Phone number length should be maximum of 15 digits"
static let sponsercodeEnter = "Please enter digital code"
    static let addEmail = "Please provide email"
    static let validEmail = "Please provide valid email"
    static let addPassword = "Please provide password"
    static let addconfirmPassword = "Please provide confirm password"
    static let notSamePassword = "Password and Confirm Password should be same"
    static let changeOldPassword = "Please provide old password"
    static let changePassword = "Please provide new password"
    static let changeConfirmPassword = "Please provide new confirm password"
    static let cameraNotFind = "You don't have camera"
    static let addCommect = "Please provide comment"
    static let addFeedback = "Please provide feedback"
    static let addSubject = "Please provide subject"
    static let msgLogout = "Are you sure you want to logout?"
    
    static let addPasswordLenth = "Please provide at least 6 character in password"
    static let addConPasswordLenth = "Please provide at least 6 character in confirm password"

    static let applideCouponCode = "Please provide coupon code"

    static let nickName = "Please provide nickName"
    static let ReviewisRequired = "Please provide summary of review"
    static let Review = "Please provide Review "
    
    static let msgDelete = "Are you sure you want to remove item from cart list?"
    static let msgChnageAddress = "Are you sure you want to change address?"

    static let cartEmptyMsg = "Cart is empty! Please add product to cart"
    static let selectBillingShipping = "Please select billing and shipping address"

    static let permmissonForLocation = "Please provide access of location"
    
    static let addShippingMethod = "Please select shipping method"
    static let addPaymentMethod = "Please select payment method"
    
    static let addCorrectPswd = "Please provide at least 6 character in old password"
    static let addNewPswdLength = "Please provide at least 6 character in new password"
    static let addNewConfPswdLength = "Please provide at least 6 character in confirm password"
    
    static let addVerificationCode = "Please provide verification code"
    static let loginmsg = "Please login for bookmark"
     static let addNotes = "Please Add notes"
}

struct  CommenMessage{
    
    static let chooseImage = "Choose image from"
    static let chooseCamera = "Camera"
    static let chooseGallery = "Gallery"
    static let chooseCancel = "Cancel"
    static let chooseOk = "Ok"
    
    static let NumberVerify = "Thanks, Did you get a SMS pin?"
}

func methodToGetDictionaryToStringURL(params : NSDictionary, strURL:String) -> String{
    let url = URL(string: strURL)!
    
    var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
    
    let queryItems = params.map{
        return URLQueryItem(name: "\($0)", value: "\($1)")
    }
    
    urlComponents?.queryItems = queryItems
    
    return "\((urlComponents?.url)!)"
}

func FontSizeFunc(size:CGFloat) -> CGFloat
{
    return ((screenWidth * size) / 375.0)
}
func kDevPropotionalHeight(size:CGFloat) -> CGFloat
{
    return ((screenHeight * size) / 667.0)
}

//
//  AppExtension.swift
//  MSP-eCommerce
//
//  Created by msp on 25/07/18.
//  Copyright © 2018 msp. All rights reserved.
//

import Foundation
import UIKit

/// classes import
// MARK: - classes import


//MARK:     extension String
//MARK:
extension String
{
    static func className(aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    //    var length: Int {
    //        return self.characters.count
    //    }
    
//    subscript (i: Int) -> String {
//        return self[Range(i ..< i + 1)]
//    }
//    
//    func substring(from: Int) -> String {
//        return self[Range(min(from, length) ..< length)]
//    }
//    
//    func substring(to: Int) -> String {
//        return self[Range(0 ..< max(0, to))]
//    }
    
//    subscript (r: Range<Int>) -> String {
//        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
//                                            upper: min(length, max(0, r.upperBound))))
//        let start = index(startIndex, offsetBy: range.lowerBound)
//        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
//        return String(self[Range(start ..< end)])
//    }
    
    func methodToPriceNumFormat() -> String {
        let strPrice = self.replacingOccurrences(of: ",", with: "", options: NSString.CompareOptions.literal, range: nil)
        if self == "" {
            return ""
        }
        let fltPrice = Float(strPrice)
        return NSString(format: "%.2f", fltPrice!) as String
    }
    
}
extension Date
{
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
    
    var age: Int {
        return Calendar.current.dateComponents([.year], from: self, to: Date()).year!
    }
}

//MARK:     extension Double
//MARK:
extension Double {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}

//MARK:     extension UIColor
//MARK:
extension UIColor {
    convenience init(r: UInt32, g: UInt32, b: UInt32, a: CGFloat) {
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    convenience init(hexColorString: String) {
        let hex = hexColorString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

//MARK:     extension UIImage
//MARK:
extension UIImage {
    static func fromColor(color: UIColor, width: CGFloat, height: CGFloat) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: width, height: height)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context?.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

//MARK:     extension UIView
//MARK:
extension UIView
{
    func addConstraintsWithFormat(format: String, views: UIView...)
    {
        var viewsDictionary = [String: UIView]()
        
        for (index, view) in views.enumerated()
        {
            let key = "v\(index)"
            
            viewsDictionary[key] = view
            
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    func addGradientView(startColor : UIColor, endColor : UIColor)  {
        let gradient = CAGradientLayer()
        
        gradient.frame = self.frame
        gradient.colors = [startColor.cgColor,endColor.cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        
        self.layer.insertSublayer(gradient, at: 0)
    }
}


//MARK:     public extension UITableView
//MARK:
public extension UITableView {
    
    func registerCellClass(cellClass: AnyClass) {
        let identifier = String.className(aClass: cellClass)
        self.register(cellClass, forCellReuseIdentifier: identifier)
    }
    
    func registerCellNib(cellClass: AnyClass) {
        let identifier = String.className(aClass: cellClass)
        let nib = UINib(nibName: identifier, bundle: nil)
        self.register(nib, forCellReuseIdentifier: identifier)
    }
    
    func registerHeaderFooterViewClass(viewClass: AnyClass) {
        let identifier = String.className(aClass: viewClass)
        self.register(viewClass, forHeaderFooterViewReuseIdentifier: identifier)
    }
    
    func registerHeaderFooterViewNib(viewClass: AnyClass) {
        let identifier = String.className(aClass: viewClass)
        let nib = UINib(nibName: identifier, bundle: nil)
        self.register(nib, forHeaderFooterViewReuseIdentifier: identifier)
    }
    
}
extension UIView {
    
    @IBInspectable var cornerRadius : CGFloat {
        set{
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
        get{
            return self.cornerRadius
        }
    }
    
    @IBInspectable var borderWidth : CGFloat   {
        set{
            self.layer.borderWidth = newValue
            
        }
        get{
            return self.borderWidth
        }
    }
    
    @IBInspectable var borderColor : UIColor   {
        set{
            self.layer.borderColor = newValue.cgColor
        }
        get{
            return self.borderColor
        }
    }
    
    func makeCornerRound(){
        
        self.layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true
    }
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:self.frame.size.width , height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    
}

extension UIButton {
    func loadingIndicator(show: Bool, strTitleOfButton: String) {
        let tag = 9876
        if show {
            let indicator = UIActivityIndicatorView()
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
            indicator.tag = tag
            self.addSubview(indicator)
            indicator.startAnimating()
            self.setTitle("", for: .normal)
        } else {
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
            self.setTitle(strTitleOfButton, for: .normal)
        }
    }}
extension UIViewController
{
    func hideKeyboard() // hide keyboard
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
}

typealias UnixTime = Int

extension UnixTime {
    private func formatType(form: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_GB")
        dateFormatter.dateFormat = form
        return dateFormatter
    }
    var dateFull: Date {
        return Date(timeIntervalSince1970: Double(self))
    }
    var toHour: String {
        return formatType(form: "HH:mm").string(from: dateFull)
    }
    var toDay: String {
        return formatType(form: "dd MMM yyyy hh:mm a").string(from: dateFull)
    }
    var toDay1: String {
        return formatType(form: "dd MMM yyyy hh:mm").string(from: dateFull)
    }
    
}
//pratik
extension UIViewController {
    func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
        self.layer.cornerRadius=5
        self.layer.masksToBounds=true
    }
    
}
extension UIFont{
    func stringRemoveDot (str:String) ->String
    {
        let strings = str.split(separator: ".").map{ String($0) }
        let newstr = strings[0]
        return newstr
    }
    
}

//Encode and Decode
func encodestring(_ str:String)->String
{
    let plainData = str.data(using: .utf8)
    let base64String = plainData?.base64EncodedString()
    debugPrint(base64String!) // Zm9v
    
    
    return base64String!
    
    //    let messageData = str.data(using: .nonLossyASCII)
    //    let finalMessage = String(data: messageData!, encoding: String.Encoding.utf8)
    //    return finalMessage!
}
func decodestring(_ str:String)->String
{
    let decodedData1 = NSData(base64Encoded: str, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
    let decodedString = NSString(data: decodedData1! as Data, encoding: String.Encoding.utf8.rawValue)
    return decodedString as! String
    
    let str:String =  String(str )
    let decodedData = Data(base64Encoded: str)
    
    debugPrint(decodedData)
    if decodedData != nil
    {
        if let decodedString:String = String(data: decodedData!, encoding: .utf8)
        {
            
            return decodedString
        }
        else{
            
            let data : NSData = (str).data(using: String.Encoding.utf8)! as NSData
            let message = String(data: data as Data, encoding: String.Encoding.nonLossyASCII)
            return message!
        }
    }
    else{
        
        let data : NSData = (str).data(using: String.Encoding.utf8)! as NSData
        let message = String(data: data as Data, encoding: String.Encoding.nonLossyASCII)
        return message!
    }
}

// Put this piece of code anywhere you like
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard1() {
        view.endEditing(true)
    }
}

func dateformateTimezoneReg(strDate:String,strZone:String)->String
{
    let dateFormate = DateFormatter()
    //dateFormate.timeZone = NSTimeZone(name: "Europe/Berlin")! as TimeZone
    dateFormate.timeZone =  TimeZone(identifier: strZone)!
    dateFormate.dateFormat="yyyy-MM-dd HH:mm:ss"
    
    let date:Date=dateFormate.date(from: strDate)!
    dateFormate.timeZone = TimeZone.current
    
    
    // dateFormate.locale=Locale(identifier: "en_US_POSIX")
    dateFormate.dateFormat="dd MMM yyyy hh:mm a"
    
    
    let str:String = dateFormate.string(from: date)
    debugPrint(str)
    
    
    return str
    
    
    
    //    let dateFormate = DateFormatter()
    //    dateFormate.dateFormat="yyyy-MM-dd HH:mm:ss"
    //    let date:Date=dateFormate.date(from: strDate)!
    //    dateFormate.dateFormat="dd MMM yyyy hh:mm a"
    //    dateFormate.locale=Locale(identifier: "en_US_POSIX")
    //    let str:String = dateFormate.string(from: date)
    //    return str
}
func dateformateDate(strDate:String,strZone:String)->String
{
    let dateFormate = DateFormatter()
    //dateFormate.timeZone = NSTimeZone(name: "Europe/Berlin")! as TimeZone
    dateFormate.timeZone =  TimeZone(identifier: strZone)!
    dateFormate.dateFormat="yyyy-MM-dd"
    
    let date:Date=dateFormate.date(from: strDate)!
    dateFormate.timeZone = TimeZone.current
    
    
    // dateFormate.locale=Locale(identifier: "en_US_POSIX")
    dateFormate.dateFormat="dd MMM yyyy"
    
    
    let str:String = dateFormate.string(from: date)
    debugPrint(str)
    
    return str
    
    
    
    
    //    let dateFormate = DateFormatter()
    //    dateFormate.dateFormat="yyyy-MM-dd"
    //    let date:Date=dateFormate.date(from: strDate)!
    //    dateFormate.dateFormat="dd MMM yyyy"
    //    let str:String = dateFormate.string(from: date)
    // return str
    
}



//
//  btnThemClass.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class btnThemClass: UIButton {

   
    override init(frame: CGRect){
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.titleLabel?.font =  UIFont.systemFont(ofSize: FontSizeFunc(size: 17.0))
        self.backgroundColor = colorset.btnThemeColor
        self.tintColor = colorset.whitetextColor
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
    }
   
   
    

}

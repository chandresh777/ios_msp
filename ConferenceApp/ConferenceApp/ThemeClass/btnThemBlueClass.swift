//
//  btnThemBlueClass.swift
//  ConferenceApp
//
//  Created by msp on 27/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class btnThemBlueClass: UIButton {
    override init(frame: CGRect){
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.titleLabel?.font =  UIFont.systemFont(ofSize: FontSizeFunc(size: 17.0))
        self.backgroundColor = colorset.drawerBackground
        self.tintColor = colorset.whitetextColor
        self.titleLabel?.textColor = colorset.whitetextColor
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

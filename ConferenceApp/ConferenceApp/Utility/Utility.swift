//
//  Utility.swift
//  MSP-eCommerce
//
//  Created by msp on 25/07/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

typealias UTILITY = (_ status:Int) -> Void

class Utility: NSObject,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
  
    static var util: Utility? = nil
    
    /*!
     * MARK: Singleton Class method
     * Model Instance will be created only once
     */
    class func shared() -> Utility
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if util == nil
        {
            util = Utility()
        }
        return util!
    }

    
    
    /*!
     * MARK: "moveToController()" is used to move to another class using navigation
     */
    class func getController(controllerIdentifier : String, storyBoardIdentifier : String)-> UIViewController
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardIdentifier, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: controllerIdentifier)
        return nextViewController
        //AppDelegate.sharedInstance().navigationController?.pushViewController(nextViewController, animated: true)
    }

    /**
     * for generate currency symbol
     */
    class func getCurrencyCode(currencyCode : String) -> String {
        
        let local = NSLocale(localeIdentifier: currencyCode)
        let currencySymbol = local.object(forKey: NSLocale.Key.currencySymbol)
        return currencySymbol! as! String
    }
    
    /**
     * for get lat long currency symbol
     */
    class func GetAddress(address : String) -> JSON
    {
        let str : String =  "https://maps.googleapis.com/maps/api/geocode/json?address=(\(address)!)&sensor=false"
        let url = URL(string:str.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)!
        let apiResponse = try? String(contentsOf: url, encoding: String.Encoding.utf8)
        if apiResponse?.isNull == false {
            let dict = JSON.init(parseJSON:apiResponse!)
            let results = (dict["results"].arrayValue)
            if results.count > 0{
                let geometry =  results[0]["geometry"]["location"]
                return geometry
            }
            else{
                return ""
            }
        }
        return ""
    }
    
    /**
     * Get Suffix number
     * Ex : 1000 = 1K
     */
    class func suffixNumber(number:NSNumber) -> String {
        
        let val = number as! Int
        if val > 999
        {
            var num:Double = number.doubleValue;
            let sign = ((num < 0) ? "-" : "" );
            
            num = fabs(num);
            
            if (num < 1000.0){
                return "\(sign)\(num)" as String;
            }
            
            let exp:Int = Int(log10(num) / 3.0 ); //log10(1000));
            
            let units:[String] = ["K","M","G","T","P","E"];
            
            let roundedNum:Double = round(10 * num / pow(1000.0,Double(exp))) / 10;
            
            return "\(sign)\(roundedNum)\(units[exp-1])" as String;
        }
        else
        {
            return "\(val)"
        }
    }
    
    
    /**
     * get String From Date
     */
    class func getStringFromDate(date : Date)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = FORMAT.dateFormat
        let dateString = dateFormatter.string(from: date as Date)
        return dateString
    }
    
    
    /**
     *   for post notificaiton
     *  @param : strNotificationName ,Date Type : String
     */
    class func postNotification(strNotificationName : String , obj : Any)
    {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: strNotificationName), object: obj)
    }
    /**
     *   for remove notificaiton
     *  @param : strNotificationName ,Date Type : String
     */
    class func removeNotificaiton(strNotificationName : String)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: strNotificationName), object: nil)
    }
    /**
     *  SET corner radious
     *  @param : radious IntType
     *  @param : view UIView
     */
    class func setCornerRadious(radious : CGFloat, view : UIView)
    {
        view.layer.cornerRadius = radious;
        view.clipsToBounds = true
    }
    
    /**
     * getFont
     * set Fontname and Size of font and get font
     */
    class  func getFont(fontName:String, sizeOfFont : CGFloat) -> UIFont
    {
        return UIFont(name: fontName, size: sizeOfFont)!
        
    }
    
    class func showAlertWithError(error : NSError, selectedIndex : @escaping UTILITY)
    {
        hideLoader()
        if error.code == ERROR.errorCODE.noInternet
        {
            
            //            let viewInternet = Bundle.main.loadNibNamed( XibName.ViewInterNetConnection, owner: topMostController, options: nil)?[0] as! ViewInterNetConnection
            //            viewInternet.frame = (AppDelegate.sharedInstance().window?.frame)!
            //            viewInternet.addPopview(callBack: { (index:NSInteger) in
            //                selectedIndex(index)
            //            })
            //            topMostController?.view.addSubview(viewInternet)
            
            
            
            //            showInternetView(showInViewController: topMostController!, tag: {index in
            //                selectedIndex(index)
            //        })
            
            showAlertHere(ERROR.errorMSG.noInterTconnection,cancelButtonTitle:"Ok", OKButtonTitle:STATIC_TEXTS.txtRetry,showInViewController: topMostController!, tapBlock:  {index in
                
                selectedIndex(index)
                
            })
            
            
        }
        else if error.code == ERROR.errorCODE.serverIssue
        {
            
            //            showAlertHere(ERROR.errorMSG.serverIssue,cancelButtonTitle:"Cancel", OKButtonTitle:STATIC_TEXTS.txtRetry,showInViewController: topMostController!, tapBlock:  {index in
            //
            //                selectedIndex(index)
            //            })
            
            
        }
        else if error.code == ERROR.errorCODE.timeOut
        {
            
            //            showAlertHere(ERROR.errorMSG.timeOut,cancelButtonTitle:"Cancel", OKButtonTitle:STATIC_TEXTS.txtRetry,showInViewController: topMostController!, tapBlock:  {index in
            //
            //                selectedIndex(index)
            //            })
            
            
        }
        else
        {
            //            let errorText = (error.localizedDescription) as String
            //            AppDelegate.sharedInstance().window.maketost
            //            AppDelegate.sharedInstance().window?.hideToastActivity()
            //            AppDelegate.sharedInstance().window?.makeToast(errorText, duration: FLOAT_CONSTANTs.THREEPOINTZERO, position: .bottom)
        }
    }
    
    class func moveToHomeViewCotroller() {
        
        let objHomeViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let objLeftViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        
        let objNavigationController: UINavigationController = UINavigationController(rootViewController: objHomeViewController)
        objNavigationController.navigationBar.isHidden = true
        
        objLeftViewController.mainViewController = objNavigationController
        
        let slideMenuController = ExSlideMenuController(mainViewController: objNavigationController, leftMenuViewController: objLeftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = objHomeViewController
        
        AppDelegate.sharedInstance().window?.backgroundColor = UIColor.white
        AppDelegate.sharedInstance().window?.rootViewController = slideMenuController
        AppDelegate.sharedInstance().window?.makeKeyAndVisible()
    }

    class func moveToProfileController() {
        
        let profileViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        let objNavigationController: UINavigationController = UINavigationController(rootViewController: profileViewController)
        objNavigationController.navigationBar.isHidden = true
        
        AppDelegate.sharedInstance().window?.backgroundColor = UIColor.white
        AppDelegate.sharedInstance().window?.rootViewController = objNavigationController
        AppDelegate.sharedInstance().window?.makeKeyAndVisible()
    }
    
    class func moveToLoginViewController() {
        
        let objLoginViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController

        let objNavigationController: UINavigationController = UINavigationController(rootViewController: objLoginViewController)
        objNavigationController.navigationBar.isHidden = true

        AppDelegate.sharedInstance().window?.backgroundColor = UIColor.white
        AppDelegate.sharedInstance().window?.rootViewController = objNavigationController
        AppDelegate.sharedInstance().window?.makeKeyAndVisible()
    }
    
    class func moveToSplashViewController() {
        
        let objLoginViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
        
        let objNavigationController: UINavigationController = UINavigationController(rootViewController: objLoginViewController)
        objNavigationController.navigationBar.isHidden = true
        
        AppDelegate.sharedInstance().window?.backgroundColor = UIColor.white
        AppDelegate.sharedInstance().window?.rootViewController = objNavigationController
        AppDelegate.sharedInstance().window?.makeKeyAndVisible()
    }

    class func setUserDefaults() {
        if !valueAlreadyExist(key: UserDefaultKey.userLoginInfo) {
            setUserDefault(NSMutableDictionary(), KeyToSave: UserDefaultKey.userLoginInfo)
        }
        
    }

}

typealias mediaSelected = (_ status:Int) -> Void

//Media controller.
class MediaController : UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    static var contr: MediaController? = nil
    var typeImage : String = ""
    var imagePicker = UIImagePickerController() // Image picker controller
    
    var imageSelected : ((Any?) -> ())? = nil
    
    /*!
     * MARK: Singleton Class method
     * Model Instance will be created only once
     */
    class func shared() -> MediaController
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if contr == nil
        {
            contr = MediaController()
        }
        return contr!
    }
    
//     * Open Date picker from camera or gallery
//     */
    func selectPhotoFrom(strType : String, vc : UIViewController)
    {
        if strType == imagePickerType.Camera
        {
            //camera
            imagePicker.delegate =  self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true

            vc.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            //gallery
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                debugPrint("Button capture")
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = true

                vc.present(imagePicker, animated: true, completion: nil)
            }
        }
    }
//     * did finish picking image , delegate of UIImagePickerController
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            picker.dismiss(animated: true, completion: nil)
            self.imageSelected!(image)
            
        } else{
            debugPrint("Something went wrong")
        }
        
    }
    
    
    
    
}

//
//  APIService.swift
//  smartrestaurants
//
//  Created by Pritesh Patel on 11/22/16.
//  Copyright © 2016 Pritesh Patel. All rights reserved.
//

/// frameworks import
// MARK: - frameworks import
import UIKit
import Foundation


/// classes import
// MARK: - classes import
import Alamofire
import SwiftyJSON


//-----------------------------------------------------------------------

//-----------------------------------------------------------------------

public enum APIServiceType: String
{
    case DEFAULT, REST, SOAP
}

// MARK: - UrlEncoding
public enum UrlEncoding: String
{
    case DEFAULT, PERCENT
}

// MARK: - RequestMethod
public enum RequestMethod: String
{
    case OPTIONS, GET, HEAD, POST, PUT, PATCH, DELETE, TRACE, CONNECT
}

// MARK: - RequestSerialization
public enum RequestSerialization: String
{
    case DEFAULT, JSON, XML
}

// MARK: - ResponseSerialization
public enum ResponseSerialization: String
{
    case DEFAULT, DATA, STRING, JSON, PROPERTYLIST, XML
}

// MARK: - ResponseFormat
public enum ResponseFormat: String
{
    case DEFAULT, JSON, XML
}

// MARK: - Internet
public enum Internet: String
{
    case DEFAULT, CHECK, DONTCHECK
}

// MARK: - Loader
public enum Loader: String
{
    case DEFAULT, SHOW, DONTSHOW, HIDE, ANIMATING, CUSTOM
}

// MARK: - ErrorState
public enum ErrorState: String
{
    case DEFAULT, PASSTOPARENT, DONTPASS
}

//-----------------------------------------------------------------------

//-----------------------------------------------------------------------

/**
 *
 */
public typealias ResultSuccess = (_ request: AnyObject?, _ sender: AnyObject?, _ json:JSON? , _ error:Error?) -> Void


/**
 *
 */
public typealias ResultFail = (_ request: AnyObject?, _ sender: AnyObject?, _ json:JSON?, _ error:Error?) -> Void

//-----------------------------------------------------------------------

//-----------------------------------------------------------------------

// MARK: API Service Class
// MARK:
class APIService
{
    
    var resultSuccess : ResultSuccess!
    var resultFail : ResultFail!
    var apiRequest : Request!
    var constantSender : AnyObject!
    
    //***********************************************************************
    
    static var apiService: APIService? = nil
    
    /// sharedService
    // MARK: - sharedService
    class func sharedService() -> APIService
    {
        if apiService == nil
        {
            apiService = APIService()
        }
        return apiService!
    }
    
    //***********************************************************************
    
    /// makeApiRequest
    // MARK: - makeApiRequest
    /**
     *
     */
    
//    func urlRequestWithComponents(urlString:String, parameters:NSDictionary) -> (URLRequestConvertible, NSData) {
//        
//        // create url request to send
//        var mutableURLRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
//        mutableURLRequest.httpMethod = "post"
//        //let boundaryConstant = "myRandomBoundary12345"
//        let boundaryConstant = "NET-POST-boundary-\(arc4random())-\(arc4random())"
//        let contentType = "multipart/form-data;boundary="+boundaryConstant
//        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
//        
//        
//        // create upload data to send
//        let uploadData = NSMutableData()
//        
//        // add parameters
//        for (key, value) in parameters {
//            
//            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(usingEncoding: String.Encoding.utf8)!)
//            
//            if value is NetData {
//                // add image
//                var postData = value as NetData
//                
//                
//                //uploadData.appendData("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(postData.filename)\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
//                
//                // append content disposition
//                var filenameClause = " filename=\"\(postData.filename)\""
//                let contentDispositionString = "Content-Disposition: form-data; name=\"\(key)\";\(filenameClause)\r\n"
//                let contentDispositionData = contentDispositionString.dataUsingEncoding(NSUTF8StringEncoding)
//                uploadData.appendData(contentDispositionData!)
//                
//                
//                // append content type
//                //uploadData.appendData("Content-Type: image/png\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!) // mark this.
//                let contentTypeString = "Content-Type: \(postData.mimeType.getString())\r\n\r\n"
//                let contentTypeData = contentTypeString.dataUsingEncoding(NSUTF8StringEncoding)
//                uploadData.appendData(contentTypeData!)
//                uploadData.appendData(postData.data)
//                
//            }else{
//                uploadData.appendData("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".dataUsingEncoding(NSUTF8StringEncoding)!)
//            }
//        }
//        uploadData.appendData("\r\n--\(boundaryConstant)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
//        
//        
//        
//        // return URLRequestConvertible and NSData
//        return (Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0, uploadData)
//    }
    
    
    func uploadImageWithMultiPart(sendOTPApi : String,parameters : [String:AnyObject],requestHeaders: [String: String]? = nil,imageParam : String ,image : UIImage?,completion:@escaping(JSON?,Error?)->())
    {
        showLoader("")
        //profileImage
        print(parameters)
//        var myImage : UIImage? = nil
//
//        if image == nil
//        {
//            myImage = nil
//
//        }
//        else
//        {
//            myImage = image ?? nil
//        }
//        //,headers: Authentication()
//        //        let headers = ["Content-Type": "text/html"]
//
        var jsonParams : [String : AnyObject] = [:]
      //  let jsonParams = ["parameters" : parameters] as [String : AnyObject]
//
//
//        Alamofire.upload(multipartFormData: { multipartFormData in
//            if myImage != nil
//            {
//                multipartFormData.append(UIImageJPEGRepresentation(myImage!, 0.5)!, withName: imageParam, fileName: "image.jpeg", mimeType: "image/jpeg")
//            }
//            for (key, value) in jsonParams {
//
//                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
//
//            }
//        }, usingThreshold: UInt64.init(), to: "https://httpbin.org/post", method: .post, headers: requestHeaders) { (result) in
//            switch result {
//            case .success(let upload, _, _):
//
//                upload.uploadProgress(closure: { (progress) in
//                    //Print progress
//                    //debugPrint(progress)
//                })
//
//                upload.responseJSON { response in
//                    //print response.result
//                    if response.result.value != nil
//                    {
//                        let json = JSON(response.result.value!)
//                        completion(json,nil)
//                    }
//                    else
//                    {
//                        completion(nil,response.error)
//                        //   Utility.showMsg(strMSg: Utility.showError(error: response.error! as NSError))
//                    }
//
//
//                    // debugPrint(response)
//                }
//
//            case .failure(let encodingError):
//
//                //  debugPrint(encodingError)
//
//                hideLoader()
//
//                let errorText = (encodingError.localizedDescription) as String
//                //                Utility.showMsg(strMSg:errorText)
//
//                completion(nil,encodingError)
//                break
//                //print encodingError.description
//            }
//        }
        
        
//        Alamofire.upload(multipartFormData:{multipartFormData in
//            if image != nil
//            {
//                multipartFormData.append(UIImageJPEGRepresentation(image!, 0.5)!, withName: imageParam, fileName: "image.jpeg", mimeType: "image/jpeg")
//            }
//            for (key, value) in parameters {
//
//                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
//
//            }}, to : sendOTPApi, method: .post)
//        { (result) in
//            //
//            switch result {
//            case .success(let upload, _, _):
//
//                upload.uploadProgress(closure: { (progress) in
//                    //Print progress
//                    //debugPrint(progress)
//                })
//
//                upload.responseJSON { response in
//                    //print response.result
//
//                    if response.result.value != nil
//                    {
//                        let json = JSON(response.result.value!)
//                        completion(json,nil)
//                    }
//                    else
//                    {
//                        completion(nil,response.error)
//                     //   Utility.showMsg(strMSg: Utility.showError(error: response.error! as NSError))
//                    }
//
//
//                    // debugPrint(response)
//                }
//
//            case .failure(let encodingError):
//
//                //  debugPrint(encodingError)
//
//                hideLoader()
//
//                let errorText = (encodingError.localizedDescription) as String
////                Utility.showMsg(strMSg:errorText)
//
//                completion(nil,encodingError)
//                break
//                //print encodingError.description
//            }
//        }
        


        Alamofire.upload(multipartFormData: { multipartFormData in
            
           if AppDelegate.sharedInstance().strattachimgorNot == "yes"
                     {
                           multipartFormData.append(UIImageJPEGRepresentation(image!, 0.5)!, withName: imageParam, fileName: "image.jpg", mimeType: "image/jpg")
                     }
               
         
            for (key, value) in parameters {
                //multipartFormData.append(value.data(using: .utf8 )! , withName: key)
                //multipartFormData.append(value.data(using: .utf8)!, withName: key)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to:sendOTPApi)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                   // print(response.result.value!)
                    hideLoader()
                    
                    
                    if response.result.value != nil
                        {
                                            let json = JSON(response.result.value!)
                                            completion(json,nil)
                                        }
                                        else
                                        {
                                            completion(nil,response.error)
                                         
                                        }
                    //let json1=JSON(response.result.value!)
                    
                
                }
           
            case .failure(let encodingError):
                print(encodingError)
               
                
            }
        }
        
        
        
    }
   
    /// makeApiRequest
    // MARK: - makeApiRequest
    /**
     *
     */
    func makeApiRequest(_ method: Alamofire.HTTPMethod = .get, url: String, parameters: [String: AnyObject]? = nil, urlEncoding: UrlEncoding = .DEFAULT, requestHeaders: [String: String]? = nil, requestSerialization: RequestSerialization = .DEFAULT, responseSerialization: ResponseSerialization = .DEFAULT, apiServiceType: APIServiceType = .DEFAULT, responseFormat: ResponseFormat = .DEFAULT, internet: Internet = .DEFAULT, errorState: ErrorState = .DEFAULT, loader: Loader = .DEFAULT, loaderText: String = "", sender: AnyObject? = nil, success_block: @escaping ResultSuccess, fail_block: @escaping ResultFail) -> Void
    {
        
        // log for request params
        var encodingError: Error? = nil
        do {
            if parameters != nil
            {
                let data = try JSONSerialization.data(withJSONObject: parameters!, options:  JSONSerialization.WritingOptions())
                let dataString = String(data: data, encoding: String.Encoding.utf8)! as String
                debugPrint(dataString)
            }
        }
        catch {
            
            encodingError = error
          //  debugPrint(encodingError!)
        }
        
        
        // MARK: internet
        self.checkInternetForAPIService(internet: internet)
        
        
        // MARK: loader
        self.show_APIService_Loader(loader: loader, loadingText: loaderText)
        
        
        // MARK: assign values to class objects
        self.constantSender = sender
        
        
        // MARK: assign values to success and fail blocks
        self.resultSuccess = success_block
        self.resultFail = fail_block
        
        
        // MARK: urlEncoding
        let requestUrl = self.encodedRequestUrl(url: url, urlEncoding: urlEncoding)
        
        
        //// MARK: assign request to class object
        //self.apiRequest = Alamofire.request(method, requestUrl, parameters: parameters)
        
        
        // MARK: apiServiceType
        self.useAPIServiceType(apiServiceType: apiServiceType)
        
        
        // MARK: requestSerialization
        self.serializeRequest(requestSerialization: requestSerialization)
        
        
        // MARK: startAPIRequestWithResponseSerialization
        self.startAPIRequestWithResponseSerialization(responseSerialization: responseSerialization, method: method, requestUrl: url, parameters: parameters, urlEncoding: urlEncoding, requestHeaders: requestHeaders, responseFormat: responseFormat, errorState: errorState, loader: loader)
        
    }
    
    //***********************************************************************
    
    /// checkInternetForAPIService
    // MARK: - checkInternetForAPIService
    func checkInternetForAPIService(internet: Internet)
    {
        
        // MARK: internet : DEFAULT, CHECK, DONTCHECK
        switch internet
        {
            
        case .DEFAULT:
            // No Internet Alert here
            break
            
            
        case .CHECK:
            // No Internet Alert here
            break
            
            
        case .DONTCHECK:
            break
            
            
            //default: break
            
        }
        
    }
    
    //***********************************************************************
    
    /// show_APIService_Loader
    // MARK: - show_APIService_Loader
    func show_APIService_Loader(loader: Loader, loadingText: String)
    {
        
        // MARK: Loder : DEFAULT, SHOW, DONTSHOW, HIDE, ANIMATING, CUSTOM
        switch loader
        {
            
        case .DEFAULT:
            
            let loadingText = (AppMethod.isNullString(AppString.Loading)) ? AppString.Loading : AppString.Loading
            showLoader(loadingText!)
            
            break
            
            
        case .SHOW:
            
            let loadingText = (AppMethod.isNullString(AppString.Loading)) ? AppString.Loading : AppString.Loading
            showLoader(loadingText!)
            
            break
            
            
        case .DONTSHOW:
            
            break
            
            
        case .HIDE:
            
            break
            
            
        case .ANIMATING:
            
            break
            
            
        case .CUSTOM:
            
            break
            
            
            //default: break
            
        }
        
    }
    
    /// hide_APIService_Loader
    // MARK: - hide_APIService_Loader
    func hide_APIService_Loader(loader: Loader)
    {
        
        // MARK: Loder : DEFAULT, SHOW, DONTSHOW, HIDE, ANIMATING, CUSTOM
        switch loader
        {
            
        case .DEFAULT:
            
           hideLoader()
            
            break
            
            
        case .SHOW:
            
           hideLoader()
            
            break
            
            
        case .DONTSHOW:
            
            break
            
            
        case .HIDE:
            
            break
            
            
        case .ANIMATING:
            
            break
            
            
        case .CUSTOM:
            
            break
            
            
            //default: break
            
        }
        
    }
    
    //***********************************************************************
    
    /// encodedRequestUrl
    // MARK: - encodedRequestUrl
    func encodedRequestUrl(url: String, urlEncoding: UrlEncoding) -> String
    {
        
        var requestUrl = url
        
        // MARK: UrlEncoding : DEFAULT, PERCENT
        switch urlEncoding
        {
            
        // MARK: - DEFAULT
        case .DEFAULT:
            
            break
            
            
        // MARK: - PERCENT
        case .PERCENT:
            
            requestUrl = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            break
            
            
            //default: break
            
        }
        
        return requestUrl
        
    }
    
    //***********************************************************************
    
    /// useAPIServiceType
    // MARK: - useAPIServiceType
    func useAPIServiceType(apiServiceType: APIServiceType)
    {
        
        // MARK: apiServiceType : DEFAULT, REST, SOAP
        switch apiServiceType
        {
            
        case .DEFAULT:
            
            break
            
            
        // MARK: - REST
        case .REST:
            
            break
            
            
        // MARK: - SOAP
        case .SOAP:
            
            break
            
            
            //default: break
            
        }
        
    }
    
    //***********************************************************************
    
    /// serializeRequest
    // MARK: - serializeRequest
    func serializeRequest(requestSerialization: RequestSerialization)
    {
        
        // MARK: requestSerialization : DEFAULT, JSON, XML
        switch requestSerialization
        {
            
        case .DEFAULT:
            
            break
            
            
        // MARK: - JSON
        case .JSON:
            
            break
            
            
        // MARK: - XML
        case .XML:
            
            break
            
        }
        
    }
    
    //***********************************************************************
    
    /// startAPIRequestWithResponseSerialization
    // MARK: - startAPIRequestWithResponseSerialization
    func startAPIRequestWithResponseSerialization(responseSerialization: ResponseSerialization, method: Alamofire.HTTPMethod = .get, requestUrl: String, parameters: [String: AnyObject]? = nil, urlEncoding: UrlEncoding = .DEFAULT, requestHeaders: [String: String]? = nil, responseFormat: ResponseFormat = .DEFAULT, errorState: ErrorState = .DEFAULT, loader: Loader = .DEFAULT)
    {
        
        // MARK: responseSerialization : DEFAULT, DATA, STRING, JSON, PROPERTYLIST, XML
        switch responseSerialization
        {
            
        // MARK: - JSON
        case .DEFAULT, .JSON:
            
            self.requestForJsonResponse(method, requestUrl: requestUrl, parameters: parameters, urlEncoding: urlEncoding, requestHeaders: requestHeaders, responseFormat: responseFormat, errorState: errorState, loader: loader)
            
            break
            
        // MARK: - DATA
        case .DATA, .XML:
            
            self.requestForXmlOrDataResponse(method, requestUrl: requestUrl, parameters: parameters, urlEncoding: urlEncoding, requestHeaders: requestHeaders, responseFormat: responseFormat, errorState: errorState, loader: loader)
            
            break
            
            
        // MARK: - STRING
        case .STRING:
            
            self.requestForStringResponse(method, requestUrl: requestUrl, parameters: parameters, urlEncoding: urlEncoding, requestHeaders: requestHeaders, responseFormat: responseFormat, errorState: errorState, loader: loader)
            
            break
            
            
        // MARK: - PROPERTYLIST
        case .PROPERTYLIST:
            
            self.requestForPlistResponse(method, requestUrl: requestUrl, parameters: parameters, urlEncoding: urlEncoding, requestHeaders: requestHeaders, responseFormat: responseFormat, errorState: errorState, loader: loader)
            
            break
            
            
            //default: break
            
        }
        
    }
    
    /// requestForJsonResponse
    // MARK: - requestForJsonResponse
    func requestForJsonResponse(_ method: Alamofire.HTTPMethod = .get, requestUrl: String, parameters: [String: AnyObject]? = nil, urlEncoding: UrlEncoding = .DEFAULT, requestHeaders: [String: String]? = nil, responseFormat: ResponseFormat = .DEFAULT, errorState: ErrorState = .DEFAULT, loader: Loader = .DEFAULT)
    {
        
        debugPrint(parameters!)
        
        debugPrint("API URL: \(requestUrl)")
        debugPrint("API Parameter: \(parameters!)")
        debugPrint("HTTP Method: \(method)")

        if method == .post {
            
            Alamofire.request(requestUrl, method: method, parameters: parameters, headers: requestHeaders).responseJSON (completionHandler: { response in
                
                //  debugPrint(response)
                
                var json : JSON? = nil
                if response.result.value != nil
                {
                    json = JSON(response.result.value!)
                    debugPrint(json)
                }
                
                var error : Error? = nil
                if response.result.error != nil
                {
                    error = response.result.error
                }
                
                
                hideLoader()
                
                // MARK: response.result : Success, Failure
                switch response.result
                {
                    
                //success
                case .success:
                    
                    self.successResponse(json, error: error, responseFormat: responseFormat, errorState: errorState, loader: loader)
                //failure error
                case .failure(let error):
                    if let error = response.result.error as? AFError {
                        self.failError(json, error: error, errorState: errorState, loader: loader)
                    }
                    else{
                        self.failError(json, error: error, errorState: errorState, loader: loader)
                    }
                    
                }
                
            })
        }
        else{            
            Alamofire.request(requestUrl, method: method, parameters: parameters, headers: requestHeaders).responseJSON (completionHandler: { response in
            
            //  debugPrint(response)
            
            var json : JSON? = nil
            if response.result.value != nil
            {
                
                json = JSON(response.result.value!)
                debugPrint(json)
            }
            
            var error : Error? = nil
            if response.result.error != nil
            {
                error = response.result.error
            }
            
            hideLoader()
            
            // MARK: response.result : Success, Failure
            switch response.result
            {
                
            //success
            case .success:
                
                self.successResponse(json, error: error, responseFormat: responseFormat, errorState: errorState, loader: loader)
                
            //failure error
            case .failure(let error):
                if let error = response.result.error as? AFError {
                    self.failError(json, error: error, errorState: errorState, loader: loader)
                }
                else{
                    self.failError(json, error: error, errorState: errorState, loader: loader)
                }

                
            }
            
            })
        }

    }
    
    /// requestForXmlOrDataResponse
    // MARK: - requestForXmlOrDataResponse
    func requestForXmlOrDataResponse(_ method: Alamofire.HTTPMethod = .get, requestUrl: String, parameters: [String: AnyObject]? = nil, urlEncoding: UrlEncoding = .DEFAULT, requestHeaders: [String: String]? = nil, responseFormat: ResponseFormat = .DEFAULT, errorState: ErrorState = .DEFAULT, loader: Loader = .DEFAULT)
    {
        
        Alamofire.request(requestUrl, parameters: parameters, encoding: JSONEncoding.default, headers: requestHeaders)
            .responseData(completionHandler: { response in
                
         //       debugPrint(response)
                let dict = try? XMLReader.dictionary(forXMLData: response.data) as NSDictionary
                
                
                var json : JSON? = nil
                if dict != nil
                {
                    json = JSON(dict!)
                }
                
                var error : Error? = nil
                if response.result.error != nil
                {
                    error = response.result.error
                }
                
                // MARK: response.result : Success, Failure
                switch response.result
                {
                    
                //success
                case .success:
                    
                    self.successResponse(json, error: error, responseFormat: responseFormat, errorState: errorState, loader: loader)
                    
                //failure error
                case .failure(let error):
                    
                    self.failError(json, error: error, errorState: errorState, loader: loader)
                    
                }
                
            })
        
    }
    
    /// requestForStringResponse
    // MARK: - requestForStringResponse
    func requestForStringResponse(_ method: Alamofire.HTTPMethod = .get, requestUrl: String, parameters: [String: AnyObject]? = nil, urlEncoding: UrlEncoding = .DEFAULT, requestHeaders: [String: String]? = nil, responseFormat: ResponseFormat = .DEFAULT, errorState: ErrorState = .DEFAULT, loader: Loader = .DEFAULT)
    {
        
        Alamofire.request(requestUrl, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: requestHeaders).responseString(completionHandler: { response in
            
            //debugPrint(response)
            
            var json : JSON? = nil
            if response.result.value != nil
            {
                json = JSON(response.result.value!)
            }
            
            var error : Error? = nil
            if response.result.error != nil
            {
                error = response.result.error
            }
            
            // MARK: response.result : Success, Failure
            switch response.result
            {
                
            //success
            case .success:
                
                self.successResponse(json, error: error, responseFormat: responseFormat, errorState: errorState, loader: loader)
                
            //failure error
            case .failure(let error):
                
                self.failError(json, error: error, errorState: errorState, loader: loader)
                
            }
            
        })
        
    }
    
    /// requestForPlistResponse
    // MARK: - requestForPlistResponse
    func requestForPlistResponse(_ method: Alamofire.HTTPMethod = .get, requestUrl: String, parameters: [String: AnyObject]? = nil, urlEncoding: UrlEncoding = .DEFAULT, requestHeaders: [String: String]? = nil, responseFormat: ResponseFormat = .DEFAULT, errorState: ErrorState = .DEFAULT, loader: Loader = .DEFAULT)
    {
        
        Alamofire.request(requestUrl, parameters: parameters, encoding: JSONEncoding.default, headers: requestHeaders).responsePropertyList(completionHandler: { response in
            
        //    debugPrint(response)
            
            var json : JSON? = nil
            if response.result.value != nil
            {
                json = JSON(response.result.value!)
            }
            
            var error : Error? = nil
            if response.result.error != nil
            {
                error = response.result.error
            }
            
            // MARK: response.result : Success, Failure
            switch response.result
            {
                
            //success
            case .success:
                
                self.successResponse(json, error: error, responseFormat: responseFormat, errorState: errorState, loader: loader)
                
            //failure error
            case .failure(let error):
                
                self.failError(json, error: error, errorState: errorState, loader: loader)
                
            }
            
        })
        
    }
    
    //***********************************************************************
    
    
    /// successResponse
    // MARK: - successResponse
    /**
     *
     */
    func successResponse(_ json:JSON? ,error:Error?, responseFormat: ResponseFormat, errorState: ErrorState, loader: Loader) -> Void
    {
        
        self.hide_APIService_Loader(loader: loader)
        
        if json != nil
        {
            
            if (self.resultSuccess != nil)
            {
                //var result = response.result.value!
                self.resultSuccess?(nil, self.constantSender, json ,error)
            }
            else
            {
           //     debugPrint("success_block is nil")
            }
            
        }
        else
        {
            self.failResponse(json, error: error, errorState: errorState)
        }
        
    }
    
    
    /// failResponse
    // MARK: - failResponse
    /**
     *
     */
    func failResponse(_ json:JSON? , error:Error?, errorState: ErrorState) -> Void
    {
        
        if (self.resultFail != nil)
        {
            self.failResponseByErrorState(errorState: errorState, json, error: error)
        }
        else
        {
            //debugPrint("fail_block is nil")
        }
        
    }
    
    
    /// failError
    // MARK: - failError
    /**
     *
     */
    func failError(_ json:JSON?, error : Error?, errorState: ErrorState, loader: Loader) -> Void
    {
        
        self.hide_APIService_Loader(loader: loader)
        
        if (self.resultFail != nil)
        {
            self.failResponseByErrorState(errorState: errorState, json, error: error)
        }
        else
        {
           // debugPrint("fail_block is nil")
        }
        
    }
    
    func failResponseByErrorState(errorState: ErrorState, _ json:JSON?, error : Error?)
    {
        
        // MARK: errorState : DEFAULT, PASSTOPARENT, DONTPASS
        switch errorState
        {
            
        case .DEFAULT:
            self.resultFail?(nil, nil, nil, error)
            debugPrint(error?.localizedDescription ?? "")
            break
            
            
        case .PASSTOPARENT:
            
            self.resultFail?(self.apiRequest, self.constantSender, json, error)
            
            break
            
            
        case .DONTPASS:
            
            break
            
            
            //default: break
            
        }
        
    }
    
    //***********************************************************************
    
}

//-----------------------------------------------------------------------

//-----------------------------------------------------------------------

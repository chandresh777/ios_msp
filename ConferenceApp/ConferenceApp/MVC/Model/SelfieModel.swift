//
//  SelfieModel.swift
//  ConferenceApp
//
//  Created by msp on 05/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

typealias responseGetSelfie = (_ response:GetSelfieRootClass) -> Void
typealias responseGetFrameList = (_ response:SelfieFrameListRootClass) -> Void

class SelfieModel: NSObject {

    var resultGetSelfie : GetSelfieRootClass? = nil
    var resultGetFrameList : SelfieFrameListRootClass? = nil

    static var selfieModel : SelfieModel? = nil
    var resultFail : ResultFail!
    
    class func sharedModel() -> SelfieModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if selfieModel == nil
        {
            selfieModel = SelfieModel()
        }
        return selfieModel!
    }
    
    
    func getSelfieList(user_id:String, conference_id:String,_ success_block: @escaping responseGetSelfie, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.getLinkAdsSponsorParam.conference_id: conference_id,
             APIsRequestParams.getLinkAdsSponsorParam.user_id: user_id
             
             ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.selfieList, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultGetSelfie = GetSelfieRootClass(fromJson:json!)
            success_block(self.resultGetSelfie!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    func getSelfiFrameList(conference_id:String,_ success_block: @escaping responseGetFrameList, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.getLinkAdsSponsorParam.conference_id: conference_id,
             
            ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getFrameList, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultGetFrameList = SelfieFrameListRootClass(fromJson:json!)
            success_block(self.resultGetFrameList!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }

    
}

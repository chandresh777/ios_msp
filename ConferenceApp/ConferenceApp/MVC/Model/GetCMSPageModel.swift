//
//  GetCMSPageModel.swift
//  ConferenceApp
//
//  Created by msp on 02/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
typealias responseCMSPage = (_ response:GetCMSRootClass) -> Void

class GetCMSPageModel: NSObject {
    
    var resultGetCMSPage : GetCMSRootClass? = nil
    
    static var getCMSPageModel : GetCMSPageModel? = nil
    var resultFail : ResultFail!
    
    
    class func sharedModel() -> GetCMSPageModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if getCMSPageModel == nil
        {
            getCMSPageModel = GetCMSPageModel()
        }
        return getCMSPageModel!
    }
    
    
    func getCMSPageLink(conference_id:String,_ success_block: @escaping responseCMSPage, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.getLinkAdsSponsorParam.conference_id: conference_id,
             
             ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getAllCmsPage, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultGetCMSPage = GetCMSRootClass(fromJson:json!)
            setDataInNSUser((self.resultGetCMSPage?.data)!, key: UserDefaultKey.GetCMSPage)
            success_block(self.resultGetCMSPage!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
}

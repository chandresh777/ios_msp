//
//  MyfavouriteModel.swift
//  ConferenceApp
//
//  Created by msp on 02/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias responseMyfavouriteDataModel = (_ response:MyfavouriteRoot) -> Void
class MyfavouriteModel: NSObject {
    var resultmyfavourite : MyfavouriteRoot? = nil
    
    static var myfavouritemodel : MyfavouriteModel? = nil
    var resultFail : ResultFail!
    
    
    class func sharedModel() -> MyfavouriteModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if myfavouritemodel == nil
        {
            myfavouritemodel = MyfavouriteModel()
        }
        return myfavouritemodel!
    }
    
    func getmyfavourite(conference_id:String,user_id:String,_ success_block: @escaping responseMyfavouriteDataModel, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.myfavouriteparam.conference_id: conference_id,
             APIsRequestParams.myfavouriteparam.user_id: user_id
             ] as [String : Any]
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getMyFavoritesUrl, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .SHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultmyfavourite = MyfavouriteRoot(fromJson:json!)
            
            success_block(self.resultmyfavourite!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
}

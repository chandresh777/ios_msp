//
//  AttendeesModel.swift
//  ConferenceApp
//
//  Created by msp on 02/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias responseAttendeesRootDataModel = (_ response:AttendeesRoot) -> Void
class AttendeesModel: NSObject {
    var resultattendees : AttendeesRoot? = nil
    
    static var attendesmodel : AttendeesModel? = nil
    var resultFail : ResultFail!
    
    
    class func sharedModel() -> AttendeesModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if attendesmodel == nil
        {
            attendesmodel = AttendeesModel()
        }
        return attendesmodel!
    }
    
    func getattendes(conference_id:String,_ success_block: @escaping responseAttendeesRootDataModel, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.getconferenceDateParam.conference_id: conference_id,
             ] as [String : Any]
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getAttendeesUrl, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultattendees = AttendeesRoot(fromJson:json!)
            
            success_block(self.resultattendees!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
}
}

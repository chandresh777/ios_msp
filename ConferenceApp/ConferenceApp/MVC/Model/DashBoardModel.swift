//
//  DashBoardModel.swift
//  ConferenceApp
//
//  Created by msp on 30/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
typealias responseAdsSponser = (_ response:GetAdsSponserRootClass) -> Void
typealias responseDashBoardRoot = (_ response:DashBoardRoot) -> Void
class DashBoardModel: NSObject {

    var resultAdsSponser : GetAdsSponserRootClass? = nil
    var resultDashBoardRoot : DashBoardRoot? = nil
    
    static var dashBoardModel : DashBoardModel? = nil
    var resultFail : ResultFail!
    
    class func sharedModel() -> DashBoardModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if dashBoardModel == nil
        {
            dashBoardModel = DashBoardModel()
        }
        return dashBoardModel!
    }
    
   
    
    
    func getAdsLink(user_id:String,sponsor_id:String,conference_id:String,is_flag:String,_ success_block: @escaping responseAdsSponser, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.getLinkAdsSponsorParam.user_id: user_id,
             APIsRequestParams.getLinkAdsSponsorParam.sponsor_id: sponsor_id,
             APIsRequestParams.getLinkAdsSponsorParam.conference_id: conference_id,
             APIsRequestParams.getLinkAdsSponsorParam.is_flag: is_flag,
             ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getLinkAdsSponsorUrl, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultAdsSponser = GetAdsSponserRootClass(fromJson:json!)
            
            success_block(self.resultAdsSponser!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }

    func updateStep(user_id:String,conference_id:String,steps:String,calorie:String,_ success_block: @escaping ResultSuccess, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.updateStepParam.user_id: user_id,
             APIsRequestParams.updateStepParam.conference_id: conference_id,
             APIsRequestParams.updateStepParam.steps: steps,
             APIsRequestParams.updateStepParam.calorie: calorie,
             ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.updateStepUrl, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            success_block(request, sender, json, error)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
    }
    
    
    
    
    
    func getdashBoard(user_id:String,conference_id:String,_ success_block: @escaping responseDashBoardRoot, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.dashboardparam.user_id: user_id,
           APIsRequestParams.dashboardparam.conference_id: conference_id
             
             ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getDashboardUrl, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultDashBoardRoot = DashBoardRoot(fromJson:json!)
            
            success_block(self.resultDashBoardRoot!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    
    
}

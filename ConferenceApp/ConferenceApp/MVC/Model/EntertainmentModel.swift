//
//  EntertainmentModel.swift
//  ConferenceApp
//
//  Created by msp on 02/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

typealias responseEntertainment = (_ response:EntertainmentRootClass) -> Void

class EntertainmentModel: NSObject {

    var resultEntertainment : EntertainmentRootClass? = nil
    static var entertainmentModel : EntertainmentModel? = nil
    var resultFail : ResultFail!
 
    class func sharedModel() -> EntertainmentModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if entertainmentModel == nil
        {
            entertainmentModel = EntertainmentModel()
        }
        return entertainmentModel!
    }
    
    
    func getEntertainmentList(conference_id:String,_ success_block: @escaping responseEntertainment, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.getLinkAdsSponsorParam.conference_id: conference_id,
             
             ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getEntertainment, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultEntertainment = EntertainmentRootClass(fromJson:json!)
            success_block(self.resultEntertainment!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
}

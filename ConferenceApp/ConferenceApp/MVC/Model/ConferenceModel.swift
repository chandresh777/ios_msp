//
//  ConferenceModel.swift
//  ConferenceApp
//
//  Created by msp on 30/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
typealias responseConferenceRoot = (_ response:ConferenceRoot) -> Void
class ConferenceModel: NSObject {
   
    
    var resultconferenceList : ConferenceRoot? = nil
    
    static var conferencemodel : ConferenceModel? = nil
    var resultFail : ResultFail!

    
    class func sharedModel() -> ConferenceModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if conferencemodel == nil
        {
            conferencemodel = ConferenceModel()
        }
        return conferencemodel!
    }
    func getConference(userid:String,_ success_block: @escaping responseConferenceRoot, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.updateProfile.user_id: userid,
            ] as [String : Any]
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getConferenceList, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultconferenceList = ConferenceRoot(fromJson:json!)
            
            success_block(self.resultconferenceList!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
}

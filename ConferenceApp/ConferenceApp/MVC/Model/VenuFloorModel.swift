//
//  VenuFloorModel.swift
//  ConferenceApp
//
//  Created by MSP on 13/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

typealias responseVenuFloorData = (_ response:VenuFloorData) -> Void
class VenuFloorModel: NSObject {
    var resultVenuFloorData : VenuFloorData? = nil
    var resultFail : ResultFail!
    static var venuFloorModel : VenuFloorModel? = nil
    
    
    class func sharedModel() -> VenuFloorModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if venuFloorModel == nil
        {
            venuFloorModel = VenuFloorModel()
        }
        return venuFloorModel!
    }
    
   
    //MARK: getVenuFloor
    //--------------------------
    func getVenuFloor(conference_id:String,_ success_block: @escaping responseVenuFloorData, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.getLinkAdsSponsorParam.conference_id: conference_id,
             
             ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getConferenceDetails, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .SHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultVenuFloorData = VenuFloorData(fromJson:json!)
            success_block(self.resultVenuFloorData!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
}

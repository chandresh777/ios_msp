//
//  OrganizerListModel.swift
//  ConferenceApp
//
//  Created by msp on 01/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
typealias responseOrganizerlistRoot = (_ response:OrganizationListRoot) -> Void
typealias responseOrganizerDetailRoot = (_ response:OrganizationDetailRoot) -> Void
class OrganizerListModel: NSObject {
    var resultorganizerList : OrganizationListRoot? = nil
     var resultorganizerDetail : OrganizationDetailRoot? = nil
    
    static var organizermodel : OrganizerListModel? = nil
    var resultFail : ResultFail!
    
    
    class func sharedModel() -> OrganizerListModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if organizermodel == nil
        {
            organizermodel = OrganizerListModel()
        }
        return organizermodel!
    }
    func getorganizerList(conference_id:String,_ success_block: @escaping responseOrganizerlistRoot, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.getconferenceDateParam.conference_id: conference_id,
             ] as [String : Any]
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getOrganizersUrl, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultorganizerList = OrganizationListRoot(fromJson:json!)
            
            success_block(self.resultorganizerList!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    
    
    func getorganizerDetail(user_id:String,_ success_block: @escaping responseOrganizerDetailRoot, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.setBookMarkParamsession.user_id: user_id,
             ] as [String : Any]
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getOrganizersDetailUrl, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultorganizerDetail = OrganizationDetailRoot(fromJson:json!)
            
            success_block(self.resultorganizerDetail!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
}

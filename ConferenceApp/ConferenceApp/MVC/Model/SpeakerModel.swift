//
//  SpeakerModel.swift
//  ConferenceApp
//
//  Created by msp on 31/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
typealias responseSpeakerlistRoot = (_ response:SpeakerListRoot) -> Void
typealias responseSpeakerDetailRoot = (_ response:SpeakerDetailRoot) -> Void
typealias responsebookmark = (_ response:BookmarkDataModel) -> Void

typealias responseaddnotes = (_ response:SessionAddNoteDataModel) -> Void

class SpeakerModel: NSObject {
    var resultspeakerList : SpeakerListRoot? = nil
     var resultspeakerdetail : SpeakerDetailRoot? = nil
    var resultbookmark : BookmarkDataModel? = nil
      var resultaddnotes : SessionAddNoteDataModel? = nil
    
    static var speakeremodel : SpeakerModel? = nil
    var resultFail : ResultFail!
    
    
    class func sharedModel() -> SpeakerModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if speakeremodel == nil
        {
            speakeremodel = SpeakerModel()
        }
        return speakeremodel!
    }
    
    func getspeakerList(conference_id:String,_ success_block: @escaping responseSpeakerlistRoot, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.getconferenceDateParam.conference_id: conference_id,
             ] as [String : Any]
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getSpeakersnURL, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultspeakerList = SpeakerListRoot(fromJson:json!)
            
            success_block(self.resultspeakerList!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    
    func getspeakerDetail(conference_id:String,speaker_id:String,user_id:String,_ success_block: @escaping responseSpeakerDetailRoot, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.getSpeakerDeatil.conference_id: conference_id,
             APIsRequestParams.getSpeakerDeatil.speaker_id: speaker_id,
             APIsRequestParams.getSpeakerDeatil.user_id: user_id
             ] as [String : Any]
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getSpeakerDetailsUrl, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .SHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultspeakerdetail = SpeakerDetailRoot(fromJson:json!)
            
            success_block(self.resultspeakerdetail!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    
    func setBookmarkSpeaker(user_id:String,speaker_id:String,conference_id:String,is_like:String,_ success_block: @escaping responsebookmark, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [
                APIsRequestParams.setBookMarkParamsSpeaker.conference_id: conference_id,
                APIsRequestParams.setBookMarkParamsSpeaker.speaker_id: speaker_id,
                APIsRequestParams.setBookMarkParamsession.user_id: user_id,
                APIsRequestParams.setBookMarkParamsession.is_like: is_like
            ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.saveBookmarkForSpeakerUrl, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .SHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultbookmark = BookmarkDataModel(fromJson:json!)
            
            success_block(self.resultbookmark!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
//    "note_id:8 (if we need to edit)
//    user_id:95
//    conference_id:1
//    note:helllo123
//    speker_id:1"""
    
    func addnotesSpeaker(user_id:String,speaker_id:String,conference_id:String,note_id:String,note:String,_ success_block: @escaping responseaddnotes, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [
                APIsRequestParams.setnoteSpeaker.conference_id: conference_id,
                APIsRequestParams.setnoteSpeaker.speaker_id: speaker_id,
                APIsRequestParams.setnoteSpeaker.user_id: user_id,
                APIsRequestParams.setnoteSpeaker.note: note,
                 APIsRequestParams.setnoteSpeaker.note_id: note_id
            ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.saveNoteForSpeakerUrl, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .SHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultaddnotes = SessionAddNoteDataModel(fromJson:json!)
            
            success_block(self.resultaddnotes!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    
}

//
//  SponsorModel.swift
//  ConferenceApp
//
//  Created by msp on 03/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
typealias responsesponsorListModel = (_ response:SponsorListRoot) -> Void
typealias responsesponsorNewListModel = (_ response:SponsorNewRootClass) -> Void
class SponsorModel: NSObject {
    var resultsponsorlist : SponsorListRoot? = nil
    var resultsponsorNewlist : SponsorNewRootClass? = nil
    static var sponsormodel : SponsorModel? = nil
    var resultFail : ResultFail!
    
    
    class func sharedModel() -> SponsorModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if sponsormodel == nil
        {
            sponsormodel = SponsorModel()
        }
        return sponsormodel!
    }
    
    
    func getsponsorList(conference_id:String,user_id:String,_ success_block: @escaping responsesponsorListModel, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.myfavouriteparam.conference_id: conference_id,
             APIsRequestParams.myfavouriteparam.user_id: user_id
            ] as [String : Any]
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getMyFavoritesUrl, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .SHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultsponsorlist = SponsorListRoot(fromJson:json!)
            
            success_block(self.resultsponsorlist!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    
    
    func getsponsorNewList(conference_id:String,_ success_block: @escaping responsesponsorNewListModel, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.myfavouriteparam.conference_id: conference_id,
//             APIsRequestParams.myfavouriteparam.user_id: user_id
            ] as [String : Any]
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getSponsorsUrl, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .SHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultsponsorNewlist = SponsorNewRootClass(fromJson:json!)
            
            success_block(self.resultsponsorNewlist!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
}

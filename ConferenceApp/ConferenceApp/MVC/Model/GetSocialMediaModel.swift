//
//  GetSocialMediaModel.swift
//  ConferenceApp
//
//  Created by msp on 02/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

typealias responseSocialMedia = (_ response:getSocialMediaRootClass) -> Void

class GetSocialMediaModel: NSObject {

    var resultGetSocialMedia : getSocialMediaRootClass? = nil
    
    static var getSocialMediaModel : GetSocialMediaModel? = nil
    var resultFail : ResultFail!
    
    class func sharedModel() -> GetSocialMediaModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if getSocialMediaModel == nil
        {
            getSocialMediaModel = GetSocialMediaModel()
        }
        return getSocialMediaModel!
    }
    
    
    func getSocialMediaURL(conference_id:String,_ success_block: @escaping responseSocialMedia, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.getLinkAdsSponsorParam.conference_id: conference_id,
             
             ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getSocialMediaUrls, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultGetSocialMedia = getSocialMediaRootClass(fromJson:json!)
//            setDataInNSUser((self.resultGetCMSPage?.data)!, key: UserDefaultKey.GetCMSPage)
            success_block(self.resultGetSocialMedia!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
}

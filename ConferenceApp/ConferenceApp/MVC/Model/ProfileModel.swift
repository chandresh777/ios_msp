//
//  ProfileModel.swift
//  ConferenceApp
//
//  Created by msp on 29/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

typealias responseCategoryOfProfile = (_ response:ProfileCategoryRootClass) -> Void
typealias responseSubCategoryOfProfile = (_ response:ProfileSubCategoryRootClass) -> Void
typealias responseProfile = (_ response:LoginRootClass) -> Void


class ProfileModel: NSObject {
    var resultProfileCategory : ProfileCategoryRootClass? = nil
    var resultProfileSubCategory : ProfileSubCategoryRootClass? = nil
    
    var resultProfilelogin : LoginRootClass? = nil

    static var profileModel : ProfileModel? = nil
    var resultFail : ResultFail!

    
    /*!
     * MARK: Singleton Class method
     * Model Instance will be created only once
     */
    class func sharedModel() -> ProfileModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if profileModel == nil
        {
            profileModel = ProfileModel()
        }
        return profileModel!
    }

    func getCategoryAPI(_ success_block: @escaping responseCategoryOfProfile, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        let parameters : [String : Any] = [:]
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.get, url: API.profileCategoryAPI, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultProfileCategory = ProfileCategoryRootClass(fromJson:json!)
            
            success_block(self.resultProfileCategory!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    func getSubCategoryAPI(_ success_block: @escaping responseSubCategoryOfProfile, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        let parameters : [String : Any] = [:]
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.profileSubCategoryAPI, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultProfileSubCategory = ProfileSubCategoryRootClass(fromJson:json!)
            
            success_block(self.resultProfileSubCategory!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    
    
    
    func UpdateProfileImageCallApi(first_name:String,last_name:String,address:String,mobile_no:String,gender:String,email:String,category_id:String,specialize_id:String,user_id:String,attachimgYN:String,profile_img:UIImage?,weight:String,height:String,_ success_block: @escaping responseProfile, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
       
        let parameters =
            [APIsRequestParams.updateProfile.first_name:first_name,
             APIsRequestParams.updateProfile.last_name:last_name,
             APIsRequestParams.updateProfile.address:address,
             APIsRequestParams.updateProfile.mobile_no:mobile_no,
             APIsRequestParams.updateProfile.gender:gender,
             APIsRequestParams.updateProfile.email:email,
             APIsRequestParams.updateProfile.category_id:category_id,
             APIsRequestParams.updateProfile.specialize_id:specialize_id,
             APIsRequestParams.updateProfile.user_id:user_id,
             APIsRequestParams.updateProfile.weight:weight,
             APIsRequestParams.updateProfile.height:height
             ] as [String : Any]
        if attachimgYN == "yes"
        {
       AppDelegate.sharedInstance().strattachimgorNot = "yes"
        }
        else{
            AppDelegate.sharedInstance().strattachimgorNot = "no"
        }
        debugPrint(parameters)
        print(parameters)
        showLoader("")
        APIService().uploadImageWithMultiPart(sendOTPApi: API.updateProfile,parameters: parameters as [String : AnyObject], imageParam: APIsRequestParams.updateProfile.profile_image, image: profile_img, completion:{ json ,error in
             print(json)
            hideLoader()
            if error != nil
            {
                fail_block(nil, nil, json, error)
            }
            else
            {
                
                self.resultProfilelogin = LoginRootClass(fromJson:json!)
                
                if self.resultProfilelogin?.code == 1{
                    setDataInNSUser((self.resultProfilelogin?.data)!, key: UserDefaultKey.userLoginInfo)
                }
                success_block( self.resultProfilelogin!)
                
                
//                if(json?[responseDictConstant.codeKey].stringValue)!.isEqual(string:"1") // check status
//                {
//                    self.resultUserProfileData1 = UserProfileData(fromJson: json)
//                    success_block( self.resultUserProfileData1!)
//
//                }
//                else
//                {
//                    self.resultUserProfileData1 = UserProfileData(fromJson: json)
//                    success_block( self.resultUserProfileData1!)
//
//                }
            }
        })
    }

}

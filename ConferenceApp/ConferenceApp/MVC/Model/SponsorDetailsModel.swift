//
//  SponsorDetailsModel.swift
//  ConferenceApp
//
//  Created by msp on 03/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

typealias responseSponsorDetails = (_ response:SponsorsDetailsRootClass) -> Void

class SponsorDetailsModel: NSObject {

    var resultSponsorsDetails : SponsorsDetailsRootClass? = nil
    static var sponsorDetailsModel : SponsorDetailsModel? = nil
    var resultFail : ResultFail!
    
    class func sharedModel() -> SponsorDetailsModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if sponsorDetailsModel == nil
        {
            sponsorDetailsModel = SponsorDetailsModel()
        }
        return sponsorDetailsModel!
    }
    
    
    func getSponsorDetails(user_id:String,_ success_block: @escaping responseSponsorDetails, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [
             APIsRequestParams.myfavouriteparam.user_id: user_id
            ] as [String : Any]
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getSponsorsDetails, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .SHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultSponsorsDetails = SponsorsDetailsRootClass(fromJson:json!)
            
            success_block(self.resultSponsorsDetails!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
}

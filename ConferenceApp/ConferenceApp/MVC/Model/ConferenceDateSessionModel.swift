//
//  ConferenceDateSessionModel.swift
//  ConferenceApp
//
//  Created by msp on 31/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
typealias responseConferenceDate = (_ response:ConferenceDateListRoot) -> Void
typealias responseSessionDetail = (_ response:SessionDetailRootClass) -> Void
typealias responseSessionbookmark = (_ response:SessionBookMarkRoot) -> Void
typealias responseSessionList = (_ response:SessionListRoot) -> Void


typealias responseSessionAddNote = (_ response:SessionAddNoteDataModel) -> Void

class ConferenceDateSessionModel: NSObject {
    var resultConferenceDate : ConferenceDateListRoot? = nil
     var resultsessionDetail : SessionDetailRootClass? = nil
     var resultsessionbookmark : SessionBookMarkRoot? = nil
     var resultsessionList : SessionListRoot? = nil
    
    
     var resultsessionAddnotes : SessionAddNoteDataModel? = nil
    
    static var conferenceSessionDatamodel : ConferenceDateSessionModel? = nil
    var resultFail : ResultFail!
    
    class func sharedModel() -> ConferenceDateSessionModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if conferenceSessionDatamodel == nil
        {
            conferenceSessionDatamodel = ConferenceDateSessionModel()
        }
        return conferenceSessionDatamodel!
    }
    
    func getConferenceList(conference_id:String,_ success_block: @escaping responseConferenceDate, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [
             APIsRequestParams.getconferenceDateParam.conference_id: conference_id
           
             ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getConferenceSessionDate, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultConferenceDate = ConferenceDateListRoot(fromJson:json!)
            
            success_block(self.resultConferenceDate!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    func getSessionList(date:String,conference_id:String,_ success_block: @escaping responseSessionList, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [
                APIsRequestParams.getsessionList.conference_id: conference_id,
                APIsRequestParams.getsessionList.date: date
            ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getConferenceSessionDate, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            print(json)
            self.resultsessionList = SessionListRoot(fromJson:json!)
            
            success_block(self.resultsessionList!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    
    
    
    func getSessionDetail(session_id:String,conference_id:String,user_id:String,_ success_block: @escaping responseSessionDetail, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [
                APIsRequestParams.getsessionDetail.conference_id: conference_id,
                APIsRequestParams.getsessionDetail.session_id: session_id,
                 APIsRequestParams.updateProfile.user_id: user_id
            ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.getSessionDetailsUrl, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .SHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultsessionDetail = SessionDetailRootClass(fromJson:json!)
            
            success_block(self.resultsessionDetail!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    
    func setBookmarkSession(user_id:String,session_id:String,conference_id:String,is_like:String,_ success_block: @escaping responseSessionbookmark, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [
                APIsRequestParams.setBookMarkParamsession.conference_id: conference_id,
                APIsRequestParams.setBookMarkParamsession.session_id: session_id,
                APIsRequestParams.setBookMarkParamsession.user_id: user_id,
                APIsRequestParams.setBookMarkParamsession.is_like: is_like
            ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.saveBookmarkForSessionURL, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultsessionbookmark = SessionBookMarkRoot(fromJson:json!)
            
            success_block(self.resultsessionbookmark!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    
    func AddnotesSession(user_id:String,session_id:String,conference_id:String,notes:String,note_id:String,_ success_block: @escaping responseSessionAddNote, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [
                APIsRequestParams.setBookMarkParamsession.conference_id: conference_id,
                APIsRequestParams.setBookMarkParamsession.session_id: session_id,
                APIsRequestParams.setBookMarkParamsession.user_id: user_id,
                APIsRequestParams.setBookMarkParamsession.is_like: notes
            ] as [String : Any]
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.saveNoteForSessionUrl, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultsessionAddnotes = SessionAddNoteDataModel(fromJson:json!)
            
            success_block(self.resultsessionAddnotes!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    
    
    
}

//
//  LoginFlowModel.swift
//  ConferenceApp
//
//  Created by msp on 29/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias responseAppVersionDataModel = (_ response:AppVersionRootClass) -> Void
typealias responseLogin = (_ response:LoginRootClass) -> Void
typealias responseSendOtpData = (_ response:SendOtpData) -> Void

class LoginFlowModel: NSObject {
    var resultAppVersionDataModel : AppVersionRootClass? = nil
    var resultLoginResponse : LoginRootClass? = nil
    var resultSendOtpData : SendOtpData? = nil
    
    static var loginFlowModel : LoginFlowModel? = nil
    var resultFail : ResultFail!

    /*!
     * MARK: Singleton Class method
     * Model Instance will be created only once
     */
    class func sharedModel() -> LoginFlowModel
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if loginFlowModel == nil
        {
            loginFlowModel = LoginFlowModel()
        }
        return loginFlowModel!
    }

    func callAppVersionAPI(device:String,_ success_block: @escaping responseAppVersionDataModel, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.APPVersion.type: device,
            ] as [String : Any]

            
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.checkVersion, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultAppVersionDataModel = AppVersionRootClass(fromJson:json!)
            
            success_block(self.resultAppVersionDataModel!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }

    func callLoginAPI(mobile_no:String,_ success_block: @escaping responseLogin, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.LoginResponse.device_type: device_type,
             APIsRequestParams.LoginResponse.mobile_no:mobile_no,
             APIsRequestParams.LoginResponse.pushtoken:getDeviceToken()!,
             APIsRequestParams.LoginResponse.deviceId: AppMethod.DEVICEID!
             ] as [String : Any]
        
        
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.loginAPI, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .DONTSHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            print(json)
            self.resultLoginResponse = LoginRootClass(fromJson:json!)

            if self.resultLoginResponse?.code == 1{
                setDataInNSUser((self.resultLoginResponse?.data)!, key: UserDefaultKey.userLoginInfo)
            }
            
            
            success_block(self.resultLoginResponse!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    
    func callSendOtpApi(mobile_no:String,_ success_block: @escaping responseSendOtpData, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        let parameters =
            [APIsRequestParams.LoginResponse.mobile_no:mobile_no,
             ] as [String : Any]
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.sendOTP, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .SHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            
            self.resultSendOtpData = SendOtpData(fromJson:json!)
            
            success_block(self.resultSendOtpData!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
    
    func verifyCode(mobile_no:String,otp_number:String,_ success_block: @escaping responseLogin, fail_block: @escaping ResultFail)
    {
        
        self.resultFail = fail_block
        var parameters : [String : Any] = [:]
        
        parameters =
            [APIsRequestParams.LoginResponse.device_type: device_type,
             APIsRequestParams.LoginResponse.mobile_no:mobile_no,
             APIsRequestParams.LoginResponse.pushtoken:getDeviceToken()!,
             APIsRequestParams.LoginResponse.deviceId: AppMethod.DEVICEID!,
             APIsRequestParams.LoginResponse.otp_number: otp_number
                
            ] as [String : Any]
        
        
        
        
        let apiService = APIService()
        
        apiService.makeApiRequest(.post, url: API.verifyOtp, parameters: parameters as [String : AnyObject]?, requestHeaders: getAPIHeaders(isAuthorized: true), responseSerialization: .JSON, apiServiceType: .DEFAULT, responseFormat: .JSON, internet: .CHECK, loader: .SHOW, loaderText: "", sender: nil, success_block: { (request, sender, json, error) in
            print(json)
            self.resultLoginResponse = LoginRootClass(fromJson:json!)
            
            if self.resultLoginResponse?.code == 1{
                setDataInNSUser((self.resultLoginResponse?.data)!, key: UserDefaultKey.userLoginInfo)
            }
            
            
            success_block(self.resultLoginResponse!)
            
        },fail_block : { (request, sender, json, error) in
            
            fail_block(request, sender, json, error)
            
        })
        
    }
    
}

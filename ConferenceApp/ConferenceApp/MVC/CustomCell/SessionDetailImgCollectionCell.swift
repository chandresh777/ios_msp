//
//  SessionDetailImgCollectionCell.swift
//  ConferenceApp
//
//  Created by msp on 27/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class SessionDetailImgCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgv: UIImageView!
}

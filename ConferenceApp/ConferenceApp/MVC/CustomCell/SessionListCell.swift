//
//  SessionListCell.swift
//  ConferenceApp
//
//  Created by msp on 26/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class SessionListCell: UITableViewCell {

    @IBOutlet weak var lblSessionDate: UILabel!
    @IBOutlet weak var lblSessionName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  DraweMenuCell.swift
//  MSP-eCommerce
//
//  Created by msp on 27/07/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class DraweMenuCell: UITableViewCell {

    @IBOutlet weak var lblMenuTitle: UILabel!
    @IBOutlet weak var imgMenu: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

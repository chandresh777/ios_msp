//
//  AttendiesListCell.swift
//  ConferenceApp
//
//  Created by msp on 27/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class AttendiesListCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var btnMoreOpsn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

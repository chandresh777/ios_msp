//
//  SelfieCollectionViewCell.swift
//  ConferenceApp
//
//  Created by MSP on 31/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class SelfieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imgVSelfie: UIImageView!
    
}

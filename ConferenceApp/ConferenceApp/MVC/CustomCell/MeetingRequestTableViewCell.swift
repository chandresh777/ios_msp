//
//  MeetingRequestTableViewCell.swift
//  ConferenceApp
//
//  Created by MSP on 31/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class MeetingRequestTableViewCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblOccupation: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgDate: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  DrawerMenuHeaderView.swift
//  MSP-eCommerce
//
//  Created by msp on 27/07/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class DrawerMenuHeaderView: UIView {

    @IBOutlet weak var imgHeaderBG: UIImageView!
    @IBOutlet weak var viewStatusBar: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnProfile: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        
    }
    
}

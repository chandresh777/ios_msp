//
//  AboutConferenceViewController.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class AboutConfCell:UITableViewCell{
    @IBOutlet weak var viewTop:UIView!
    @IBOutlet weak var viewDetail:UIView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var imgNext:UIImageView!
}

class AboutConferenceViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tblAboutConference: UITableView!
    var arrListOfCMS : [String] = []
     @IBOutlet weak var imgvAds: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
 self.imgvAds.image = UIImage.animatedImage(withAnimatedGIFURL: URL(string: Gifurlads)!)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.imgvAds.addGestureRecognizer(tap)
        self.imgvAds.isUserInteractionEnabled = true
        
        // Do any additional setup after loading the view.
//        "" : "<p>asd<\/p>\r\n",
//        "" : "<p>asd<\/p>\r\n",
//        "about_conference" : "<p>asd<\/p>\r\n",
//        "" : "<p>asd<\/p>\r\n",
//        "app_tips" : "",
//        "" : "<p>asd<\/p>\r\n",
//        "" : "<p>as<\/p>\r\n",
//        "" : "<p>asd<\/p>\r\n",
//        "confrence_committee" : "<p>asd<\/p>\r\n",
//        "" : "<p>asd<\/p>\r\n",
//        "" : "<p>asd<\/p>\r\n",
//        "" : "",
//        "" : "<p>asd<\/p>\r\n"
        arrListOfCMS = ["History of Conference","History Of Organisation","Note From Chairman","Deadlines and fees","Disclaimer","Conference Secretariat","Address","Legal","Data Protection policy","Terms and Condition"]
        self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")

        setLayoutMethod()
    }
    
    //MARK: - TapGesture
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
//        let objCommanWebViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as! CommanWebViewController
//        objCommanWebViewController.strNavTitle = "Ads"
//        objCommanWebViewController.strUrl = GifurladsRedirect
//        self.navigationController?.pushViewController(objCommanWebViewController, animated: true)
        if let url = URL(string: GifurladsRedirect) {
            UIApplication.shared.open(url, options: [:]) }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "About Conference"
        
      
       

        tblAboutConference.dataSource = self
        tblAboutConference.delegate = self
        tblAboutConference.backgroundColor = .clear
        tblAboutConference.separatorStyle = .none
    }

    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        slideMenuController()?.toggleLeft()
    }
    
    
    //MARK: -Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellAboutConfCell = tableView.dequeueReusableCell(withIdentifier: "AboutConfCell") as! AboutConfCell
        
        cellAboutConfCell.viewTop.backgroundColor = colorset.whitetextColor
        
        cellAboutConfCell.viewDetail.backgroundColor = colorset.navigationBar
        
        cellAboutConfCell.lblTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        cellAboutConfCell.lblTitle.textColor = colorset.blacktextcolor
        cellAboutConfCell.lblTitle.text = arrListOfCMS[indexPath.row]
        
        cellAboutConfCell.imgNext.image = #imageLiteral(resourceName: "imgrightarrow")
        cellAboutConfCell.imageView?.contentMode = .scaleAspectFit
        
        cellAboutConfCell.selectionStyle = .none
        return cellAboutConfCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objCommanWebViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as! CommanWebViewController
        objCommanWebViewController.isFromMenu = false
        objCommanWebViewController.strNavTitle = arrListOfCMS[indexPath.row]
        if indexPath.row == 0 {
            objCommanWebViewController.strCMS = getCMSPageListData()?.historyConference
        }else if indexPath.row == 1{
            objCommanWebViewController.strCMS = getCMSPageListData()?.historyOrganisation
        }else if indexPath.row == 2{
            objCommanWebViewController.strCMS = getCMSPageListData()?.confrenceCommittee
        }else if indexPath.row == 3{
            objCommanWebViewController.strCMS = getCMSPageListData()?.deadline
        }else if indexPath.row == 4{
            objCommanWebViewController.strCMS = getCMSPageListData()?.disclaimer
        }else if indexPath.row == 5{
            objCommanWebViewController.strCMS = getCMSPageListData()?.confrenceSecretariat
        }else if indexPath.row == 6{
            objCommanWebViewController.strCMS = getCMSPageListData()?.address
        }else if indexPath.row == 7{
            objCommanWebViewController.strCMS = getCMSPageListData()?.legal
        }else if indexPath.row == 8{
            objCommanWebViewController.strCMS = getCMSPageListData()?.dataProtection
        }else if indexPath.row == 9{
            objCommanWebViewController.strCMS = getCMSPageListData()?.termsCondition
        }
        self.navigationController?.pushViewController(objCommanWebViewController, animated: true)
    }

}

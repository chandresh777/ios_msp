//
//  NumberVerifyViewController.swift
//  ConferenceApp
//
//  Created by msp on 26/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
import MaterialTextField


class NumberVerifyViewController: BaseViewController, UIScrollViewDelegate, UITextFieldDelegate, KWVerificationCodeViewDelegate {
    @IBOutlet var lblCounter: UILabel!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblMobileNum: UILabel!
    
    @IBOutlet weak var lblThankYou: UILabel!
    @IBOutlet weak var viewTxtVerifyCode: KWVerificationCodeView!
    @IBOutlet weak var viewTextFieldSep: UIView!
    @IBOutlet weak var btnVerifyCode: btnThemClass!
    @IBOutlet weak var txtEnterCode: MFTextField!
    
    var phoneNo : String = ""
    
    var resultOfLoginResponse : LoginRootClass? = nil
    var resultOfSendOtp : SendOtpData? = nil
    
    
    @IBOutlet weak var viewBottom: UIView!
    //    @IBOutlet weak var btnChangeNum: UIButton!
    
    @IBOutlet var lblDidnt: UILabel!
    @IBOutlet weak var viewSep: UIView!
    @IBOutlet weak var btnResendCode: UIButton!
    var counter = 60
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        lblCounter.text = ""
        self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        setLayout()
        lblCounter.isHidden = true
        var _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        
    }
    
    @objc func updateCounter() {
        //you code, this is an example
        if counter > 0 {
            print("\(counter) seconds to the end of the world")
            counter -= 1
            lblCounter.isHidden = true
            lblCounter.text = "\(counter)"
        }else {
            
            lblCounter.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: -Custom Access Methods
    func setLayout() {
        self.headerView.lblHeader.text = "Verify Mobile"
        
        scrollView.delegate = self
        
        scrollView.translatesAutoresizingMaskIntoConstraints = true
        scrollView.frame = CGRect(x: 0, y: self.headerView.frame.origin.y + self.headerView.frame.size.height, width: screenWidth, height: screenHeight - self.headerView.frame.origin.y + self.headerView.frame.size.height)
        scrollView.showsVerticalScrollIndicator = false
        
        viewTop.backgroundColor = colorset.navigationBar
        
//        imgIcon.backgroundColor = colorset.themeColor
        
        lblMobileNum.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 20.0))
        lblMobileNum.textColor = colorset.blacktextcolor
        lblMobileNum.text = "+91\(phoneNo)"
        
        lblThankYou.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 20.0))
        lblThankYou.textColor = colorset.blacktextcolor
        lblThankYou.text = CommenMessage.NumberVerify
        
        viewTxtVerifyCode.delegate = self
        viewTxtVerifyCode.digits = 5
        
        txtEnterCode.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 18.0))
        txtEnterCode.textColor = colorset.blacktextcolor
        txtEnterCode.delegate = self
        addToolBar(textField: txtEnterCode)
        
        btnVerifyCode.setTitle("SUBMIT", for: .normal)
        
        //        viewBottom.backgroundColor = colorset.navigationBar
        
        //        btnResendCode.setTitle("RESEND CODE", for: .normal)
        //        btnResendCode.setTitleColor(colorset.statusBarColor, for: .normal)
        //        btnResendCode.titleLabel?.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        
        //        btnChangeNum.setTitle("CHANGE NUMBER", for: .normal)
        //        btnChangeNum.setTitleColor(colorset.statusBarColor, for: .normal)
        //        btnChangeNum.titleLabel?.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        
        //        viewSep.backgroundColor = colorset.sepretorColor
        
        btnVerifyCode.addTarget(self, action: #selector(btnVerifyNumberTapped), for: .touchUpInside)
    }
    
    //MARK: -API Call
    func sendOtpApi() {
        LoginFlowModel.sharedModel().callSendOtpApi(mobile_no: phoneNo, { (response) in
            if response.code == 1{
                self.resultOfSendOtp = response
                self.counter = 0
                self.counter = 60
                var _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCounter), userInfo: nil, repeats: true)
            }else {
                
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
                
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.sendOtpApi()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    
    //MARK: -API Call
    func callVerifyOtp() {
        LoginFlowModel.sharedModel().verifyCode(mobile_no: phoneNo,otp_number: viewTxtVerifyCode.getVerificationCode(), { (response) in
            if response.code == 1{
                self.resultOfLoginResponse = response
                if self.resultOfLoginResponse?.data.isProfileFilled == "1"
                {
                    Utility.moveToHomeViewCotroller()
                }
                else{
                    let objNumberVerifyVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                    objNumberVerifyVC.strpresent = "no"
                    AppDelegate.sharedInstance().appfistTimeLogin = "yes"
                    
                    self.navigationController?.pushViewController(objNumberVerifyVC, animated: true)
                }
                
            }else {
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
                
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.callVerifyOtp()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    
    
    //MARK: -Button Clicked Events
    @objc func btnVerifyNumberTapped() {
        self.view.endEditing(true)
        if viewTxtVerifyCode.getVerificationCode() == ""
        {
            self.popupAlert(title: AppName.appName, message: ValidationMsg.addVerificationCode, actionTitles: ["Ok"], actions:[{action1 in
                
                }, nil])
        }
        else{
            
            self.callVerifyOtp()
            //            Utility.moveToHomeViewCotroller()
        }
    }
    
    @IBAction func btnResendOtp(_ sender: UIButton) {
        
        self.sendOtpApi()
        
        
        
        
    }
    
    override func btnLeftBtnTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: -UITextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.4) {
            self.viewMain.translatesAutoresizingMaskIntoConstraints = true
            self.viewMain.frame.origin.y = -216
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.4) {
            self.viewMain.translatesAutoresizingMaskIntoConstraints = true
            self.viewMain.frame.origin.y = 0
        }
    }
    
    func didChangeVerificationCode() {
        print("Done Code\(viewTxtVerifyCode.getVerificationCode())")
    }
    
    //MARK: -Scrollview Delegate Methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0{
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
    }
}

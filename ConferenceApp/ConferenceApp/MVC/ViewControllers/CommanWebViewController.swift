//
//  CommanWebViewVC.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class CommanWebViewController: BaseViewController,UIWebViewDelegate {
    @IBOutlet weak var web_view: UIWebView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var strUrl:String! = ""
    var strCMS : String! = ""
    
    var strNavTitle : String = ""
    var isFromMenu : Bool!
    @IBOutlet var view_Bootom : UIView!
    @IBOutlet var imgBanner : UIImageView!
    @IBOutlet var layoutForBootom : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if isFromMenu == true{
            self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")
        }else{
            self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        }
        
        setLayout()
    }
    
    //MARK: - Custom Access Methods
    func setLayout()  {
        
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        web_view.delegate = self
        web_view.backgroundColor = colorset.whitetextColor
        
        if strCMS.isEqual(string: ""){
//            var str: String = strNavTitle
//            str.removeLast(4)
            self.headerView.lblHeader.text = strNavTitle
            let url = URL(string: strUrl)
            let requestObj = URLRequest(url: url! as URL)
            web_view.loadRequest(requestObj)
            view_Bootom.isHidden = true
            layoutForBootom.constant = 0
        }else{
            self.headerView.lblHeader.text = strNavTitle
            if ((strNavTitle.isEqual(string: "Local Tips")) || (strNavTitle.isEqual(string: "App Tips") ) || strNavTitle.isEqual(string: "Commite Members")){
                view_Bootom.isHidden = false
                imgBanner.image = UIImage.animatedImage(withAnimatedGIFURL: URL(string: Gifurlads)!)
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                self.imgBanner.addGestureRecognizer(tap)
                self.imgBanner.isUserInteractionEnabled = true
                layoutForBootom.constant = 60
            }else{
                view_Bootom.isHidden = true
                layoutForBootom.constant = 0
            }
            loadHtmlCode()
        }
        
        
        
    }
    
    //MARK: - TapGesture
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
//        let objCommanWebViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as! CommanWebViewController
//        objCommanWebViewController.strNavTitle = "Ads"
//        objCommanWebViewController.strUrl = GifurladsRedirect
//        self.navigationController?.pushViewController(objCommanWebViewController, animated: true)
        if let url = URL(string: GifurladsRedirect) {
            UIApplication.shared.open(url, options: [:]) }
        
    }
    
    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        if isFromMenu == true{
            slideMenuController()?.toggleLeft()
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    @available(iOS 2.0, *)
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        activityIndicator.isHidden = false
    }
    @available(iOS 2.0, *)
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        activityIndicator.isHidden = true
    }
    @available(iOS 2.0, *)
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        activityIndicator.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadHtmlCode() {
        let htmlCode = strCMS
        web_view.loadHTMLString(htmlCode!, baseURL: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

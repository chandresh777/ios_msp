//
//  AbstactDetailViewController.swift
//  ConferenceApp
//
//  Created by msp on 27/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class AbstactDetailViewController: BaseViewController {

    @IBOutlet weak var lblAbstactTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    
    @IBOutlet weak var lblInstrctTitle: UILabel!
    
    @IBOutlet weak var lblInstructDesc: UILabel!
    
    
    @IBOutlet weak var lblDateTitle: UILabel!
    
    
    @IBOutlet weak var btnAddAbstact: btnThemBlueClass!
    
    @IBOutlet weak var lblDateVal: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
setLayout()
        // Do any additional setup after loading the view.
    }
    func setLayout()
    {
         self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        self.headerView.lblHeader.text = "Abstract Detail"
        lblAbstactTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        lblDesc.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 18.0))
        lblInstrctTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
   lblInstructDesc.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 18.0))
    lblDateTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
    lblDateVal.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        lblDesc.text = "Generally, when iPhone releases a new  update, the new update will be automatically downloaded on your iOS devices when connecting to  Wi-Fi. It will not be strange for most iDevice users of the constant irritating reminders and p op-ups of iOS software updates, especially when your iPhone/iPad connecting to Wi-Fi.                However, iPhone, iPad or iPod Touch users sometimes do not want to install the updates for full storage space or the existing iOS system is great for them. Anyways, how to delete iOS upd ate (even delete iOS 12 update) on your iPhone/iPad/iPod Touch so as to save storage space or jus t avoid these nagging pop-ups and notifications? You are right here for it!          Most iOS users want to delete iOS update because update would occupy much storage space. They believe dele ting iOS 10.3/11 update would provide more space for favorite contents. However, generally, the  new iOS update would only occupy less than 200 MB which is not so huge, compared with your photos,  videos or music.              If iOS users need more space, in stead of deleting iOS upda      te, iMyFone iOS Cleaner (iOS 12 supported) can easily bring tons of storage space back. 200 MB is just piece of cake for iMyFone iPhone Cleaner or iMyFone iPhone Cleaner for Mac. Cleaning up storage space is one of the key features of iMyFone Umate Pro iOS Cleaner. As it is regarded as the all-in-one solution for space saving and privacy protection, and permanent erasure is another key feature."
        
        lblAbstactTitle.textColor = colorset.blacktextcolor
        lblDesc.textColor = colorset.lightBlackTextColor
        lblInstrctTitle.textColor = colorset.blacktextcolor
        lblInstructDesc.textColor = colorset.lightBlackTextColor
        lblDateTitle.textColor = colorset.blacktextcolor
        lblDateVal.textColor = colorset.lightBlackTextColor
    }
    override func btnLeftBtnTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

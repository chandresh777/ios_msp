//
//  OrganizationDetailViewController.swift
//  ConferenceApp
//
//  Created by msp on 27/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class OrganizationLocationCell:UITableViewCell{
    @IBOutlet weak var imgInfo:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
}

class OrganizationDetailCell:UITableViewCell{
    @IBOutlet weak var viewSep:UIView!
    @IBOutlet weak var lblDesc:UILabel!
    @IBOutlet var imgDescription: UIImageView!
}

class OrganizationDetailViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tblOrganizationDetail: UITableView!
    
    @IBOutlet weak var viewTblOrgHeader: UIView!
    @IBOutlet weak var viewSepForBooth: UIView!
    
    @IBOutlet weak var lblOrgTitle: UILabel!
    @IBOutlet weak var viewSep: UIView!
    @IBOutlet weak var viewBoothNo: UIView!
    @IBOutlet weak var lblBoothTitle: UILabel!
    @IBOutlet weak var lblBoothNo: UILabel!
    var strUserID:String!
    var organizationDetail:OrganizationDetailRoot!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLayout()
        self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        getOrganizationDetail()
    }

    
    
    func getOrganizationDetail()
    {
        OrganizerListModel.sharedModel().getorganizerDetail(user_id:strUserID, { (response) in
            if response.code == 1{
                self.organizationDetail = response
                self.setData()
                self.tblOrganizationDetail.dataSource = self
                self.tblOrganizationDetail.delegate = self
                self.tblOrganizationDetail.reloadData()
                
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.getOrganizationDetail()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: -Custom Access Methods
    func setLayout() {
        self.headerView.lblHeader.text = "Organizer Detail"
        
        tblOrganizationDetail.translatesAutoresizingMaskIntoConstraints = true
        tblOrganizationDetail.frame = CGRect(x: 0, y: self.headerView.frame.origin.y + self.headerView.frame.size.height, width: screenWidth, height: screenHeight - (self.headerView.frame.origin.y + self.headerView.frame.size.height))

        tblOrganizationDetail.backgroundColor = .clear
        tblOrganizationDetail.separatorStyle = .none
        
        lblOrgTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        lblOrgTitle.textColor = colorset.blacktextcolor
        
        
        viewSep.backgroundColor = colorset.sepretorColor
        
//        lblBoothTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
//        lblBoothTitle.textColor = colorset.blacktextcolor
//        lblBoothTitle.text = "Booth No."
//
//        lblBoothNo.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0  ))
//        lblBoothNo.textColor = colorset.lightBlackTextColor
//        lblBoothNo.text = "1"
        
//        viewSepForBooth.backgroundColor = colorset.sepretorColor
        
      
        
    }
    func setData()
    {
        
        lblOrgTitle.text = String(format: "%@ %@", organizationDetail.data.firstName,organizationDetail.data.lastName)
    }
    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK: -Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < 3{
            let objOrganizationLocationCell = tableView.dequeueReusableCell(withIdentifier: "OrganizationLocationCell") as! OrganizationLocationCell
            
            //objOrganizationLocationCell.imgInfo.backgroundColor = colorset.themeColor
            
            objOrganizationLocationCell.lblTitle.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
            objOrganizationLocationCell.lblTitle.numberOfLines = 0
           
           
            if indexPath.row == 0
            {
                objOrganizationLocationCell.imgInfo.image = UIImage(named: "loction")
                 objOrganizationLocationCell.lblTitle.text = organizationDetail.data.address ?? ""
            }
            else if indexPath.row == 1
            {
                objOrganizationLocationCell.imgInfo.image = UIImage(named: "email_icon")
                 objOrganizationLocationCell.lblTitle.text = organizationDetail.data.email ?? ""
            }else{
              objOrganizationLocationCell.imgInfo.image = UIImage(named: "phone_icon")
               objOrganizationLocationCell.lblTitle.text = organizationDetail.data.mobileNo ?? ""
            }
            
            
            objOrganizationLocationCell.lblTitle.textColor = colorset.lightBlackTextColor
            
            objOrganizationLocationCell.imgInfo.contentMode = .scaleAspectFit
            objOrganizationLocationCell.selectionStyle = .none
            return objOrganizationLocationCell
        }
        let objOrganizationDetailCell = tableView.dequeueReusableCell(withIdentifier: "OrganizationDetailCell") as! OrganizationDetailCell

        objOrganizationDetailCell.lblDesc.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        objOrganizationDetailCell.lblDesc.numberOfLines = 0
        objOrganizationDetailCell.lblDesc.text = organizationDetail.data.descriptionField ?? ""
        objOrganizationDetailCell.lblDesc.textColor = colorset.lightBlackTextColor

        objOrganizationDetailCell.viewSep.backgroundColor = colorset.sepretorColor
        
        objOrganizationDetailCell.selectionStyle = .none
        return objOrganizationDetailCell
    }
}


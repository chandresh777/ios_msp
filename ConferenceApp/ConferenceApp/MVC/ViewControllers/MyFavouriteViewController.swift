//
//  MyFavouriteViewController.swift
//  ConferenceApp
//
//  Created by msp on 29/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class MyFavouriteViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate {
    @IBOutlet weak var tblFavourite: UITableView!
    var arrSection:[String] = ["Hall A","Hall B","Hall C"]
    var arrrow:[[String]] = [["Session A1","Session A1","Session A1"],["Session B1","Session N1","Session A1"],["Session D1","Session C1"]]
    var arrowtemp:[[String]] = []
    
     var arropenArrow:[Int] = []
   
    @IBOutlet weak var imgvAds: UIImageView!
    
    var myfavouriteList:MyfavouriteRoot!
    var myfavouriteListsession:[MyfavouriteSessionList] = []
     var myfavouriteListspeaker:[MyfavouriteSpeakersList] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrowtemp = arrrow
     closealltab()
        // Do any additional setup after loading the view.
        headerView.lblHeader.text = "MY Favorites"
        self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        tblFavourite.tableFooterView = UIView()
         imgvAds.image = UIImage.animatedImage(withAnimatedGIFURL: URL(string: Gifurlads)!)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.imgvAds.addGestureRecognizer(tap)
        self.imgvAds.isUserInteractionEnabled = true

     getmyfavourite()
        

        // Do any additional setup after loading the view.
    }
    
    //MARK: - TapGesture
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
//        let objCommanWebViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as! CommanWebViewController
//        objCommanWebViewController.strNavTitle = "Ads"
//        objCommanWebViewController.strUrl = GifurladsRedirect
//        self.navigationController?.pushViewController(objCommanWebViewController, animated: true)
        if let url = URL(string: GifurladsRedirect) {
            UIApplication.shared.open(url, options: [:]) }
        
    }
    
    func getmyfavourite()
    {
       
        
        MyfavouriteModel.sharedModel().getmyfavourite(conference_id: AppDelegate.sharedInstance().conference.data[0].iD, user_id: AppDelegate.sharedInstance().userId, { (response) in
            if response.code == 1{
                self.myfavouriteList = response
                
                
                
               self.myfavouriteListsession = self.myfavouriteList.data.sessionList
                self.myfavouriteListspeaker = self.myfavouriteList.data.speakersList
                
//                for i in 0..<self.sessionList.data.count
//                {
//                    var arrsession:[Session] = []
//                    for j in 0..<self.sessionList.data[i].session.count
//                    {
//                        arrsession.append(self.sessionList.data[i].session[j])
//                    }
//                    self.sessionListTemp[i] = arrsession
//                }
                
                self.closealltab()
                self.tblFavourite.dataSource = self
                self.tblFavourite.delegate = self
                self.tblFavourite.reloadData()
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.getmyfavourite()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    
    
    //MARK: - UITableview delegate method
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 55
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 55))
        viewHeader.backgroundColor = colorset.navigationBar
        
        viewHeader.isUserInteractionEnabled = true
        let lbltitle = UILabel(frame: CGRect(x: 20, y: 15, width: 100, height: 30))
        lbltitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        lbltitle.textColor = colorset.blacktextcolor
        if section == 0
        {
            lbltitle.text = "Sessions"
        }
        else{
            lbltitle.text = "Speakers"
        }
        
        viewHeader.addSubview(lbltitle)
        
        
        let imgv = UIImageView(frame: CGRect(x: UIScreen.main.bounds.size.width - 40, y: 15, width:20, height: 20))
        imgv.contentMode = .scaleAspectFit
        if arropenArrow[section] == 1
        {
             imgv.image = UIImage(named: "imgdownarrow")
          
           
           
        }
        else{
            
           imgv.image = UIImage(named: "imgrightarrow")
        }
        
        
       
        viewHeader.tag = section
        viewHeader.addSubview(imgv)
        
        let viewwhite = UIView(frame: CGRect(x: 0, y: 50, width: UIScreen.main.bounds.size.width, height: 5))
        viewwhite.backgroundColor = UIColor.white
        
        viewHeader.addSubview(viewwhite)
        
        
        let tapGest = UITapGestureRecognizer(target: self, action: #selector(tapClick(_:)))
        tapGest.delegate = self
        viewHeader.addGestureRecognizer(tapGest)
        
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return myfavouriteList.data.sessionList.count
        }
        else{
            return myfavouriteList.data.speakersList.count
        }
        
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! favouriteCell
        
        cell.lblTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 16.0))
        
        cell.lblTitle.textColor = colorset.blacktextcolor
        
if indexPath.section == 0
{
        cell.lblTitle.text = myfavouriteList.data.sessionList[indexPath.row].name
        }
else{
   
 
    cell.lblTitle.text =  String(format: "%@ %@", myfavouriteList.data.speakersList[indexPath.row].firstName,myfavouriteList.data.speakersList[indexPath.row].lastName)
        }
        return cell
    }
    
    
    @objc func tapClick(_ gest:UITapGestureRecognizer)
    {
       if (gest.view?.tag)! == 0
       {
        
        if myfavouriteList.data.sessionList.count > 0
        {
            arropenArrow[(gest.view?.tag)!] = 0
            var arrindepath:[IndexPath]=[]
           
                for i in 0..<myfavouriteList.data.sessionList.count
                {
                    let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
                    arrindepath.append(indexpath)
                }
           
                myfavouriteList.data.sessionList = []
            tblFavourite.deleteRows(at: arrindepath, with: .top)
            
            tblFavourite.reloadData()
       
        }
       else{
        
            arropenArrow[(gest.view?.tag)!] = 1
            var arrindepath:[IndexPath]=[]
            myfavouriteList.data.sessionList = myfavouriteListsession
            for i in 0..<myfavouriteList.data.sessionList.count
            {
                let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
                arrindepath.append(indexpath)
            }
            
            tblFavourite.insertRows(at: arrindepath, with: .top)
            
            tblFavourite.reloadData()
            
            
        }
        }
       else{
        
        if myfavouriteList.data.speakersList.count > 0
        {
            arropenArrow[(gest.view?.tag)!] = 0
            var arrindepath:[IndexPath]=[]
            for i in 0..<myfavouriteList.data.speakersList.count
            {
                let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
                arrindepath.append(indexpath)
            }
             myfavouriteList.data.speakersList = []
            tblFavourite.deleteRows(at: arrindepath, with: .top)
            
            tblFavourite.reloadData()
        }
        else{
            
            arropenArrow[(gest.view?.tag)!] = 1
            var arrindepath:[IndexPath]=[]
             myfavouriteList.data.speakersList = myfavouriteListspeaker
            for i in 0..<myfavouriteList.data.speakersList.count
            {
                let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
                arrindepath.append(indexpath)
            }
            
            tblFavourite.insertRows(at: arrindepath, with: .top)
            
            tblFavourite.reloadData()
            
        }
        }
//        if arrrow[(gest.view?.tag)!].count > 0
//        {
//            arropenArrow[(gest.view?.tag)!] = 0
//            var arrindepath:[IndexPath]=[]
//            if (gest.view?.tag)! == 0
//            {
//            for i in 0..<myfavouriteList.data.sessionList.count
//            {
//                let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
//                arrindepath.append(indexpath)
//            }
//            }
//            else{
//
//                for i in 0..<myfavouriteList.data.speakersList.count
//                {
//                    let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
//                    arrindepath.append(indexpath)
//                }
//
//            }
//
//            if (gest.view?.tag)! == 0
//            {
//                myfavouriteList.data.sessionList = []
//            }
//            else{
//              myfavouriteList.data.speakersList = []
//            }
//
//            tblFavourite.deleteRows(at: arrindepath, with: .top)
//
//             tblFavourite.reloadData()
//
//
//        }
//        else{
//            arropenArrow[(gest.view?.tag)!] = 1
//            var arrindepath:[IndexPath]=[]
//
//            if (gest.view?.tag)! == 0
//            {
//                myfavouriteList.data.sessionList = myfavouriteListsession
//            }
//            else{
//              myfavouriteList.data.speakersList = myfavouriteListspeaker
//            }
//
//
//            if (gest.view?.tag)! == 0
//            {
//                for i in 0..<myfavouriteList.data.sessionList.count
//                {
//                    let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
//                    arrindepath.append(indexpath)
//                }
//            }
//            else{
//
//                for i in 0..<myfavouriteList.data.speakersList.count
//                {
//                    let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
//                    arrindepath.append(indexpath)
//                }
//
//            }
//            tblFavourite.insertRows(at: arrindepath, with: .top)
//
//            tblFavourite.reloadData()
//
//        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       if indexPath.section == 0
       {
        
        let sessionDetailVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "SessionDetailViewController") as! SessionDetailViewController
        sessionDetailVC.strsessionID = myfavouriteList.data.sessionList[indexPath.row].iD
        self.navigationController?.pushViewController(sessionDetailVC, animated: true)
        
       }
       else{
        let objSpeakerDetailVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "SpeakerDetailViewController") as! SpeakerDetailViewController
        objSpeakerDetailVC.struserId = myfavouriteList.data.speakersList[indexPath.row].iD
        self.navigationController?.pushViewController(objSpeakerDetailVC, animated: true)
        }
        
        
        //        let sessionDetailVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "SessionDetailViewController") as! SessionDetailViewController
        //
        //        self.navigationController?.pushViewController(sessionDetailVC, animated: true)
    }
    func closealltab()
    {
        var indexpath:[IndexPath]=[]
        arropenArrow.removeAll()
        for j in 0..<2
        {
            arropenArrow.append(1)
          
               // indexpath.append(IndexPath(item:i , section:j))
            arrrow[j] = []
            
            
           
        }
        //tblVAlbums.deleteRows(at: indexpath, with: .top)
        tblFavourite.reloadData()
    }

    override func btnLeftBtnTapped() {
       self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

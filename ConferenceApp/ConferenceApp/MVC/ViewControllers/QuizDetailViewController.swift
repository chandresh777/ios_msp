//
//  QuizDetailViewController.swift
//  ConferenceApp
//
//  Created by MSP on 31/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class QuizDetailViewController: BaseViewController {

    @IBOutlet var lblHeader: UILabel!
    @IBOutlet var lblName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        
        setLayoutMethod()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "Quiz Detail"
        lblName.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 30.0))
        lblHeader.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 17.0))
        lblName.textColor = colorset.blacktextcolor
        lblHeader.textColor = colorset.lightBlackTextColor
    }
    
    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  NotificationViewController.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class NotificationViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    var isFromMenu: Bool!

    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if isFromMenu{
            self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")
        }
        else{
            self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        }


        setLayoutMethod()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    //MARK:- TableView dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 20
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! dayOfGlanceCell
        cell.selectionStyle = .none
        cell.lblTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 17.0))
        cell.lblDesc.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 17.0))
        cell.lblTitle.textColor = colorset.blacktextcolor
        cell.lblDesc.textColor = colorset.lightBlackTextColor
        
        cell.lblTitle.text = "Title 1"
        cell.lblDesc.text = "New subscribers only. Plan automatically renews after trial.Group FaceTime will be available in iOS 12 later this year through a software update.Some AR apps shown may not be available in all regions or all languages.FaceTime will be available in iOS 12 later this year through a software update."
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc : NotificationDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "NotificationDetailViewController") as! NotificationDetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "Notification"
        
    }

    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        if isFromMenu{
            slideMenuController()?.toggleLeft()
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }

    //MARK:- Memory management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
//  SponsorDetailViewController.swift
//  ConferenceApp
//
//  Created by msp on 03/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
import SDWebImage

class SponsorLocationCell:UITableViewCell{
    @IBOutlet weak var imgInfo:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
}

class SponsorDetailCell:UITableViewCell{
    @IBOutlet weak var viewSep:UIView!
    @IBOutlet weak var lblDesc:UILabel!
}

class SponsorImageColCell: UICollectionViewCell {
    @IBOutlet weak var imgSponsor:UIImageView!
}

class SponsorDetailViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var tblSponsorDetail: UITableView!
    
    @IBOutlet weak var viewTblOrgHeader: UIView!
    @IBOutlet weak var viewSepForBooth: UIView!
    
    @IBOutlet weak var lblSponsorTitle: UILabel!
    @IBOutlet weak var imgSponsor : UIImageView!
    
    @IBOutlet weak var viewSep: UIView!
    @IBOutlet weak var viewSep1:UIView!
    @IBOutlet weak var viewBoothNo: UIView!
    @IBOutlet weak var lblBoothTitle: UILabel!
    @IBOutlet weak var lblBoothNo: UILabel! //Booth No.
    var strUserID:String!
    var objSponsorDetail:SponsorNewSponsor!
    
    @IBOutlet weak var colSponsorImage: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        // Do any additional setup after loading the view.
    }

    //MARK: -Custom Access Methods
    func setLayout() {
        self.headerView.lblHeader.text = "Sponsor Detail"
        
        tblSponsorDetail.translatesAutoresizingMaskIntoConstraints = true
        tblSponsorDetail.frame = CGRect(x: 0, y: self.headerView.frame.origin.y + self.headerView.frame.size.height, width: screenWidth, height: screenHeight - (self.headerView.frame.origin.y + self.headerView.frame.size.height))
        
        tblSponsorDetail.backgroundColor = .clear
        tblSponsorDetail.separatorStyle = .none
        
        lblSponsorTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        lblSponsorTitle.textColor = colorset.blacktextcolor
        
        lblBoothTitle.font = UIFont (name: FONT.Bold, size: FontSizeFunc(size: 23.0))
        lblBoothTitle.textColor = colorset.blacktextcolor
        
        lblBoothNo.font = UIFont (name: FONT.Regular, size: FontSizeFunc(size: 20.0))
        lblBoothNo.textColor = colorset.lightBlackTextColor
        
        
        viewSep.backgroundColor = colorset.sepretorColor
//        viewSep1.backgroundColor = colorset.sepretorColor
//        getSponsorDetail()
        
        //        lblBoothTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        //        lblBoothTitle.textColor = colorset.blacktextcolor
        //        lblBoothTitle.text = "Booth No."
        //
        //        lblBoothNo.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0  ))
        //        lblBoothNo.textColor = colorset.lightBlackTextColor
        //        lblBoothNo.text = "1"
        
        viewSepForBooth.backgroundColor = colorset.sepretorColor
        
        
        setData()
    }
    func setData()
    {
        
        lblSponsorTitle.text = String(format: "%@ %@", objSponsorDetail.firstName,objSponsorDetail.lastName)
        lblBoothTitle.text = String (format: "Booth No. %@", objSponsorDetail.catName)
        lblBoothNo.text = objSponsorDetail.specializeId
//        imgSponsor.sd_setImage(with: URL(string: objSponsorDetail.profileImage), placeholderImage: UIImage (named: "imgLoginLogo"))
//        cell.imgv.sd_setImage(with: URL(string: strimgUrl ), placeholderImage: imgPlaceHolder)
        
        self.tblSponsorDetail.dataSource = self
        self.tblSponsorDetail.delegate = self
        self.tblSponsorDetail.reloadData()
        
        self.colSponsorImage.dataSource = self
        self.colSponsorImage.delegate = self
    }
    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - API Call
    func getSponsorDetail()
    {
        SponsorDetailsModel().getSponsorDetails(user_id: "84",{(response) in
            if response.code == 1{
//                self.objSponsorDetail = response
                self.setData()
                self.tblSponsorDetail.reloadData()
                
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.getSponsorDetail()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    
    
    //MARK: -Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 && objSponsorDetail.address == ""{
            return 0
        }
        else if indexPath.row == 1 && objSponsorDetail.mobileNo == ""{
            return 0
        }
        else if indexPath.row == 2 && objSponsorDetail.address == ""{
            return 0
        }

        return tableView.estimatedRowHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < 3{
            let objSponsorLocationCell = tableView.dequeueReusableCell(withIdentifier: "SponsorLocationCell") as! SponsorLocationCell
            
            //objOrganizationLocationCell.imgInfo.backgroundColor = colorset.themeColor
            
            objSponsorLocationCell.lblTitle.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
            objSponsorLocationCell.lblTitle.numberOfLines = 0
            
            
            if indexPath.row == 0
            {
                if objSponsorDetail.address != ""{
                    objSponsorLocationCell.imgInfo.image = UIImage(named: "loction")
                    objSponsorLocationCell.lblTitle.text = objSponsorDetail.address ?? ""
                }
                
            }
            else if indexPath.row == 1
            {
                if objSponsorDetail.email != ""{
                    objSponsorLocationCell.imgInfo.image = UIImage(named: "email_icon")
                    objSponsorLocationCell.lblTitle.text = objSponsorDetail.email ?? ""
                }
            }else{
                if objSponsorDetail.mobileNo != ""{
                    objSponsorLocationCell.imgInfo.image = UIImage(named: "phone_icon")
                    objSponsorLocationCell.lblTitle.text = objSponsorDetail.mobileNo ?? ""
                }
            }
            
            
            objSponsorLocationCell.lblTitle.textColor = colorset.lightBlackTextColor
            
            objSponsorLocationCell.imgInfo.contentMode = .scaleAspectFit
            objSponsorLocationCell.selectionStyle = .none
            return objSponsorLocationCell
        }
        let objSponsorDetailCell = tableView.dequeueReusableCell(withIdentifier: "SponsorDetailCell") as! SponsorDetailCell
        
        objSponsorDetailCell.lblDesc.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        objSponsorDetailCell.lblDesc.numberOfLines = 0
        objSponsorDetailCell.lblDesc.text = "hjdfhkjhsdfkljdhsklghdfksgjhdflskghdfklsjghdfklsjghldjsfghkdfsjghldfksghkdfsgjdfhlgjdfhsklgjdfshlgjhdfvhdffghndfj"
        objSponsorDetailCell.lblDesc.textColor = colorset.lightBlackTextColor
        
        objSponsorDetailCell.viewSep.backgroundColor = colorset.sepretorColor
        
        
        objSponsorDetailCell.selectionStyle = .none
        return objSponsorDetailCell
    }
    
    //MARK: -CollectionView Delegate Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let objSponsorImageColCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SponsorImageColCell", for: indexPath) as! SponsorImageColCell
        
        objSponsorImageColCell.imgSponsor.contentMode = .scaleToFill
        objSponsorImageColCell.imgSponsor.clipsToBounds = true
        objSponsorImageColCell.imgSponsor.image = #imageLiteral(resourceName: "imgSelfieFrame")
        
        return objSponsorImageColCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenWidth - 30, height: colSponsorImage.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 5, 0, 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  MeetingRequestViewController.swift
//  ConferenceApp
//
//  Created by MSP on 31/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class MeetingRequestViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    var customPopUPView:CustomAlertPopView!

    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        
        setLayoutMethod()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    //MARK:- TableView dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 10
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MeetingRequestTableViewCell
        cell.selectionStyle = .none
        cell.lblName.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 17.0))
        cell.lblOccupation.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 15.0))
        cell.lblDate.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 15.0))
        cell.lblName.textColor = colorset.blacktextcolor
        cell.lblOccupation.textColor = colorset.lightBlackTextColor
        cell.lblDate.textColor = colorset.lightBlackTextColor
        cell.lblName.text = "Saran Swader"
        cell.lblOccupation.text = "Doctor"
        cell.lblDate.text = "03 NOV 2018"

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        customPopUPView = Bundle.main.loadNibNamed("CustomAlertPopView", owner: self, options: nil)![0] as! CustomAlertPopView
        customPopUPView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.size.height)
        self.view.addSubview(customPopUPView)
        customPopUPView.isHidden = false
        customPopUPView.lblAlertMessage.isHidden = false
        customPopUPView.txtEnterCode.isHidden = true
//        customPopUPView.lblAlertMessage.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 17.0))
        customPopUPView.lblAlertTitle.text = "Meeting Requests"
        customPopUPView.lblAlertMessage.text = "I want to meet you. and need to discuss about ...?"
        customPopUPView.btnOkey.setTitle("Accept", for: .normal)
        customPopUPView.btnCancel.setTitle("Reject", for: .normal)
        customPopUPView.btnCancel.addTarget(self, action: #selector(btnclickRejectPOp(_:)), for: .touchUpInside)
        customPopUPView.btnOkey.addTarget(self, action: #selector(btnclickAcceptPOp(_:)), for: .touchUpInside)
    }
    
    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "Meeting Requests"
        
    }
    
    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - POPUP Action
    @objc func btnclickRejectPOp(_ sender:UIButton)
    {
        customPopUPView.isHidden = true
    }
    @objc func btnclickAcceptPOp(_ sender:UIButton)
    {
        customPopUPView.isHidden = true
    }
    //MARK:- Memory management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
//  MySelfieViewController.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
import SDWebImage

class MySelfieViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var isFromMenu: Bool!

    var objGetSelfieList : GetSelfieRootClass? = nil
    @IBOutlet var colMySelfie : UICollectionView!
    @IBOutlet weak var imgvAds: UIImageView!
    
    var imgSelfie : UIImage!
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if isFromMenu{
            self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")
        }
        else{
            self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        }
        
        setLayoutMethod()
        imgvAds.image = UIImage.animatedImage(withAnimatedGIFURL: URL(string: Gifurlads)!)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.imgvAds.addGestureRecognizer(tap)
        self.imgvAds.isUserInteractionEnabled = true

    }

    //MARK: - TapGesture
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
//        let objCommanWebViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as! CommanWebViewController
//        objCommanWebViewController.strNavTitle = "Ads"
//        objCommanWebViewController.strUrl = GifurladsRedirect
//        self.navigationController?.pushViewController(objCommanWebViewController, animated: true)
        if let url = URL(string: GifurladsRedirect) {
            UIApplication.shared.open(url, options: [:]) }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    //MARK: - API Call
    func getMySelfieList()
    {
        
        SelfieModel().getSelfieList(user_id: AppDelegate.sharedInstance().userId, conference_id: AppDelegate.sharedInstance().conference.data[0].iD, {(response) in
            if response.code == 1{
                self.objGetSelfieList = response
               
                self.colMySelfie.dataSource = self
                self.colMySelfie.delegate = self

                self.colMySelfie.reloadData()
                
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.getMySelfieList()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    
    
    //MARK:- Collection delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if imgSelfie != nil{
//            return 5
//        }
        return (objGetSelfieList?.data.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SelfieCollectionViewCell
        cell.layer.cornerRadius = 2
        cell.layer.borderColor = colorset.lightBlackTextColor.cgColor
        cell.layer.borderWidth = 1
        cell.imgVSelfie.sd_setImage(with: URL(string: (objGetSelfieList?.data[indexPath.row].image)! ), placeholderImage: UIImage(named: "imgLoginLogo"))

//        if imgSelfie != nil{
//            cell.imgVSelfie.image = imgSelfie
//        }
//        else{
//            cell.imgVSelfie.sd_setImage(with: URL(string: (objGetSelfieList?.data[indexPath.row].image)! ), placeholderImage: UIImage(named: "imgLoginLogo"))
//        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: screenWidth/3 - 20, height:kDevPropotionalHeight(size: 130) )
    }
    
    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "My Selfie"

        getMySelfieList()
    }

    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        if isFromMenu{
            slideMenuController()?.toggleLeft()
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

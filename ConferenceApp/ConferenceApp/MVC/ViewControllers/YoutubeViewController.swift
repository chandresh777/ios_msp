//
//  YoutubeViewController.swift
//  Sonictone
//
//  Created by MSPSYS061 on 05/03/18.
//  Copyright © 2018 MSP. All rights reserved.
//

import UIKit
import YouTubePlayer
import MobileCoreServices
import AVFoundation
class YoutubeViewController: BaseViewController,YouTubePlayerDelegate {
    
    @IBOutlet var activity_indicatior: UIActivityIndicatorView!
    @IBOutlet var view_youtube: YouTubePlayerView!
    var strUrl : String = ""
    var songPlay:String = "notplay"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_youtube.delegate=self
        activity_indicatior.startAnimating()
        headerView.lblHeader.text = "My Session"
        self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        
//        if AppDelegate.sharedInstance().avplayer != nil
//        {
//            if AppDelegate.sharedInstance().avplayer.timeControlStatus == AVPlayerTimeControlStatus.playing
//            {
//                songPlay = "play"
//                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "PlayYoutube") , object: nil, userInfo:nil)
//                //btnPlay.setImage(UIImage(named: "Playerpause"), for: .normal)
//            }
//        }
        //Set url of Youtube
        let myVideoURL = NSURL(string: strUrl)
        view_youtube.loadVideoURL(myVideoURL! as URL)        // Do any additional setup after loading the view.
    }
    
    override func btnLeftBtnTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        //pausemusic()
        
//        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
//        tracker.set(kGAIScreenName, value: "YoutubePlayerScreen")
//
//        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
//        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    /*
     //MARK: Youtube Delegate
     --------------------------------------------------
     */
    func playerReady(_ videoPlayer: YouTubePlayerView)
    {
        activity_indicatior.hidesWhenStopped=true
        activity_indicatior.stopAnimating()
        //    print("playerReady")
    }
    func playerStateChanged(_ videoPlayer: YouTubePlayerView, playerState: YouTubePlayerState)
    {
        //    print("playerStateChanged")
        //    print(playerState)
    }
    
//    override func btnclickBack(_ sender: UIButton) {
//        let str:String? = UserDefaults.standard.value(forKey: "music") as? String
//        if songPlay == "play"
//        {
//            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "PlayYoutube") , object: nil, userInfo:nil)
//        }
//        self.dismiss(animated: true, completion: nil)
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

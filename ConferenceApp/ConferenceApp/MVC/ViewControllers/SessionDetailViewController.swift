//
//  SessionDetailViewController.swift
//  ConferenceApp
//
//  Created by msp on 26/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class SessionDetailViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITextViewDelegate {
    @IBOutlet weak var lblBookMarkTitle: UILabel!
    
    @IBOutlet weak var imgvBookmark: UIImageView!
    @IBOutlet weak var ctladdNote: UIControl!
    @IBOutlet weak var ctlBookMark: UIControl!
    @IBOutlet weak var lblQuizTitle: UILabel!
    @IBOutlet weak var lblAddNoteTitle: UILabel!
    @IBOutlet weak var viewtblHeader: UIView!
    @IBOutlet weak var lblSessionTitle: UILabel!
    
    @IBOutlet weak var lblSessionDate: UILabel!
    
    @IBOutlet weak var lblSessionDesc: UILabel!
    
    @IBOutlet weak var lblSessionTimeTitle: UILabel!
    
    @IBOutlet weak var lblSessionTimeVal: UILabel!
    
    @IBOutlet weak var lblConferenceHall: UILabel!
    
    var customviewNotes:CustomViewAddNotes!
    
    @IBOutlet weak var tblSessionDetail: UITableView!
    var sessionDetail:SessionDetailRootClass!
    var heightcollection : CGFloat!
    var strsessionID:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        setlayout()
        
        customviewNotes = Bundle.main.loadNibNamed("CustomViewAddNotes", owner: self, options: nil)![0] as! CustomViewAddNotes
        customviewNotes.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        self.view.addSubview(customviewNotes)
        customviewNotes.btnCancel.addTarget(self, action: #selector(btnclickCancel(_:)), for: .touchUpInside)
         customviewNotes.btnOkey.addTarget(self, action: #selector(btnclickOkey(_:)), for: .touchUpInside)
        customviewNotes.txtView.delegate = self
        
        customviewNotes.isHidden = true
        tblSessionDetail.separatorInset = UIEdgeInsetsMake(0, 20, 0, 0)
        self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")

        var dbl = 2.0/Float(3)
        print(dbl)
        if dbl.truncatingRemainder(dividingBy: 1) == 0 {
            //it's an integer
            
        }
        else{
            dbl = Float(Int(dbl) + 1)
            
        }
        print(dbl)
        let spaceCount = dbl * 10
        
        //        heightcollection = ((((UIScreen.main.bounds.size.width-20)/3)-7) + CGFloat(dbl)) * CGFloat(spaceCount) + 70
        
        heightcollection = ((((UIScreen.main.bounds.size.width-20)/3)-7) * CGFloat(dbl)) + CGFloat(spaceCount) + 50
        tblSessionDetail.reloadData()
        
      tblSessionDetail.dataSource = nil
        callSessionDetail()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setlayout()
    }
    func setlayout()
    {
        self.headerView.lblHeader.text = "Session Detail"

        lblSessionTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        lblSessionDate.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        lblSessionDesc.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        lblSessionTimeTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 16.0))
        lblSessionTimeVal.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        lblConferenceHall.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        lblConferenceHall.layoutIfNeeded()
        lblConferenceHall.setNeedsLayout()
        viewtblHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: lblConferenceHall.frame.origin.y + 35)
        
        viewtblHeader.layoutIfNeeded()
        viewtblHeader.setNeedsLayout()

        lblBookMarkTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 16.0))
        lblAddNoteTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 16.0))
        
        lblBookMarkTitle.textColor = colorset.whitetextColor
        lblAddNoteTitle.textColor = colorset.whitetextColor
        lblSessionDesc.textColor = colorset.lightBlackTextColor
        lblConferenceHall.textColor = colorset.drawerBackground
        ctladdNote.backgroundColor = colorset.drawerBackground
        ctlBookMark.backgroundColor = colorset.drawerBackground
        self.tblSessionDetail.reloadData()
    }
    
    //MARK: Set session data
    func setsessionData()
    {
        lblSessionTitle.text = sessionDetail.data.name
        lblSessionDate.text = stringdateFormate(strdate: sessionDetail.data.sessionDate)
        lblSessionDesc.text = sessionDetail.data.descriptionField
        lblSessionTimeVal.text = String(format: "%@ To %@", sessionDetail.data.startTime,sessionDetail.data.endTime)
        lblConferenceHall.text = sessionDetail.data.hallName
        
        if sessionDetail.data.is_liked == "0"
        {
          self.imgvBookmark.image =  UIImage(named: "imgBookMark")
        }
        else{
           self.imgvBookmark.image = UIImage(named: "bookmarked_icon")
        }
        
        if sessionDetail.data.notes == ""
        {
            customviewNotes.txtView.text = "Add notes"
            customviewNotes.txtView.textColor = UIColor.lightGray
        }
        else{
            customviewNotes.txtView.text  = sessionDetail.data.notes
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.005) {
            self.setlayout()

        }

    }
    
    @objc func callSessionDetail()
    {
        
        ConferenceDateSessionModel.sharedModel().getSessionDetail(session_id: strsessionID, conference_id: AppDelegate.sharedInstance().conference.data[0].iD,user_id:AppDelegate.sharedInstance().userId, { (response) in
            if response.code == 1{
                self.sessionDetail = response
                self.setsessionData()
                self.tblSessionDetail.dataSource = self
                self.tblSessionDetail.delegate = self
                self.tblSessionDetail.reloadData()
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.callSessionDetail()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    func callsetBookmark(islike:String)
    {
       if getUserProfileDataList() == nil
       {
        self.popupAlert(title: AppName.appName, message: ValidationMsg.loginmsg, actionTitles: ["Ok"], actions:[{action1 in
            
            }, nil])
        
       }
       else{
        ConferenceDateSessionModel.sharedModel().setBookmarkSession(user_id:(getUserProfileDataList()?.iD)! , session_id: strsessionID, conference_id: AppDelegate.sharedInstance().conference.data[0].iD, is_like: islike, { (response) in
            if response.code == 1{
                
                if islike == "0"
                {
                    self.sessionDetail.data.is_liked = "0"
                    self.imgvBookmark.image =  UIImage(named: "imgBookMark")
                  
                }
                else {
                    self.sessionDetail.data.is_liked = "1"
                    self.imgvBookmark.image = UIImage(named: "bookmarked_icon")
                   
                }
                
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.callsetBookmark(islike: islike)
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
        }
        
    }
    override func btnLeftBtnTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - UITableview delegate method
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vwHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60))
        let lbl = UILabel(frame: CGRect(x: 15, y: 20, width: 200, height: 23))
        lbl.textColor = colorset.blacktextcolor
        lbl.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        if section == 0
        {
            lbl.text = "Session Speaker"
        }
        else{
            lbl.text = "Session Moderator"
        }
        
        vwHeader.addSubview(lbl)
        
        
        return vwHeader
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 && indexPath.row == sessionDetail.data.moderatorLists.count + 1
        {
            
            
            return heightcollection
        }
        else{
            return 40
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
         if section == 0
         {
            return sessionDetail.data.speakerLists.count
        }
         else{
            return sessionDetail.data.moderatorLists.count + 2
        }
      
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! sessionDetailCell
            cell.lblName.textColor = colorset.blacktextcolor
            cell.lblName.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
            
            cell.lblName.text = String(format: "%@ %@", sessionDetail.data.speakerLists[indexPath.row].firstName,sessionDetail.data.speakerLists[indexPath.row].lastName)
            cell.imgv.image = UIImage(named: "imgSpeaker")
            cell.viewSepertor.backgroundColor = colorset.sepretorColor
            cell.imgv.contentMode = .scaleAspectFit
            
            return cell
        }
        else if indexPath.section == 1 && indexPath.row == sessionDetail.data.moderatorLists.count
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellquiz", for: indexPath) as! sessionDetailcellQuiz
            cell.lblQuizTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 16))
            cell.lblQuizTitle.textColor = colorset.blacktextcolor
            cell.viewBGQuiz.backgroundColor = colorset.navigationBar
            return cell
        }
        else if indexPath.section == 1 && indexPath.row == sessionDetail.data.moderatorLists.count + 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "celltbl", for: indexPath) as! customSessiontblImgCell
            cell.lblMediaTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 16))
            cell.lblMediaTitle.textColor = colorset.blacktextcolor
            cell.collection_view.dataSource = self
            cell.collection_view.delegate = self
            cell.collection_view.reloadData()
            // cell.collection_view.backgroundColor = UIColor.green
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! sessionDetailCell
            cell.lblName.textColor = colorset.blacktextcolor
            cell.lblName.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
            
            cell.lblName.text = String(format: "%@ %@", sessionDetail.data.moderatorLists[indexPath.row].firstName,sessionDetail.data.moderatorLists[indexPath.row].lastName)
            cell.viewSepertor.backgroundColor = colorset.sepretorColor
            cell.imgv.image = UIImage(named: "imgModrator")
            cell.imgv.contentMode = .scaleAspectFit
            
            return cell
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0
        {
            let sessionDetailVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "SpeakerDetailViewController") as! SpeakerDetailViewController
            sessionDetailVC.struserId = sessionDetail.data.speakerLists[indexPath.row].iD
            self.navigationController?.pushViewController(sessionDetailVC, animated: true)
            
        }
        // else if sessionDetail.data.moderatorLists.count > 0{
        // let sessionDetailVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "SpeakerDetailViewController") as! SpeakerDetailViewController
        // sessionDetailVC.struserId = sessionDetail.data.moderatorLists[indexPath.row].iD
        // self.navigationController?.pushViewController(sessionDetailVC, animated: true)
        //
        // }
        
    }
    
    //MARK: - UICollectionview delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return 2
    }
    
    
    @available(iOS 6.0, *)
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellimgv", for: indexPath) as! SessionDetailImgCollectionCell
        if indexPath.row == 0
        {
          
         cell.imgv.sd_setImage(with: URL(string: sessionDetail.data.file ?? ""), placeholderImage: UIImage(named: "imgLoginLogo"))
        }
        else{
             cell.imgv.sd_setImage(with: URL(string: sessionDetail.data.thumb_youtube_img ?? ""), placeholderImage: UIImage(named: "imgLoginLogo"))

        }
        // cell.backgroundColor = UIColor.lightGray
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        //return CGSize(width: (screenWidth - 40) / 3, height: (screenWidth - 40) / 3)
                return CGSize(width: ((UIScreen.main.bounds.size.width-20)/3)-7, height: ((UIScreen.main.bounds.size.width-20)/3)-7)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func stringdateFormate(strdate:String)->String
    {
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: strdate)!
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            let zoomVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "ZoomImageController") as! ZoomImageController
            zoomVC.imgString = sessionDetail.data.file
            self.present(zoomVC, animated: true, completion: nil)
        }else if indexPath.row == 1{
            let youtubeVC=StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "YoutubeViewController") as! YoutubeViewController
            // youtubeVC.strUrl =  "https://www.youtube.com/watch?v=IvDZWuQcEc4"
            youtubeVC.strUrl = sessionDetail.data.youtubeUrl
//            youtubeVC.strUrl = (objRow?.videoUrl)!
            self.present(youtubeVC, animated: true, completion: nil)
        }
    }
    @IBAction func btnclickAddNote(_ sender: Any) {
        customviewNotes.isHidden = false
    }
    
    @IBAction func btnclickBookmark(_ sender: Any) {
        
        
        if sessionDetail.data.is_liked == "0"
        {
            callsetBookmark(islike: "1")
        }
        else{
           callsetBookmark(islike: "0")
        }
    }
    //MARK: - Add notes
    @objc func btnclickCancel(_ sender:UIButton)
    {
        self.customviewNotes.isHidden = true
        self.customviewNotes.txtView.resignFirstResponder()
    }
    @objc func btnclickOkey(_ sender:UIButton)
    {
        if customviewNotes.txtView.text == "Add notes" || customviewNotes.txtView.text == ""
        {
            self.popupAlert(title: AppName.appName, message: ValidationMsg.addNotes, actionTitles: ["Ok"], actions:[{action1 in
                
                }, nil])
            
        }
        else
        {
        addNotes()
        }
        }
    func addNotes()
    {
   
         ConferenceDateSessionModel.sharedModel().AddnotesSession(user_id: (getUserProfileDataList()?.iD)!, session_id: strsessionID, conference_id: AppDelegate.sharedInstance().conference.data[0].iD, notes: customviewNotes.txtView.text ?? "", note_id: sessionDetail.data.note_id, { (response) in
            if response.code == 1{
                
                self.customviewNotes.isHidden = true
                self.customviewNotes.txtView.resignFirstResponder()
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.addNotes()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    //MARK : UItextview delegate method
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add notes"
            textView.textColor = UIColor.lightGray
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

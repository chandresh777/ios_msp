//
//  SponsorListViewController.swift
//  ConferenceApp
//
//  Created by msp on 02/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class SponsorListViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate{
    var sponsorList:SponsorListRoot!
    var objSponsorList : SponsorNewRootClass!
    
    @IBOutlet weak var tblsponsor: UITableView!
    var goldarr:[SponsorNewSponsor] = []
    var silverarr:[SponsorNewSponsor] = []
    var platinumarr:[SponsorNewSponsor] = []
    var bronzearr:[SponsorNewSponsor] = []
    
       var arropenArrow:[Int] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.lblHeader.text = "Sponsors"
self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")
        getNewSponsorList()
        // Do any additional setup after loading the view.
    }
    override func btnLeftBtnTapped() {
        slideMenuController()?.toggleLeft()
    }
    
    //MARK: - UITableview Method
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 55
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 55))
        viewHeader.backgroundColor = colorset.navigationBar
        
        viewHeader.isUserInteractionEnabled = true
        let lbltitle = UILabel(frame: CGRect(x: 20, y: 15, width: 100, height: 30))
        lbltitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        lbltitle.textColor = colorset.blacktextcolor
        if section == 0
        {
            lbltitle.text = "Gold"
        }
        else if section == 1 {
            lbltitle.text = "Silver"
        }
        else if section == 2{
          lbltitle.text = "Platinum"
        }else{
            lbltitle.text = "Bronze"
        }
        
        viewHeader.addSubview(lbltitle)
        
        
        let imgv = UIImageView(frame: CGRect(x: UIScreen.main.bounds.size.width - 40, y: 15, width:20, height: 20))
        imgv.contentMode = .scaleAspectFit
        if arropenArrow[section] == 1
        {
            imgv.image = UIImage(named: "imgdownarrow")
            
            
            
        }
        else{
            
            imgv.image = UIImage(named: "imgrightarrow")
        }
        
        
        
        viewHeader.tag = section
        viewHeader.addSubview(imgv)
        
        let viewwhite = UIView(frame: CGRect(x: 0, y: 50, width: UIScreen.main.bounds.size.width, height: 5))
        viewwhite.backgroundColor = UIColor.white
        
        viewHeader.addSubview(viewwhite)
        
        
        let tapGest = UITapGestureRecognizer(target: self, action: #selector(tapClick(_:)))
        tapGest.delegate = self
        viewHeader.addGestureRecognizer(tapGest)
        
        
        return viewHeader
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
     {
        if section == 0
        {
        return goldarr.count
        }
        else if section == 1
        {
            return silverarr.count
        }
        else if section == 2{
            return platinumarr.count
        }else{
            return bronzearr.count
        }
        }
    
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
   {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! sopnsorListCell
    
    cell.lbltitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 16.0))
    
    cell.lbltitle.textColor = colorset.blacktextcolor
    if indexPath.section == 0
    {
        
//        cell.lbltitle.text = String(format: "%@ %@", sponsorList.data.gOLD[indexPath.row].firstName,sponsorList.data.gOLD[indexPath.row].lastName)
        cell.lbltitle.text = String (format: "%@ %@", goldarr[indexPath.row].firstName,goldarr[indexPath.row].lastName)
    }
    else if indexPath.section == 1
    {
//         cell.lbltitle.text = String(format: "%@ %@", sponsorList.data.sILVER[indexPath.row].firstName,sponsorList.data.sILVER[indexPath.row].lastName)
        cell.lbltitle.text = String (format: "%@ %@", silverarr[indexPath.row].firstName,silverarr[indexPath.row].lastName)
    }
    else if indexPath.section == 2{
//         cell.lbltitle.text = String(format: "%@ %@", sponsorList.data.pLATINIUM[indexPath.row].firstName,sponsorList.data.pLATINIUM[indexPath.row].lastName)
        cell.lbltitle.text = String (format: "%@ %@", platinumarr[indexPath.row].firstName,platinumarr[indexPath.row].lastName)
        
    }else {
        cell.lbltitle.text = String (format: "%@ %@", bronzearr[indexPath.row].firstName,bronzearr[indexPath.row].lastName)
    }
    
    cell.selectionStyle = .none
    return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objSponsorDetailViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "SponsorDetailViewController") as! SponsorDetailViewController
        if indexPath.section == 0{
            objSponsorDetailViewController.objSponsorDetail = goldarr[indexPath.row]
        }
        else if indexPath.section == 1{
            objSponsorDetailViewController.objSponsorDetail = silverarr[indexPath.row]
        }
        else if indexPath.section == 2{
            objSponsorDetailViewController.objSponsorDetail = platinumarr[indexPath.row]
        }
        else if indexPath.section == 3{
            objSponsorDetailViewController.objSponsorDetail = bronzearr[indexPath.row]
        }

        self.navigationController?.pushViewController(objSponsorDetailViewController, animated: true)
    }
    
    
        func getNewSponsorList()
        {
            SponsorModel().getsponsorNewList(conference_id: AppDelegate.sharedInstance().conference.data[0].iD, {  (response) in
                if response.code == 1{
                    self.objSponsorList = response
                    self.goldarr = self.objSponsorList.data[0].sponsors
                    self.silverarr = self.objSponsorList.data[1].sponsors
                    self.platinumarr = self.objSponsorList.data[2].sponsors
                    self.bronzearr = self.objSponsorList.data[3].sponsors
                    
                    self.closealltab()
                    self.tblsponsor.dataSource = self
                    self.tblsponsor.delegate = self
                    self.tblsponsor.reloadData()
                }
                else{
                    self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
    
                        }, nil])
                }
    
            }) { (request, sender, json, error) in
                Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
    
                    switch selectedIndex {
                    case CONSTANTs.TWO: //for retry
    
                        //Handle action
                        self.getNewSponsorList()
    
                        break
    
                    default:
    
                        // ignore case
    
                        break
                    }
    
                })
            }
        }
    
//    func getsponsorlist()
//    {
//        SponsorModel.sharedModel().getsponsorList(conference_id: AppDelegate.sharedInstance().conference.data[0].iD, user_id: AppDelegate.sharedInstance().userId, { (response) in
//            if response.code == 1{
//                self.sponsorList = response
//
//
//                self.goldarr = self.sponsorList.data.gOLD
//                self.silverarr = self.sponsorList.data.sILVER
//                self.platinumarr = self.sponsorList.data.pLATINIUM
////                self.bronze = self.sponsorList.data.
//
//
//                self.tblsponsor.dataSource = self
//                self.tblsponsor.delegate = self
//                self.tblsponsor.reloadData()
//            }
//            else{
//                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
//
//                    }, nil])
//            }
//
//        }) { (request, sender, json, error) in
//            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
//
//                switch selectedIndex {
//                case CONSTANTs.TWO: //for retry
//
//                    //Handle action
//                    self.getsponsorlist()
//
//                    break
//
//                default:
//
//                    // ignore case
//
//                    break
//                }
//
//            })
//        }
//    }
    
    
   @objc func tapClick(_ gest:UITapGestureRecognizer)
  {
//        if (gest.view?.tag)! == 0
//        {
//            
//            if myfavouriteList.data.sessionList.count > 0
//            {
//                arropenArrow[(gest.view?.tag)!] = 0
//                var arrindepath:[IndexPath]=[]
//                
//                for i in 0..<myfavouriteList.data.sessionList.count
//                {
//                    let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
//                    arrindepath.append(indexpath)
//                }
//                
//                myfavouriteList.data.sessionList = []
//                tblsponsor.deleteRows(at: arrindepath, with: .top)
////                tblFavourite.deleteRows(at: arrindepath, with: .top)
//                
//                tblsponsor.reloadData()
//                
//            }
//            else{
//                
//                arropenArrow[(gest.view?.tag)!] = 1
//                var arrindepath:[IndexPath]=[]
//                sponsorList.data.gOLD  = goldarr
//               
//                for i in 0..<sponsorList.data.gOLD.count
//                {
//                    let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
//                    arrindepath.append(indexpath)
//                }
//                
//                tblsponsor.insertRows(at: arrindepath, with: .top)
//                
//                tblsponsor.reloadData()
//                
//                
//            }
//        }
//        else{
//            
//            if myfavouriteList.data.speakersList.count > 0
//            {
//                arropenArrow[(gest.view?.tag)!] = 0
//                var arrindepath:[IndexPath]=[]
//                for i in 0..<myfavouriteList.data.speakersList.count
//                {
//                    let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
//                    arrindepath.append(indexpath)
//                }
//                myfavouriteList.data.speakersList = []
//                tblsponsor.deleteRows(at: arrindepath, with: .top)
//                
//                tblsponsor.reloadData()
//            }
//            else{
//                
//                arropenArrow[(gest.view?.tag)!] = 1
//                var arrindepath:[IndexPath]=[]
//                myfavouriteList.data.speakersList = myfavouriteListspeaker
//                for i in 0..<myfavouriteList.data.speakersList.count
//                {
//                    let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
//                    arrindepath.append(indexpath)
//                }
//                
//                tblsponsor.insertRows(at: arrindepath, with: .top)
//                
//                tblsponsor.reloadData()
//                
//            }
//        }
//        //        if arrrow[(gest.view?.tag)!].count > 0
//        //        {
//        //            arropenArrow[(gest.view?.tag)!] = 0
//        //            var arrindepath:[IndexPath]=[]
//        //            if (gest.view?.tag)! == 0
//        //            {
//        //            for i in 0..<myfavouriteList.data.sessionList.count
//        //            {
//        //                let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
//        //                arrindepath.append(indexpath)
//        //            }
//        //            }
//        //            else{
//        //
//        //                for i in 0..<myfavouriteList.data.speakersList.count
//        //                {
//        //                    let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
//        //                    arrindepath.append(indexpath)
//        //                }
//        //
//        //            }
//        //
//        //            if (gest.view?.tag)! == 0
//        //            {
//        //                myfavouriteList.data.sessionList = []
//        //            }
//        //            else{
//        //              myfavouriteList.data.speakersList = []
//        //            }
//        //
//        //            tblFavourite.deleteRows(at: arrindepath, with: .top)
//        //
//        //             tblFavourite.reloadData()
//        //
//        //
//        //        }
//        //        else{
//        //            arropenArrow[(gest.view?.tag)!] = 1
//        //            var arrindepath:[IndexPath]=[]
//        //
//        //            if (gest.view?.tag)! == 0
//        //            {
//        //                myfavouriteList.data.sessionList = myfavouriteListsession
//        //            }
//        //            else{
//        //              myfavouriteList.data.speakersList = myfavouriteListspeaker
//        //            }
//        //
//        //
//        //            if (gest.view?.tag)! == 0
//        //            {
//        //                for i in 0..<myfavouriteList.data.sessionList.count
//        //                {
//        //                    let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
//        //                    arrindepath.append(indexpath)
//        //                }
//        //            }
//        //            else{
//        //
//        //                for i in 0..<myfavouriteList.data.speakersList.count
//        //                {
//        //                    let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
//        //                    arrindepath.append(indexpath)
//        //                }
//        //
//        //            }
//        //            tblFavourite.insertRows(at: arrindepath, with: .top)
//        //
//        //            tblFavourite.reloadData()
//        //
//        //        }
//        
   }
    
    
    
    func closealltab()
    {
        var indexpath:[IndexPath]=[]
        arropenArrow.removeAll()
        for j in 0..<4
        {
            if objSponsorList.data[0].sponsors.count == 0{
                arropenArrow.append(0)
            }else{
                arropenArrow.append(1)
            }
            
            
            // indexpath.append(IndexPath(item:i , section:j))
         
            
        }
        //tblVAlbums.deleteRows(at: indexpath, with: .top)
        tblsponsor.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

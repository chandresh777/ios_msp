//
//  EntertainmentDetailViewController.swift
//  ConferenceApp
//
//  Created by MSP on 31/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class EntertainmentDetailViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var lblEntertainment: UILabel!
    @IBOutlet var lblVanue: UILabel!
    var selectedIndex : Int!
    var objEntertainmentDetailData : EntertainmentRootClass? = nil
//    @IBOutlet var lblFooter: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        
        setLayoutMethod()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    //MARK: -Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row < 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrganizationLocationCell
            cell.lblTitle.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 17.0))
            cell.lblTitle.numberOfLines = 0
            
            cell.lblTitle.textColor = colorset.lightBlackTextColor
            cell.selectionStyle = .none
            if indexPath.row == 0{
                cell.imgInfo.image = UIImage.init(named: "loction")
                cell.lblTitle.text = objEntertainmentDetailData?.data[selectedIndex].venue
            }else
            {
                cell.imgInfo.image = UIImage.init(named: "calendar_icon")
                cell.lblTitle.text = "\(stringdateFormate(strdate: (objEntertainmentDetailData?.data[selectedIndex].date)!)) at \(stringTimeFormate(strdate: (objEntertainmentDetailData?.data[selectedIndex].time)!))"
//                cell.lblTitle.text = "\(objEntertainmentDetailData?.data[selectedIndex].date ?? "") ,\(objEntertainmentDetailData?.data[selectedIndex].time ?? "")"
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrganizationDetailCell") as! OrganizationDetailCell
        
        cell.lblDesc.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        cell.lblDesc.numberOfLines = 0
        cell.lblDesc.text = objEntertainmentDetailData?.data[selectedIndex].descriptionField ?? ""
        cell.imgDescription.sd_setImage(with:URL(string:objEntertainmentDetailData?.data[selectedIndex].image ?? ""), placeholderImage: UIImage(named: "imgLoginLogo"))

        cell.lblDesc.textColor = colorset.lightBlackTextColor
        cell.viewSep.backgroundColor = colorset.sepretorColor
        cell.imgDescription.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let abstactDetailVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "EntertainmentDetailViewController") as! EntertainmentDetailViewController
        
        self.navigationController?.pushViewController(abstactDetailVC, animated: true)
    }
    
    
    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "Entertainment Detail"
        lblEntertainment.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 17.0))
        lblVanue.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 17.0))
//        lblFooter.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 15.0))

        lblEntertainment.textColor = colorset.blacktextcolor
        lblVanue.textColor = colorset.blacktextcolor
//        lblFooter.textColor = colorset.blacktextcolor

        lblEntertainment.text = objEntertainmentDetailData?.data[selectedIndex].title
        lblVanue.text = "Vanue"
        
    }
    
    
    func stringdateFormate(strdate:String)->String
    {
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: strdate)!
        dateFormatter.dateFormat = "dd MMM yyyy"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    func stringTimeFormate(strdate:String)->String
    {
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "HH:mm"
        let date = dateFormatter.date(from: strdate)!
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    
    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

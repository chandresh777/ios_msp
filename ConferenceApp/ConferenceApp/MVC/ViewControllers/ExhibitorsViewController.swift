//
//  ExhibitorsViewController.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class ExihibitorCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewSep: UIView!
    
}

class ExhibitorsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tblExhibitorList: UITableView!
    
    @IBOutlet weak var viewBottomBar: UIView!
    @IBOutlet weak var imgvAds: UIImageView!
    var organizationList:OrganizationListRoot!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")
        
        setLayoutMethod()
         imgvAds.image = UIImage.animatedImage(withAnimatedGIFURL: URL(string: Gifurlads)!)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.imgvAds.addGestureRecognizer(tap)
        self.imgvAds.isUserInteractionEnabled = true
        getOrganizerList()
    }

    //MARK: - TapGesture
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
//        let objCommanWebViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as! CommanWebViewController
//        objCommanWebViewController.strNavTitle = "Ads"
//        objCommanWebViewController.strUrl = GifurladsRedirect
//        self.navigationController?.pushViewController(objCommanWebViewController, animated: true)
        if let url = URL(string: GifurladsRedirect) {
            UIApplication.shared.open(url, options: [:]) }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    func getOrganizerList()
    {
        
        OrganizerListModel.sharedModel().getorganizerList(conference_id: AppDelegate.sharedInstance().conference.data[0].iD, { (response) in
            if response.code == 1{
                self.organizationList = response
              
                self.tblExhibitorList.dataSource = self
                self.tblExhibitorList.delegate = self
                self.tblExhibitorList.reloadData()
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.getOrganizerList()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "Organization List"
        
        tblExhibitorList.translatesAutoresizingMaskIntoConstraints = true
        tblExhibitorList.frame = CGRect(x: tblExhibitorList.frame.origin.x, y: self.headerView.frame.origin.y + self.headerView.frame.size.height, width: screenWidth, height: screenHeight - (self.headerView.frame.origin.y + self.headerView.frame.size.height + viewBottomBar.frame.size.height))

        tblExhibitorList.backgroundColor = .clear
        tblExhibitorList.separatorStyle = .none
        
        
    }
    
    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        slideMenuController()?.toggleLeft()
    }

    //MARK: -Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return organizationList.data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let objExihibitorCell = tableView.dequeueReusableCell(withIdentifier: "ExihibitorCell") as! ExihibitorCell
        
       
       
        
        objExihibitorCell.lblTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        objExihibitorCell.viewSep.backgroundColor = colorset.sepretorColor
        
         objExihibitorCell.lblTitle.text =  String(format: "%@ %@", organizationList.data[indexPath.row].firstName,organizationList.data[indexPath.row].lastName)
        
        objExihibitorCell.selectionStyle = .none
        return objExihibitorCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objSpeakerDetailVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "OrganizationDetailViewController") as! OrganizationDetailViewController
        objSpeakerDetailVC.strUserID = organizationList.data[indexPath.row].iD
        self.navigationController?.pushViewController(objSpeakerDetailVC, animated: true)
    }


}

//
//  LeftViewController.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case main = 0
    case sessions
    case presenters
//    case abstracts
   //case exhibitors
    case sponsors
//    case dayAtGlance
    case socialAndNews
    case attendees
    case mySelfie
    case entertainment
    case aboutConference
//    case quizAndWinner
//    case notification
//    case digitalWalk
    case venueMap
    case localTips
    case appTips
    case commiteMembers
    case logout
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}


class LeftViewController: UIViewController {
    
    let objAlertViewClass = AlertViewClass()
    @IBOutlet weak var tblDrawerMenu: UITableView!
    var menus : NSMutableArray = []
    var arrImgDrawer : NSMutableArray = []
    var mainViewController: UIViewController!
    
    var viewDrawerHeader : DrawerMenuHeaderView!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setUseinfo(_:)), name: Notification.Name("setuserInfo"), object: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tblDrawerMenu.reloadData()
    }
    //MARK: - UserInfo
    @objc func setUseinfo(_ notification:NSNotification)
    {
        tblDrawerMenu.reloadData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.slideMenuController()?.leftPanGesture?.isEnabled=true
    }
    
    //MARK: -Custom Access Methods
    func setLayout() {
        
        tblDrawerMenu.separatorStyle = .none
        tblDrawerMenu.dataSource = self
        tblDrawerMenu.delegate = self
        
        self.tblDrawerMenu.clipsToBounds = true
        self.tblDrawerMenu.backgroundColor = colorset.drawerBackground

        /*
         Design for creating the header view of drawer
         */
        viewDrawerHeader = Bundle.main.loadNibNamed("DrawerMenuHeaderView", owner: self, options: nil)![0] as! DrawerMenuHeaderView
        
        /*
         set layout for display menu item
         */
        
// letest       menus = ["Home/Dashboard", "Sessions", "Speakers", "Abstracts","Sponsors","Day at Glance", "Social & News", "Attendees", "My Selfie", "Entertainment", "About Conference", "Quiz & Winner", "Notification/Updates", "Local Tips", "App Tips", "Committee Members", "Logout"]
//        menus = ["Home/Dashboard", "Sessions", "Speakers", "Abstracts","Sponsors","Day at Glance", "Social & News", "Attendees", "My Selfie", "Entertainment", "About Conference", "Quiz & Winner", "Notification/Updates", "Digital Walk", "Local Tips", "App Tips", "Committee Members", "Logout"]
// letest       arrImgDrawer = ["Dashboard", "Sessions", "Speakers", "Abstracts", "sponser", "Day at Glance", "Social & News", "Attendees", "imgSelfie", "Entertainment", "About Conference", "Quiz & Winner", "Notification", "Local Tips", "Local Tips", "Commite Members", "logout"]
//        arrImgDrawer = ["Dashboard", "Sessions", "Speakers", "Abstracts", "Exhibitors", "sponser", "Day at Glance", "Social & News", "Attendees", "imgSelfie", "Entertainment", "About Conference", "Quiz & Winner", "Notification", "Digital Walk", "Local Tips", "Local Tips", "Commite Members", "logout"]
        
        
        menus = ["Home/Dashboard", "Sessions", "Speakers", "Sponsors", "Social & News", "Attendees", "My Selfie", "Entertainment", "About Conference","Venue Floor Map","Local Tips", "App Tips", "Committee Members", "Logout"]
        //        menus = ["Home/Dashboard", "Sessions", "Speakers", "Abstracts","Sponsors","Day at Glance", "Social & News", "Attendees", "My Selfie", "Entertainment", "About Conference", "Quiz & Winner", "Notification/Updates", "Digital Walk", "Local Tips", "App Tips", "Committee Members", "Logout"]
        arrImgDrawer = ["Dashboard", "Sessions", "Speakers",  "sponser",  "Social & News", "Attendees", "imgSelfie", "Entertainment", "About Conference","venue Floor map", "Local Tips", "Local Tips", "Commite Members", "logout"]
        
        tblDrawerMenu.translatesAutoresizingMaskIntoConstraints = true
        if DEVICE_INFO.IS_IPHONE_X {
            tblDrawerMenu.frame = CGRect(x: 0, y: 0, width: DRAWERWIDTH, height: screenHeight)
        }
        else{
            tblDrawerMenu.frame = CGRect(x: 0, y: 20, width: DRAWERWIDTH, height: screenHeight - 20)
        }
            tblDrawerMenu.reloadData()
    }
    
    func setImageViewImage(imageView: UIImageView, image: UIImage) {
        imageView.image = image.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
    }
    
    func setCALayerImage(layer: CALayer, image: UIImage) {
        let tintedImage = image.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        layer.contents = tintedImage.cgImage
    }
    
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .logout:
            AppDelegate.sharedInstance().indexpathOfDrawerSel = IndexPath()
             removeObjectFromDefault(UserDefaultKey.userLoginInfo)
            Utility.moveToLoginViewController()
            
            print("Logout")
        case .sessions:
            let objSessionsViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "ConferenceDayViewController")
            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objSessionsViewController)
            objNavigationController.navigationBar.isHidden = true
            UINavigationBar.appearance().tintColor = UIColor.black
            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
        case .presenters:
            let objPresenterListVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "PresenterListVC")
          
            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objPresenterListVC)
            objNavigationController.navigationBar.isHidden = true
            UINavigationBar.appearance().tintColor = UIColor.black
            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
//        case .abstracts: break
//            let objAbstractVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "AbstractViewController")
//            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objAbstractVC)
//            objNavigationController.navigationBar.isHidden = true
//            UINavigationBar.appearance().tintColor = UIColor.black
//            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
            
//        case .exhibitors:
//            let objExhibitorsVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "ExhibitorsViewController")
//            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objExhibitorsVC)
//            objNavigationController.navigationBar.isHidden = true
//            UINavigationBar.appearance().tintColor = UIColor.black
//            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
        case .sponsors:
            let objSessionsViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "SponsorListViewController")
            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objSessionsViewController)
            objNavigationController.navigationBar.isHidden = true
            UINavigationBar.appearance().tintColor = UIColor.black
            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
//        case .dayAtGlance: break
//            let objDayAtGlanceVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "DayAtGlanceViewController")
//            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objDayAtGlanceVC)
//            objNavigationController.navigationBar.isHidden = true
//            UINavigationBar.appearance().tintColor = UIColor.black
//            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
            
        case .socialAndNews:
            let objSocialAndNewsVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "SocialAndNewsViewController")
            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objSocialAndNewsVC)
            objNavigationController.navigationBar.isHidden = true
            UINavigationBar.appearance().tintColor = UIColor.black
            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
        case .attendees:
            let objAttendeesVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "AttendeesViewController")
            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objAttendeesVC)
            objNavigationController.navigationBar.isHidden = true
            UINavigationBar.appearance().tintColor = UIColor.black
            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
        case .mySelfie:
            let objMySelfieVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "MySelfieViewController") as! MySelfieViewController
            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objMySelfieVC)
            objMySelfieVC.isFromMenu =  true
            objNavigationController.navigationBar.isHidden = true
            UINavigationBar.appearance().tintColor = UIColor.black
            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
        case .entertainment:
            let objEntertainMentVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "EntertainmentViewController")
            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objEntertainMentVC)
            objNavigationController.navigationBar.isHidden = true
            UINavigationBar.appearance().tintColor = UIColor.black
            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
        case .aboutConference:
            let objAboutConferenceVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "AboutConferenceViewController")
            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objAboutConferenceVC)
            objNavigationController.navigationBar.isHidden = true
            UINavigationBar.appearance().tintColor = UIColor.black
            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
//        case .quizAndWinner: break
//            let objQuizVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "QuizViewController")
//            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objQuizVC)
//            objNavigationController.navigationBar.isHidden = true
//            UINavigationBar.appearance().tintColor = UIColor.black
//            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
 //       case .notification: break
//            let objNotificationVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
//            objNotificationVC.isFromMenu = true
//            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objNotificationVC)
//            objNavigationController.navigationBar.isHidden = true
//            UINavigationBar.appearance().tintColor = UIColor.black
//            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)

            
//        case .digitalWalk:
//            let objDigitalWalkVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "DigitalWalkViewController")
//            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objDigitalWalkVC)
//            objNavigationController.navigationBar.isHidden = true
//            UINavigationBar.appearance().tintColor = UIColor.black
//            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
            
        case.venueMap:
            let objVenueFloorMapController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "VenueFloorMapController")
            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objVenueFloorMapController)
            objNavigationController.navigationBar.isHidden = true
            UINavigationBar.appearance().tintColor = UIColor.black
            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
            
        case .localTips:
            let objCommanWebVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as!CommanWebViewController
            objCommanWebVC.strNavTitle = "Local Tips"
            objCommanWebVC.strCMS = getCMSPageListData()?.localTips
            objCommanWebVC.isFromMenu = true
            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objCommanWebVC)
            objNavigationController.navigationBar.isHidden = true
            UINavigationBar.appearance().tintColor = UIColor.black
            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
            //            print("localTips")
            
        case .appTips:
            let objCommanWebVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as!CommanWebViewController
            objCommanWebVC.strNavTitle = "App Tips"
            objCommanWebVC.strCMS = getCMSPageListData()?.appTips
            objCommanWebVC.isFromMenu = true
            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objCommanWebVC)
            objNavigationController.navigationBar.isHidden = true
            UINavigationBar.appearance().tintColor = UIColor.black
            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
        //            print("appTips")
        case .commiteMembers:
            let objCommanWebVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as!CommanWebViewController
            objCommanWebVC.strNavTitle = "Commite Members"
            objCommanWebVC.strCMS = getCMSPageListData()?.confrenceCommittee
            objCommanWebVC.isFromMenu = true
            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objCommanWebVC)
            objNavigationController.navigationBar.isHidden = true
            UINavigationBar.appearance().tintColor = UIColor.black
            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
            //            let objCommiteMemberVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommitteMemberViewController")
            //            let objNavigationController: UINavigationController = UINavigationController(rootViewController: objCommiteMemberVC)
            //            objNavigationController.navigationBar.isHidden = true
            //            UINavigationBar.appearance().tintColor = UIColor.black
            //            self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
        
        }
        
        
        
        
        
        
        
    }
    
    func logoutCall() {
        objAlertViewClass.showAlert(title:AppName.appName, msg: ValidationMsg.msgLogout, vc: self, positiveActionHandler: { (positiv) in
            
        }) { (Nagative) in
            
            debugPrint("Cancel Click")
        }
    }
    
    @objc func btnEditProfileTapped()  {
        
        
    }
    
    @objc func btnLoginClick(){
//        Utility.openLoginViewController(viewController: self)
    }
    
    @objc func btnProfileTapped(){
        let objProfileVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
          objProfileVC.strpresent = "yes"
//        let objNavigationController: UINavigationController = UINavigationController(rootViewController: objProfileVC)
//        objNavigationController.navigationBar.isHidden = true
//        UINavigationBar.appearance().tintColor = UIColor.black
//
self.present(objProfileVC, animated: true, completion: nil)
        self.slideMenuController()?.closeLeft()
       // self.slideMenuController()?.changeMainViewController(objNavigationController, close: true)
  
    
    }
    
    //MARK: - API Call
    
    //MARK: -ScrollView Delegate Methods
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        debugPrint("scrollViewDidScroll")
        self.slideMenuController()?.leftPanGesture?.isEnabled=false
        
    }
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView)
    {
        debugPrint("scrollViewWillBeginDecelerating")
        self.slideMenuController()?.leftPanGesture?.isEnabled=false
        
    }
    
    @available(iOS 2.0, *)
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        debugPrint("scrollViewDidEndDecelerating")
        self.slideMenuController()?.leftPanGesture?.isEnabled=true
        
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        debugPrint("scrollViewWillBeginDragging")
        self.slideMenuController()?.leftPanGesture?.isEnabled=false
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>)
    {
        debugPrint("scrollViewWillEndDragging")
        self.slideMenuController()?.leftPanGesture?.isEnabled=true
        
    }
    // called on finger up if the user dragged. decelerate is true if it will continue moving afterwards
    @available(iOS 2.0, *)
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        debugPrint("scrollViewDidEndDragging")
        self.slideMenuController()?.leftPanGesture?.isEnabled=true
        
    }
}

extension LeftViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if  let menu = LeftMenu(rawValue: indexPath.row) {
            AppDelegate.sharedInstance().indexpathOfDrawerSel = indexPath
            tblDrawerMenu.reloadData()
            self.changeViewController(menu)
        }
    }
}

extension LeftViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
      //  viewDrawerHeader.viewStatusBar.backgroundColor = colorset.statusBarColor
        viewDrawerHeader.backgroundColor = colorset.drawerBackground
        viewDrawerHeader.imgHeaderBG.image = UIImage(named: "placeholdericon")
        viewDrawerHeader.imgHeaderBG.layer.cornerRadius = viewDrawerHeader.imgHeaderBG.frame.size.height / 2
        viewDrawerHeader.imgHeaderBG.clipsToBounds = true
        viewDrawerHeader.imgHeaderBG.borderWidth = 2.0
        viewDrawerHeader.imgHeaderBG.borderColor = UIColor.white
        
        viewDrawerHeader.lblUserName.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        viewDrawerHeader.lblUserName.textColor = colorset.whitetextColor
        if getUserProfileDataList() != nil
        {
        viewDrawerHeader.lblUserName.text = getUserProfileDataList()?.firstName
            viewDrawerHeader.imgHeaderBG.sd_setImage(with: URL(string: getUserProfileDataList()?.profileImage ?? ""), placeholderImage: UIImage(named: "placeholdericon"))
        }
        viewDrawerHeader.btnProfile.addTarget(self, action: #selector(btnProfileTapped), for: .touchUpInside)
        
        return viewDrawerHeader
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DrawerCell", for: indexPath) as! DraweMenuCell
        
        cell.lblMenuTitle.text = (menus.object(at: indexPath.row) as! String)
        cell.lblMenuTitle.font = UIFont(name: FONT.Regular, size: FONT_SIZE.EIGHTEEN)
        cell.lblMenuTitle.textColor = colorset.whitetextColor
        cell.imgMenu.tintColor = colorset.whitetextColor
        cell.imgMenu.image = UIImage.init(named: arrImgDrawer[indexPath.row] as! String)?.withRenderingMode(.alwaysTemplate)
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        
        if AppDelegate.sharedInstance().indexpathOfDrawerSel.count == 0 && indexPath.row == 0{
            cell.backgroundColor = colorset.whitetextColor
            cell.lblMenuTitle.textColor = colorset.drawerBackground
            cell.imgMenu.tintColor = colorset.drawerBackground
        }
        else if AppDelegate.sharedInstance().indexpathOfDrawerSel == indexPath{
            cell.backgroundColor = colorset.whitetextColor
            cell.lblMenuTitle.textColor = colorset.drawerBackground
            cell.imgMenu.tintColor = colorset.drawerBackground

        }
        
        return cell
    }
}

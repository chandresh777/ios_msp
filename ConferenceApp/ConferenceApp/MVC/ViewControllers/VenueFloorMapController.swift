//
//  VenueFloorMapController.swift
//  ConferenceApp
//
//  Created by MSP on 13/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
import SDWebImage
class VenueFloorMapController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var tblVenueFloorMap: UITableView!
    @IBOutlet weak var imgvAds: UIImageView!
    
    var getVenuData : VenuFloorData? = nil

    override func viewDidLoad() {
    
        super.viewDidLoad()
        
        self.imgvAds.image = UIImage.animatedImage(withAnimatedGIFURL: URL(string: Gifurlads)!)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.imgvAds.addGestureRecognizer(tap)
        self.imgvAds.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
        self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")
        headerView.lblHeader.text = "Venue Floor Map"

        self.getVeneuFloorList()
        
//        self.tblVenueFloorMap.delegate = self
//        self.tblVenueFloorMap.dataSource = self
//        self.tblVenueFloorMap.reloadData()

        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        if let url = URL(string: GifurladsRedirect) {
            UIApplication.shared.open(url, options: [:]) }
        
    }
    
    func getVeneuFloorList()
    {
        

    
    
    VenuFloorModel.sharedModel().getVenuFloor(conference_id: AppDelegate.sharedInstance().conference.data[0].iD, {  (response) in
        if response.code == 1{
            self.getVenuData = response
            self.tblVenueFloorMap.dataSource = self
            self.tblVenueFloorMap.delegate = self
            self.tblVenueFloorMap.reloadData()
        }
        else
        {
        self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
    
        }, nil])
    }
    
    }) { (request, sender, json, error) in
        Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
    
        switch selectedIndex {
        case CONSTANTs.TWO: //for retry
    
    //Handle action
        self.getVeneuFloorList()
    
    break
    
    default:
    
    // ignore case
    
    break
    }
    
    })
    }
    
    
}
    
    
    
    
    //MARK: -Tableview Delegate Methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VenueFloorMapCell", for: indexPath) as! VenueFloorMapCell
        cell.imgMap.backgroundColor =  UIColor.red
        
        if indexPath.row == 0 {
            
            cell.imgMap.sd_setImage(with: URL(string:  (self.getVenuData?.data.floorPlanOne)!), placeholderImage: UIImage(named: "imgLoginLogo"))

        }else {
            cell.imgMap.sd_setImage(with: URL(string:  (self.getVenuData?.data.floorPlanTwo)!), placeholderImage: UIImage(named: "imgLoginLogo"))
        }
        
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            
            let zoomVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "ZoomImageController") as! ZoomImageController
            zoomVC.imgString = (self.getVenuData?.data.floorPlanOne)!
            zoomVC.modalPresentationStyle = .overCurrentContext
            self.present(zoomVC, animated: true, completion: nil)
        }else {
            
            let zoomVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "ZoomImageController") as! ZoomImageController
            zoomVC.imgString = (self.getVenuData?.data.floorPlanTwo)!
            zoomVC.modalPresentationStyle = .overCurrentContext
            self.present(zoomVC, animated: true, completion: nil)
            
        }
        
        

    }
    
    
    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        slideMenuController()?.toggleLeft()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  NotificationDetailViewController.swift
//  ConferenceApp
//
//  Created by MSP on 31/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class NotificationDetailViewController: BaseViewController {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblDate: UILabel!
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        
        setLayoutMethod()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }

    
    //MARK:- Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "Notification Detail"
        lblTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 17.0))
        lblDesc.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 17.0))
        lblDate.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 17.0))
        lblTitle.textColor = colorset.blacktextcolor
        lblDesc.textColor = colorset.lightBlackTextColor
        lblDate.textColor = colorset.lightBlackTextColor
        lblDesc.text = "New subscribers only. Plan automatically renews after trial.Group FaceTime will be available in iOS 12 later this year through a software update.Some AR apps shown may not be available in all regions or all languages.FaceTime will be available in iOS 12 later this year through a software update."
    }
    
    //MARK:- Button Clicked Events
    override func btnLeftBtnTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Memory management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

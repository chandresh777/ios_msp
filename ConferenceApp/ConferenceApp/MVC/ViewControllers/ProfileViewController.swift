//
//  ProfileViewController.swift
//  ConferenceApp
//
//  Created by msp on 27/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
import MaterialTextField
import SDWebImage
class ProfileViewController: BaseViewController,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UIGestureRecognizerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var txtHeight: MFTextField!
    @IBOutlet weak var txtWeight: MFTextField!
    @IBOutlet weak var lblWalkTitle: UILabel!
    @IBOutlet weak var viewSepertorSubCat: UIView!
    @IBOutlet weak var viewseperatorCat: UIView!
    @IBOutlet weak var viewSubCat: UIView!
    @IBOutlet weak var heightStackCat: NSLayoutConstraint!
    @IBOutlet weak var txtSubCategory: UITextField!
    @IBOutlet weak var txtCategory: UITextField!
    //@IBOutlet weak var picker_view: UIPickerView!
    @IBOutlet weak var imgvProfile: UIImageView!
    @IBOutlet weak var viewBGImg: UIView!
    
    @IBOutlet weak var lblFirstNameTitle: UILabel!
    
    
    @IBOutlet weak var txtFName: MFTextField!
    
    @IBOutlet weak var lblLastNameTitle: UILabel!
    
    
    @IBOutlet weak var txtLname: MFTextField!
    
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var lblGender: UILabel!
    
    @IBOutlet weak var txtEmail: MFTextField!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFmale: UIButton!
    
    @IBOutlet weak var lblMobNumber: UILabel!
    
    @IBOutlet weak var lblCategory: UILabel!
    
    var picker_view: UIPickerView!
    
    var img_picker:UIImagePickerController!
    @IBOutlet weak var txtMobNumber: MFTextField!
    @IBOutlet weak var btnUpdate: btnThemBlueClass!
    @IBOutlet weak var lblSubCategory: UILabel!
    var strpresent:String!
    var arrCategory:[String] = ["dfds","gdsgds","gsdgds","gsdgdsg","erewrewr"]
    var selectIndexCategory = 0
    var selectIndexSubCategory = 0
    var strPicker = ""
    
    var resultProfileCategory : ProfileCategoryRootClass? = nil
    
    var resultProfileSubCategory : ProfileSubCategoryRootClass? = nil
var strimgpick = "no"
    override func viewDidLoad() {
        super.viewDidLoad()
        btnMale.isSelected = true
        imgvProfile.image = UIImage(named: "placeholdericon")
        setLayout()
         addToolBar(textField: txtWeight)
       addToolBar(textField: txtHeight)
        
        if getUserProfileDataList()?.isProfileFilled == "0" {
            
            self.headerView.btnLeftBtn.isHidden = true
        }
        else
        {
            self.headerView.btnLeftBtn.isHidden = false
            
        }
        // Do any additional setup after loading the view.
    }
    
    func setLayout()
    {
        imgvProfile.isUserInteractionEnabled = true
        let tap_gest = UITapGestureRecognizer(target: self, action: #selector(tap_clickImg(_:)))
        tap_gest.delegate = self
        imgvProfile.addGestureRecognizer(tap_gest)
        
        self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        
      
        
        headerView.lblHeader.text = "Profile"
        imgvProfile.layer.cornerRadius = 68
        imgvProfile.layer.masksToBounds = true
        
        picker_view = UIPickerView(frame: CGRect(x: 0, y: 200, width: UIScreen.main.bounds.size.width, height: 220))
       
        picker_view.backgroundColor = UIColor.white
        picker_view.showsSelectionIndicator = true
        let toolBar = UIToolbar()
        toolBar.translatesAutoresizingMaskIntoConstraints = false
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = colorset.blacktextcolor
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(btnclickDone))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action:  #selector(btnclickCancel))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        lblWalkTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 16.0))
        lblWalkTitle.textColor = colorset.blacktextcolor
        lblWalkTitle.text = "Required details for Digital walk"
        self.txtFName.tintColor = colorset.textFieldLine
        txtFName.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        txtFName.textColor = colorset.blacktextcolor
        self.txtFName.defaultPlaceholderColor = colorset.textFieldLine
        self.txtFName.placeholderAnimatesOnFocus = true;
        txtFName.attributedPlaceholder = NSAttributedString(string: "FIRST NAME", attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: FontSizeFunc(size: 16))])
        
        self.txtLname.tintColor = colorset.textFieldLine
        txtLname.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        txtLname.textColor = colorset.blacktextcolor
        self.txtLname.defaultPlaceholderColor = colorset.textFieldLine
        self.txtLname.placeholderAnimatesOnFocus = true;
        txtLname.attributedPlaceholder = NSAttributedString(string: "LAST NAME", attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: FontSizeFunc(size: 16))])
        
        self.txtEmail.tintColor = colorset.textFieldLine
        txtEmail.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        txtEmail.textColor = colorset.blacktextcolor
        self.txtEmail.defaultPlaceholderColor = colorset.textFieldLine
        self.txtEmail.placeholderAnimatesOnFocus = true;
        txtEmail.attributedPlaceholder = NSAttributedString(string: "ENTER EMAIL ID", attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: FontSizeFunc(size: 16))])
        
        
        self.txtMobNumber.tintColor = colorset.textFieldLine
        txtMobNumber.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        txtMobNumber.textColor = colorset.blacktextcolor
        self.txtMobNumber.defaultPlaceholderColor = colorset.textFieldLine
        self.txtMobNumber.placeholderAnimatesOnFocus = true;
        txtMobNumber.attributedPlaceholder = NSAttributedString(string: "ENTER MOBILE NUMBER", attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: FontSizeFunc(size: 16))])
        
        
        
        self.txtWeight.tintColor = colorset.textFieldLine
        txtWeight.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        txtWeight.textColor = colorset.blacktextcolor
        self.txtWeight.defaultPlaceholderColor = colorset.textFieldLine
        self.txtWeight.placeholderAnimatesOnFocus = true;
        txtWeight.attributedPlaceholder = NSAttributedString(string: "Weight(Kg)", attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: FontSizeFunc(size: 16))])
        
        
        self.txtHeight.tintColor = colorset.textFieldLine
        txtHeight.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        txtHeight.textColor = colorset.blacktextcolor
        self.txtHeight.defaultPlaceholderColor = colorset.textFieldLine
        self.txtHeight.placeholderAnimatesOnFocus = true;
        txtHeight.attributedPlaceholder = NSAttributedString(string: "Height(cm.)", attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: FontSizeFunc(size: 16))])
        
        
        
         viewseperatorCat.backgroundColor = colorset.textFieldLine
        viewSepertorSubCat.backgroundColor = colorset.textFieldLine
        txtCategory.inputView = picker_view
        txtCategory.inputAccessoryView = toolBar
        
        txtSubCategory.inputView = picker_view
        txtSubCategory.inputAccessoryView = toolBar
        
        
        self.picker_view.reloadAllComponents()
        //txtSubCategory.inputView = picker_view
        
        imgvProfile.layer.borderWidth = 2.0
        imgvProfile.layer.borderColor = UIColor.init(hexColorString: "5248FB").cgColor
        imgvProfile.layer.cornerRadius = imgvProfile.frame.width/2
        imgvProfile.layer.masksToBounds = true
        
        //    lblFirstNameTitle.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 14.0))
        //    lblLastNameTitle.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 14.0))
        //    lblEmail.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 14.0))
        //    lblMobNumber.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 14.0))
        
        
        txtFName.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        txtLname.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        txtEmail.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        txtMobNumber.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        txtWeight.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        txtHeight.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        lblGender.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 16.0))
        lblCategory.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        lblSubCategory.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        
        btnMale.titleLabel?.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        btnFmale.titleLabel?.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        
        btnUpdate.titleLabel?.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        //    txtFName.placeholder = "Enter First Name"
        //    txtLname.placeholder = "Enter Last Name"
        //    txtEmail.placeholder = "Enter Email ID"
        //    txtMobNumber.placeholder = "Enter Mobile No."
        
        //    lblFirstNameTitle.textColor = colorset.lightBlackTextColor
        //    lblLastNameTitle.textColor = colorset.lightBlackTextColor
        //    lblEmail.textColor = colorset.lightBlackTextColor
        //    lblMobNumber.textColor = colorset.lightBlackTextColor
        
        txtFName.textColor = colorset.blacktextcolor
        txtLname.textColor = colorset.blacktextcolor
        txtEmail.textColor = colorset.blacktextcolor
        txtMobNumber.textColor = colorset.blacktextcolor
        txtCategory.delegate = self
        txtSubCategory.delegate = self
        lblCategory.textColor = colorset.blacktextcolor
        lblSubCategory.textColor = colorset.blacktextcolor
        btnMale.tintColor = colorset.blacktextcolor
        btnFmale.tintColor = colorset.blacktextcolor
        
        btnMale.titleLabel?.textColor = colorset.blacktextcolor
        btnFmale.titleLabel?.textColor = colorset.blacktextcolor
        
        
        viewBGImg.backgroundColor = colorset.navigationBar
        
        txtFName.delegate = self
        txtLname.delegate = self
        txtEmail.delegate = self
        txtMobNumber.delegate = self
        txtFName.returnKeyType = .next
        txtLname.returnKeyType = .next
        txtEmail.returnKeyType = .next
        txtMobNumber.returnKeyType = .next
        txtMobNumber.isUserInteractionEnabled = false
        callCategoryAPI()
         txtMobNumber.text = getUserProfileDataList()?.mobileNo ?? ""
        if getUserProfileDataList()?.firstName != ""
        {
        setalluserData()
        }
    }
    //MARK: Set all user DATA
    @objc func setalluserData()
    {
        txtFName.text = getUserProfileDataList()?.firstName ?? ""
        txtLname.text = getUserProfileDataList()?.lastName ?? ""
        txtEmail.text = getUserProfileDataList()?.email ?? ""
       txtWeight.text = getUserProfileDataList()?.weight ?? ""
       txtHeight.text =  getUserProfileDataList()?.height ?? ""
        
        imgvProfile.sd_setImage(with: URL(string: getUserProfileDataList()?.profileImage ?? ""), placeholderImage: UIImage(named: "placeholdericon"))
    lblCategory.text = getUserProfileDataList()?.catName ?? ""
        if  lblCategory.text == "Doctor"
        {
            heightStackCat.constant = 100
            viewSubCat.isHidden = false
            lblSubCategory.text = resultProfileSubCategory?.data[selectIndexSubCategory].name
            lblSubCategory.text = getUserProfileDataList()?.specializeName ?? ""
        }
        else{
            heightStackCat.constant = 50
            viewSubCat.isHidden = true
            
        }
        if getUserProfileDataList()?.gender == "Male"
        {
        btnMale.isSelected = true
            btnFmale.isSelected = false
        }
        else{
            btnMale.isSelected = false
            btnFmale.isSelected = true
        }
        
        
    }
    
    override func btnLeftBtnTapped() {
        if strpresent == "yes"
        {
        self.dismiss(animated: true, completion: nil)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
        
//        slideMenuController()?.toggleLeft()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    //MARK: - Tap Click Img
    @objc func tap_clickImg(_ gest:UITapGestureRecognizer)
    {
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Choose option", message: "", preferredStyle: .actionSheet)
        
        
        
        let cameraButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            print("Save")
            self.img_picker = UIImagePickerController()
            self.img_picker.delegate = self
            self.img_picker.sourceType = .camera
            self.present(self.img_picker, animated: true, completion: nil)
        }
        actionSheetControllerIOS8.addAction(cameraButton)
        
        let gallaryButton = UIAlertAction(title: "Gallary", style: .default)
        { _ in
            print("Delete")
            self.img_picker = UIImagePickerController()
            self.img_picker.delegate = self
            self.img_picker.sourceType = .photoLibrary
            self.present(self.img_picker, animated: true, completion: nil)
        }
        
        
        actionSheetControllerIOS8.addAction(gallaryButton)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
        
        
        
    }
    //MARK: - UIIMage picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
//            imgvProfile.contentMode = .scaleAspectFit
            imgvProfile.image = pickedImage
        }
        strimgpick = "yes"
        img_picker.dismiss(animated: true, completion: nil)
    }
    //MARK: - Picker method
    @objc func btnclickDone()
    {
        if strPicker == "cat"
        {
            
            lblCategory.text = resultProfileCategory?.data[selectIndexCategory].catName
        if resultProfileCategory?.data[selectIndexCategory].isDoctor == "1"
        {
            heightStackCat.constant = 100
            viewSubCat.isHidden = false
            lblSubCategory.text = resultProfileSubCategory?.data[selectIndexSubCategory].name
            }
        else{
            heightStackCat.constant = 50
            viewSubCat.isHidden = true
            
            }
        }
        else if strPicker == "subcat"
        {
            lblSubCategory.text = resultProfileSubCategory?.data[selectIndexSubCategory].name
        }
        
        self.view.endEditing(true)
        
    }
    @objc func btnclickCancel()
    {
        self.view.endEditing(true)
    }
    //MARK: - UITextField delegate method
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == txtCategory
        {
            selectIndexCategory = 0
            selectIndexSubCategory = 0
            strPicker = "cat"
            picker_view.selectRow(0, inComponent: 0, animated: false)
            picker_view.reloadAllComponents()
        }
        else if textField == txtSubCategory{
            strPicker = "subcat"
            selectIndexSubCategory = 0
            picker_view.selectRow(0, inComponent: 0, animated: false)
            picker_view.reloadAllComponents()
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFName
        {
            txtLname.becomeFirstResponder()
        }
        else if textField == txtLname
        {
            txtEmail.becomeFirstResponder()
        }
        else if textField == txtEmail
        {
            txtCategory.becomeFirstResponder()
        }
        
        return true
    }
    
    //MARK:- PickerView Delegate Method
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    // returns the # of rows in each component..
    @available(iOS 2.0, *)
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
         if strPicker == "cat"
         {
        return (resultProfileCategory?.data.count)!
        }
         else{
          return  (resultProfileSubCategory?.data.count)!
           
        }
      //  return arrCategory.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if strPicker == "cat"
        {
        return resultProfileCategory?.data[row].catName
        }
        else{
           return resultProfileSubCategory?.data[row].name
            
        }

    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if strPicker == "cat"
        {
            selectIndexCategory = row
        }
        else if strPicker == "subcat"
        {
            selectIndexSubCategory = row
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: -API Call
    
    
    func callCategoryAPI()  {
        ProfileModel.sharedModel().getCategoryAPI({ (response) in
            if response.code == 1{
                self.resultProfileCategory = response
                self.callSubCategoryAPI()
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.callCategoryAPI()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    func callSubCategoryAPI()  {
        ProfileModel.sharedModel().getSubCategoryAPI({ (response) in
            if response.code == 1{
                self.picker_view.delegate = self
                self.picker_view.dataSource = self
                self.resultProfileSubCategory = response
                if getUserProfileDataList()?.firstName == ""
                {
                 self.lblCategory.text = self.resultProfileCategory?.data[self.selectIndexCategory].catName
                self.lblSubCategory.text = self.resultProfileSubCategory?.data[self.selectIndexSubCategory].name
                }
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.callCategoryAPI()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    
    @IBAction func btnclickUpdate(_ sender: Any) {
        
        if txtFName.text?.length == 0
        {
            self.popupAlert(title: AppName.appName, message: ValidationMsg.addAddressFirstName, actionTitles: ["Ok"], actions:[{action1 in
                
                }, nil])
        }
        else if txtLname.text?.length == 0
        {
            self.popupAlert(title: AppName.appName, message: ValidationMsg.addAddressLastName, actionTitles: ["Ok"], actions:[{action1 in
                
                }, nil])
        }
        else if txtEmail.text?.length == 0
        {
            self.popupAlert(title: AppName.appName, message: ValidationMsg.addEmail, actionTitles: ["Ok"], actions:[{action1 in
                
                }, nil])
        }
            else if txtEmail.text?.isValidEmail == false
        {
            self.popupAlert(title: AppName.appName, message: ValidationMsg.validEmail, actionTitles: ["Ok"], actions:[{action1 in
                
                }, nil])
        }
        else{
          callprofile()
        }
    }
    @IBAction func btnMaleClick(_ sender: Any) {
        btnFmale.isSelected = false
        btnMale.isSelected = true
    }
    @IBAction func btnFmaleClick(_ sender: Any) {
        btnMale.isSelected = false
        btnFmale.isSelected = true
    }
    
    func callprofile()
    {
        var gender:String!
        if btnMale.isSelected
        {
            gender = "Male"
        }
        else{
           gender = "Female"
        }
        var catid = resultProfileCategory?.data[selectIndexCategory].iD
        var specialize_id = ""
        if resultProfileCategory?.data[selectIndexCategory].isDoctor == "1"
        {
            specialize_id = (resultProfileSubCategory?.data[selectIndexSubCategory].iD)!
        }
        
      
        ProfileModel.sharedModel().UpdateProfileImageCallApi(first_name: txtFName.text!, last_name: txtLname.text!, address: "", mobile_no: txtMobNumber.text!, gender: gender, email: txtEmail.text!, category_id: catid!, specialize_id: specialize_id, user_id: getUserProfileDataList()?.iD ?? "",attachimgYN:strimgpick, profile_img: imgvProfile.image,weight:txtWeight.text ?? "",height:txtHeight.text ?? "",{ (response) in
            if response.code == 1 {
              print(response)
                
               NotificationCenter.default.post(name: Notification.Name("setuserInfo"), object: nil)
                if self.strpresent == "yes"
                {
                self.dismiss(animated: true, completion: nil)
                }
                else{
                    Utility.moveToHomeViewCotroller()
                }
            }else
            {
                self.popupAlert(title: AppName.appName, message:response.msg , actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
                
            }
            
        }, fail_block: { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                self.callprofile()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
            
        })
        
    }

}

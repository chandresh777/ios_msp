//
//  ViewController.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit


class HomeVC: BaseViewController,UIGestureRecognizerDelegate{
    
//    private let activityManager = CMMotionActivityManager()
//    private let pedometer = CMPedometer()
//    private var shouldStartUpdating: Bool = false
//    private var startDate: Date? = nil

    @IBOutlet weak var lblNotificationTitle: UILabel!
    @IBOutlet weak var imgvAds: UIImageView!
    
    @IBOutlet weak var lblMeetingreqTitle: UILabel!
    
    @IBOutlet weak var viewDitalWalk: UIView!
    @IBOutlet weak var lblScanTitle: UILabel!
    @IBOutlet weak var lblDateDigi: UILabel!
    @IBOutlet weak var lblStepTitle: UILabel!
    
    @IBOutlet weak var lblStepCountVal: UILabel!
    @IBOutlet weak var lblCalrieTitle: UILabel!
    @IBOutlet weak var lblSelfieTitle: UILabel!
    
    @IBOutlet weak var lblCalrieVal: UILabel!
    @IBOutlet weak var lblDigitalWalkTitle: UILabel!
    
    @IBOutlet weak var vwNotification: UIControl!
    @IBOutlet weak var vwmeetingReq: UIControl!
    @IBOutlet weak var vwscan: UIControl!
    @IBOutlet weak var vwselfie: UIControl!
    
    var customPopUPView:CustomAlertPopView!
    var strQrCode:String = ""
    var timerUpdate: Timer!
    var dashboardData:DashBoardRoot!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if getUserProfileDataList() != nil
        {
            AppDelegate.sharedInstance().userId = (getUserProfileDataList()?.iD)!
        }
        else{
            AppDelegate.sharedInstance().userId = ""
        }
        self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")
        setLayout()
        getconfernce()
        customPopUPView = Bundle.main.loadNibNamed("CustomAlertPopView", owner: self, options: nil)![0] as! CustomAlertPopView
        customPopUPView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.size.height)
        self.view.addSubview(customPopUPView)
       
        if  getUserProfileDataList()?.selected_digital == "0"
        {
            customPopUPView.isHidden = false
            customPopUPView.lblAlertMessage.isHidden = true
            customPopUPView.txtEnterCode.isHidden = false
        }
        else{
          customPopUPView.isHidden = true
        }
       
        customPopUPView.btnOkey.setTitle("Submit", for: .normal)
        customPopUPView.btnCancel.setTitle("Cancel", for: .normal)
        customPopUPView.btnCancel.addTarget(self, action: #selector(btnclickCancelPOp(_:)), for: .touchUpInside)
         customPopUPView.btnOkey.addTarget(self, action: #selector(btnclickOKPOp(_:)), for: .touchUpInside)
       
       
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateValueWalk), name: Notification.Name("updateValueWalk"), object: nil)

//        self.updateStepCount()
        timerUpdate = Timer.scheduledTimer(timeInterval: 900, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)

    }

    override func viewWillLayoutSubviews() {
        viewDitalWalk.layer.cornerRadius = viewDitalWalk.frame.size.width/2
        
        viewDitalWalk.layer.borderWidth = 5.0
        viewDitalWalk.layer.borderColor = UIColor.init(hexColorString: "5248FB").cgColor
        viewDitalWalk.layer.masksToBounds = true
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        if strQrCode != ""
        {
            callphysical()
        }
        
       self.updateValueWalk()
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    //MARK: -Custom Access Methods
    
    @objc func updateValueWalk() {
        lblCalrieVal.text = AppDelegate.sharedInstance().strCaloriesBurn
        lblStepCountVal.text = AppDelegate.sharedInstance().strStepCount
    }
    
    func setLayout()  {
        vwNotification.backgroundColor = UIColor.init(hexColorString: "5248FB")
        vwmeetingReq.backgroundColor = UIColor.init(hexColorString: "C44AF7")
        vwscan.backgroundColor = UIColor.init(hexColorString: "FEB231")
        vwselfie.backgroundColor = UIColor.init(hexColorString: "FB5A97")
        lblDateDigi.textColor = UIColor.init(hexColorString: "484848")
        self.view.backgroundColor = colorset.backroundOfView
        
        self.headerView.lblHeader.text = "Dashboard"
        lblNotificationTitle.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 14.0))
        lblMeetingreqTitle.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 14.0))
        lblScanTitle.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 14.0))
        lblSelfieTitle.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 14.0))
        
        lblDigitalWalkTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 17.0))
        lblDateDigi.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        
        lblCalrieTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        lblStepTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        lblCalrieVal.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 18.0))
        lblStepCountVal.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 18.0))
        
        lblCalrieTitle.textColor = colorset.blacktextcolor
        lblStepTitle.textColor = colorset.blacktextcolor
        lblDigitalWalkTitle.textColor = colorset.blacktextcolor
        lblDateDigi.textColor = colorset.blacktextcolor
        
        let date = NSDate()
        let dateformate = DateFormatter()
        dateformate.dateFormat = "dd/MM/yy"
        let strCurentDate = dateformate.string(from: date as Date)
        print(strCurentDate)
        lblDateDigi.text = strCurentDate
        

        lblNotificationTitle.textColor = colorset.whitetextColor
        lblMeetingreqTitle.textColor = colorset.whitetextColor
        lblScanTitle.textColor = colorset.whitetextColor
        lblSelfieTitle.textColor = colorset.whitetextColor
    }
    
    @objc func update() {
        self.updateStepCount()
    }
    
    
    
    //MARK: - POPUP Action
    @objc func btnclickCancelPOp(_ sender:UIButton)
    {
        customPopUPView.isHidden = true
    }
    @objc func btnclickOKPOp(_ sender:UIButton)
    {
        if customPopUPView.txtEnterCode.text == ""
        {
            self.popupAlert(title: AppName.appName, message: ValidationMsg.sponsercodeEnter, actionTitles: ["Ok"], actions:[{action1 in
                
                }, nil])
        }
        else
        {
        callgetsponserLink()
        }
    }
    @objc func getconfernce()
    {
       ConferenceModel.sharedModel().getConference(userid:"", { (response) in
            if response.code == 1{
                
                AppDelegate.sharedInstance().conference = response
                self.getCMSPageList()
                self.getdashBoardData()
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.getconfernce()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    //MARK: - Get Dashboard apicall
    func getdashBoardData()
    {
        DashBoardModel.sharedModel().getdashBoard(user_id: AppDelegate.sharedInstance().userId, conference_id: AppDelegate.sharedInstance().conference.data[0].iD,  { (response) in
            if response.code == 1{
               self.dashboardData = response
                Gifurlads = self.dashboardData.data.advertiseBanner
                GifurladsRedirect = self.dashboardData.data.redirectUrl
                 self.imgvAds.image = UIImage.animatedImage(withAnimatedGIFURL: URL(string: Gifurlads)!)
                

                let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                
                self.imgvAds.addGestureRecognizer(tap)
                
                self.imgvAds.isUserInteractionEnabled = true
                

                
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.getdashBoardData()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    //MARK: - Call sponsor link
    func updateStepCount() {
        DashBoardModel.sharedModel().updateStep(user_id: (getUserProfileDataList()?.iD)!, conference_id: AppDelegate.sharedInstance().conference.data[0].iD, steps: lblStepCountVal.text!, calorie: lblCalrieVal.text!, { (response, sender, json, error) in
            
        }) { (request, sender, json, error) in
            
        }
    }
    
    func callgetsponserLink()
    {
        DashBoardModel.sharedModel().getAdsLink(user_id: (getUserProfileDataList()?.iD)!, sponsor_id: customPopUPView.txtEnterCode.text!, conference_id: AppDelegate.sharedInstance().conference.data[0].iD, is_flag: "2",  { (response) in
            if response.code == 1{
                self.customPopUPView.isHidden = true
                self.customPopUPView.txtEnterCode.resignFirstResponder()
                AppDelegate.sharedInstance().sopnseorData = response
                getUserProfileDataList()?.selected_digital = "1"
                self.callLoginAPI()
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.callgetsponserLink()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    @objc func callphysical()
    {
        DashBoardModel.sharedModel().getAdsLink(user_id: (getUserProfileDataList()?.iD)!, sponsor_id: strQrCode, conference_id: AppDelegate.sharedInstance().conference.data[0].iD, is_flag: "1",  { (response) in
            if response.code == 1{
                self.customPopUPView.isHidden = true
                AppDelegate.sharedInstance().sopnseorData = response
                
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.callgetsponserLink()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
        
    }
    
    func getCMSPageList() {
        GetCMSPageModel().getCMSPageLink(conference_id:AppDelegate.sharedInstance().conference.data[0].iD ,{ (response) in
            if response.code == 1{
                //                print(getCMSPageListData()?.historyConference ?? "")
                
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.getCMSPageList()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    
    func callLoginAPI() {
        LoginFlowModel.sharedModel().callLoginAPI(mobile_no: getUserProfileDataList()?.mobileNo ?? "", { (response) in
            if response.code == 1{
                
                
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.callLoginAPI()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    
    //MARK: - TapGesture
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
    
//        let objCommanWebViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as! CommanWebViewController
//        objCommanWebViewController.strNavTitle = "Ads"
//        objCommanWebViewController.strUrl = GifurladsRedirect
//        self.navigationController?.pushViewController(objCommanWebViewController, animated: true)
        if let url = URL(string: GifurladsRedirect) {
            UIApplication.shared.open(url, options: [:]) }
        
    }
    
    //MARK: -Button Clicked Events

    @IBAction func btnclickScan(_ sender: Any) {
        let qrcodeVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
        qrcodeVC.homeViewctl = self
    self.present(qrcodeVC, animated: true, completion: nil)
    }
    @IBAction func btnMyFavoriteClick(_ sender: Any) {
        let vc : MyFavouriteViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyFavouriteViewController") as! MyFavouriteViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSelfieClicked(_ sender: Any) {
        let vc : SelfieViewController = self.storyboard?.instantiateViewController(withIdentifier: "SelfieViewController") as! SelfieViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnNotificationClicked(_ sender: Any) {
        let vc : NotificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        vc.isFromMenu = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func btnLeftBtnTapped() {
        slideMenuController()?.toggleLeft()
    }
}

extension HomeVC : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        debugPrint("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        debugPrint("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        
        debugPrint("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        debugPrint("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        debugPrint("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        debugPrint("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        debugPrint("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        debugPrint("SlideMenuControllerDelegate: rightDidClose")
    }
}


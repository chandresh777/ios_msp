//
//  SessionListVC.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class SessionListVC: BaseViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate, UISearchBarDelegate {
    
    
    @IBOutlet weak var tblSessionList: UITableView!
    var arrSection:[String] = ["Hall A","Hall B","Hall C"]
    var arrrow:[[String]] = [["Session A1","Session A1","Session A1"],["Session B1","Session N1","Session A1"],["Session D1","Session C1"]]
    var arrowtemp:[[String]] = []
    var strDate:String!
    @IBOutlet weak var searchBar: UISearchBar!
    var objCustomAlertViewClass = CustomAlertView()
    var arropenArrow:[Int] = []
    var sessionList:SessionListRoot!
     var sessionListTemp:[Int:[Session]] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        tblSessionList.dataSource = nil
        tblSessionList.delegate = nil
        getsessionList()
        setLayoutMethod()
        self.searchBar.tintColor = UIColor.black
        
    }
    func getsessionList()
    {
        
         ConferenceDateSessionModel.sharedModel().getSessionList(date: strDate, conference_id: AppDelegate.sharedInstance().conference.data[0].iD, { (response) in
            if response.code == 1{
                self.sessionList = response
                
               
                
                for i in 0..<self.sessionList.data.count
                {
                    var arrsession:[Session] = []
                    for j in 0..<self.sessionList.data[i].session.count
                    {
                        arrsession.append(self.sessionList.data[i].session[j])
                    }
                    self.sessionListTemp[i] = arrsession
                }
                self.closealltab()
                self.tblSessionList.dataSource = self
                self.tblSessionList.delegate = self
                self.tblSessionList.reloadData()
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.getsessionList()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        arrowtemp = arrrow
    
       
        // Do any additional setup after loading the view.
        headerView.lblHeader.text = "My Session"
        self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        tblSessionList.tableFooterView = UIView()
        
//        self.headerView.lblHeader.text = "SessionList"
        
        
        searchBar.barTintColor = colorset.navigationBar
        searchBar.backgroundColor = colorset.navigationBar
        searchBar.backgroundImage = UIImage()
        searchBar.showsCancelButton = true
        
        
        for view in (searchBar.subviews.last?.subviews)! {
            if view is UITextField{
                (view as! UITextField).font = UIFont(name: FONT.Regular, size: 16.0)
                (view as! UITextField).textColor = colorset.lightBlackTextColor
            }
            else if view is UIButton{
                (view as! UIButton).titleLabel?.font = UIFont(name: FONT.Regular, size: 14.0)
                (view as! UIButton).setTitleColor(colorset.lightBlackTextColor, for: .normal)
            }
            
        }
        
        searchBar.showsCancelButton = false
        
        searchBar.placeholder = "Search"
        searchBar.delegate = self
    }
    
    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - UITableview delegate method
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 55
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.estimatedRowHeight
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return sessionList.data.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 55))
        viewHeader.backgroundColor = colorset.navigationBar
        
        viewHeader.isUserInteractionEnabled = true
        let lbltitle = UILabel(frame: CGRect(x: 20, y: 10, width: UIScreen.main.bounds.size.width-100, height: 30))
        lbltitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        lbltitle.textColor = colorset.blacktextcolor
        lbltitle.text = sessionList.data[section].hallname
        viewHeader.addSubview(lbltitle)
        
        
        
        let imgv = UIImageView(frame: CGRect(x: UIScreen.main.bounds.size.width - 40, y: 10, width:20, height: 20))
        imgv.contentMode = .scaleAspectFit
        if arropenArrow[section] == 1
        {
            imgv.image = UIImage(named: "imgdownarrow")
            
            
            
        }
        else{
            
            imgv.image = UIImage(named: "imgrightarrow")
        }
        viewHeader.tag = section
        viewHeader.addSubview(imgv)
        
        let viewwhite = UIView(frame: CGRect(x: 0, y: 50, width: UIScreen.main.bounds.size.width, height: 5))
        viewwhite.backgroundColor = UIColor.white
        
        viewHeader.addSubview(viewwhite)
        
        
        let tapGest = UITapGestureRecognizer(target: self, action: #selector(tapClick(_:)))
        tapGest.delegate = self
        viewHeader.addGestureRecognizer(tapGest)
        
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return sessionList.data[section].session.count
        
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SessionListCell
        
        cell.lblSessionName.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 17.0))
        cell.lblSessionDate.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 17.0))
      
        let objw = sessionList.data[indexPath.section].session[indexPath.row]
        
        cell.lblSessionDate.text = String(format: "Start at %@",objw.startTime )
        cell.lblSessionName.text = objw.name
        let strdate1 = String(format: "%@ %@", objw.sessionDate,objw.startTime)
        let strdate2 = String(format: "%@ %@", objw.sessionDate,objw.endTime)
        if datecompare(strdate1: strdate1, strdate2: strdate2)
        {
            cell.backgroundColor = colorset.buttonBGcolor
            cell.lblSessionName.textColor = colorset.whitetextColor
            cell.lblSessionDate.textColor = colorset.whitetextColor
        }
        else{
            cell.lblSessionName.textColor = colorset.blacktextcolor
            cell.lblSessionDate.textColor = colorset.blacktextcolor
            cell.backgroundColor = UIColor.white
        }
        return cell
    }
    
    
    @objc func tapClick(_ gest:UITapGestureRecognizer)
    {
        
        
        if sessionList.data[(gest.view?.tag)!].session.count > 0
        {
            arropenArrow[(gest.view?.tag)!] = 0
            var arrindepath:[IndexPath]=[]
            for i in 0..<sessionList.data[(gest.view?.tag)!].session.count
            {
                let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
                arrindepath.append(indexpath)
            }
            
            sessionList.data[(gest.view?.tag)!].session = []
            tblSessionList.deleteRows(at: arrindepath, with: .top)
            tblSessionList.reloadData()
            
            
            
        }
        else{
            arropenArrow[(gest.view?.tag)!] = 1
            var arrindepath:[IndexPath]=[]
           sessionList.data[(gest.view?.tag)!].session = sessionListTemp[(gest.view?.tag)!]
            for i in 0..<sessionList.data[(gest.view?.tag)!].session.count
            {
                let indexpath:IndexPath = IndexPath(row: i, section: (gest.view?.tag)!)
                arrindepath.append(indexpath)
            }
            
            tblSessionList.insertRows(at: arrindepath, with: .top)
            tblSessionList.reloadData()
            
            
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
                let sessionDetailVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "SessionDetailViewController") as! SessionDetailViewController
        sessionDetailVC.strsessionID = sessionList.data[indexPath.section].session[indexPath.row].iD
                self.navigationController?.pushViewController(sessionDetailVC, animated: true)
    }
    
    func closealltab()
    {
        var indexpath:[IndexPath]=[]
        arropenArrow.removeAll()
        for j in 0..<sessionList.data.count
        {
            arropenArrow.append(1)
            
           
         
            
            
            
        }
       
        tblSessionList.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: -Searchbar Delegate Methods
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
        
    }
     func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
     {
        print(searchBar.text)
        print(searchText)
        if searchText != ""
        {
         for i in 0..<sessionList.data.count
         {
            sessionList.data[i].session = sessionListTemp[i]
            arropenArrow[i] = 1
        }
       
        for j in 0..<sessionList.data.count
        {
            sessionList.data[j].session =
                sessionList.data[j].session.filter { ($0.name).localizedCaseInsensitiveContains(searchText)  }
    
        }
        tblSessionList.reloadData()
        }
        else {
            for i in 0..<sessionList.data.count
            {
                arropenArrow[i] = 1
                sessionList.data[i].session = sessionListTemp[i]
                 tblSessionList.reloadData()
            }
        }
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
       print(searchBar.text)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        for i in 0..<sessionList.data.count
        {
            arropenArrow[i] = 1
            sessionList.data[i].session = sessionListTemp[i]
            tblSessionList.reloadData()
        }
    }
    
    func datecompare(strdate1:String,strdate2:String) -> Bool
    {
        let dateformate = DateFormatter()
        dateformate.dateFormat = "yyyy-MM-dd HH:mm"
        let date1:Date = dateformate.date(from: strdate1) as! Date
        let date2:Date = dateformate.date(from: strdate2) as! Date
       
        print(date1)
        print(date2)
        if Date() > date1 && Date() < date2
        {
          return true
        }
        else{
            return false
        }
        
    }
    
    
}

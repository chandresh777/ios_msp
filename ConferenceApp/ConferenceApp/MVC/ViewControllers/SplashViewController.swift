//
//  SplashViewController.swift
//  ConferenceApp
//
//  Created by msp on 12/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var imgSplash: UIImageView!
    
    var resultOfAppVersion : AppVersionRootClass? = nil
    var customPopUPView:CustomAlertPopView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: -Custom Access Methods
    func setLayout() {
        imgSplash.contentMode = .scaleAspectFill
        imgSplash.clipsToBounds = true
        imgSplash.image = #imageLiteral(resourceName: "imgSplash")
        if getUserProfileDataList() != nil
        {
            if getUserProfileDataList()?.isProfileFilled == "0" {
                
                Utility.moveToProfileController()
            }else {
                
                Utility.moveToHomeViewCotroller()
                
            }
        }
        else{
            Utility.moveToLoginViewController()
        }

//        callAppVersionAPI()
    }
    
    //MARK: -API Call
    func callAppVersionAPI() {
        LoginFlowModel.sharedModel().callAppVersionAPI(device: "2", { (response) in
            if response.code == 1{
                self.resultOfAppVersion = response
                
//                let dblVersion : Double = Double((self.resultOfAppVersion?.data.appConfig.version)!)!
                
//                if Double(APP_INFO.APP_VERSION)! > dblVersion{
//                    self.customPopUPView = Bundle.main.loadNibNamed("CustomAlertPopView", owner: self, options: nil)![0] as! CustomAlertPopView
//                    self.customPopUPView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.size.height)
//                    AppDelegate.sharedInstance().window?.rootViewController?.view.addSubview(self.customPopUPView)
//                    self.customPopUPView.isHidden = false
//                    self.customPopUPView.lblAlertMessage.isHidden = false
//                    self.customPopUPView.txtEnterCode.isHidden = true
//                    //        customPopUPView.lblAlertMessage.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 17.0))
//                    self.customPopUPView.lblAlertTitle.text = "Update"
//                    self.customPopUPView.lblAlertMessage.text = "Current version of the this app is old. Download latest version from App Store"
//                    self.customPopUPView.btnOkey.setTitle("Update", for: .normal)
//                    self.customPopUPView.btnCancel.setTitle("Cancel", for: .normal)
//                    self.customPopUPView.btnCancel.addTarget(self, action: #selector(self.btnClickCancel(_:)), for: .touchUpInside)
//                    self.customPopUPView.btnOkey.addTarget(self, action: #selector(self.btnClickUpdate(_:)), for: .touchUpInside)
//                }
//                else{
                    if getUserProfileDataList() != nil
                    {
                        if getUserProfileDataList()?.isProfileFilled == "0" {
                            
                            Utility.moveToProfileController()
                        }else {
                            
                            Utility.moveToHomeViewCotroller()
                            
                        }
                    }
                    else{
                        Utility.moveToLoginViewController()
                    }
                    
                }
//            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.callAppVersionAPI()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    //MARK: - POPUP Action
    @objc func btnClickCancel(_ sender:UIButton)
    {
        customPopUPView.isHidden = true
        exit(0)
    }
    @objc func btnClickUpdate(_ sender:UIButton)
    {
        customPopUPView.isHidden = true
        
//        let url = URL(string: "https://itunes.apple.com/us/app/desk-write-more/id%@?at=1000lLve")

        let url = URL(string: "http://itunes.apple.com/us/app/id1024941703")
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url!)
        }
    }
    

}

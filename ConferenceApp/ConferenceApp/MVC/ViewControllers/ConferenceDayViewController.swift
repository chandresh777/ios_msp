//
//  ConferenceDayViewController.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class ConferenceDayCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewSep: UIView!

}

class ConferenceDayViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var imgvAds: UIImageView!
    @IBOutlet weak var tblConferenceDayList: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var conferenceDayList:ConferenceDateListRoot!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
        
        self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")
 tblConferenceDayList.tableFooterView = UIView()
        setLayoutMethod()
        callConferenceDate()
         imgvAds.image = UIImage.animatedImage(withAnimatedGIFURL: URL(string: "http://202.131.107.107/vr/CMA/assets/uploads/sponsor/advertisement_footer.gif")!)
    }
    @objc func callConferenceDate()
    {
        ConferenceDateSessionModel.sharedModel().getConferenceList(conference_id: AppDelegate.sharedInstance().conference.data[0].iD, { (response) in
            if response.code == 1{
               self.conferenceDayList = response
               
                self.tblConferenceDayList.dataSource = self
                self.tblConferenceDayList.delegate = self
                self.tblConferenceDayList.reloadData()
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.callConferenceDate()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "Conference Day"
        
//        searchBar.translatesAutoresizingMaskIntoConstraints = true
//        searchBar.frame = CGRect(x: searchBar.frame.origin.x, y: self.headerView.frame.origin.y + self.headerView.frame.size.height + 5, width: screenWidth, height: searchBar.frame.size.height)

        tblConferenceDayList.backgroundColor = .clear
        //tblConferenceDayList.separatorStyle = .none
        
      
        
//        searchBar.barTintColor = colorset.navigationBar
//        searchBar.backgroundColor = colorset.navigationBar
//        searchBar.backgroundImage = UIImage()
//        searchBar.showsCancelButton = true
        
//        for view in (searchBar.subviews.last?.subviews)! {
//            if view is UITextField{
//                (view as! UITextField).font = UIFont(name: FONT.Regular, size: 16.0)
//                (view as! UITextField).textColor = colorset.lightBlackTextColor
//            }
//            else if view is UIButton{
//                (view as! UIButton).titleLabel?.font = UIFont(name: FONT.Regular, size: 14.0)
//                (view as! UIButton).setTitleColor(colorset.lightBlackTextColor, for: .normal)
//            }
//
//        }
        
//        searchBar.showsCancelButton = false
//
//        searchBar.placeholder = "Search"
//        searchBar.delegate = self
    }

    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        slideMenuController()?.toggleLeft()
    }

    //MARK: -Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conferenceDayList.data.date.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let objConferenceDayCell = tableView.dequeueReusableCell(withIdentifier: "ConferenceDayCell") as! ConferenceDayCell
        
        objConferenceDayCell.lblTitle.text = stringdateFormate(strdate: conferenceDayList.data.date[indexPath.row])
        
        objConferenceDayCell.lblTitle.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 18.0))
        objConferenceDayCell.viewSep.backgroundColor = colorset.sepretorColor
        
        objConferenceDayCell.selectionStyle = .none
        return objConferenceDayCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objSessionListVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "SessionListVC") as! SessionListVC
        objSessionListVC.strDate = conferenceDayList.data.date[indexPath.row]
        self.navigationController?.pushViewController(objSessionListVC, animated: true)
    }
    
    func stringdateFormate(strdate:String)->String
    {
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: strdate)!
        dateFormatter.dateFormat = "dd MMM yyyy"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    //MARK: -Searchbar Delegate Methods
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }

}

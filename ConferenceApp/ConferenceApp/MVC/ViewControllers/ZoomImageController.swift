//
//  ZoomImageController.swift
//  HonestGreens
//
//  Created by ABBACUS TECH on 08/06/17.
//  Copyright © 2017 Pritesh Patel. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
class ZoomImageController: BaseViewController , UIScrollViewDelegate {
    
    
    @IBOutlet weak var imgScrollView : UIScrollView!
    @IBOutlet weak var imgView : UIImageView!
    
    var imgString : String = ""
    var img:UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.lblHeader.isHidden = true
       
        imgScrollView.backgroundColor = UIColor.clear
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        imgScrollView.minimumZoomScale = 1.0
        imgScrollView.maximumZoomScale = 3.0
       self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
        
        if imgString != ""
        {
            let urlString = URL(string:imgString)
            
           
            
            // Set Zoom Image
            if urlString != nil{
                
                imgView.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "imgLoginLogo"))
//                imgView.af_setImage(withURLRequest: URLRequest(url: urlString!), placeholderImage: nil, filter: nil, progress: { (progress) in
//
//                }, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false, completion: { (image) in
//
//                })
            }
        }
        else{
            imgView.image = img
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgView
    }
    override func btnLeftBtnTapped() {
         self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnBack(sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

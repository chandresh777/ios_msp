//
//  SelfieViewController.swift
//  ConferenceApp
//
//  Created by msp on 12/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
import AVFoundation

class SelfiFrameListColCell: UICollectionViewCell {
    @IBOutlet weak var imgFrame: UIImageView!
}

class SelfieViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var imgv: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnFrameList: UIButton!
    @IBOutlet weak var imgFrame: UIImageView!
    @IBOutlet weak var btncapture: UIButton!
    
    @IBOutlet weak var viewCam: UIView!

    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var btnCameraMode: UIButton!
    
    @IBOutlet weak var colFrameListBottom: NSLayoutConstraint!
    @IBOutlet weak var colFrameList: UICollectionView!
    let session = AVCaptureSession()
    let photoOutput = AVCapturePhotoOutput()
    
    var videoDeviceInput: AVCaptureDeviceInput!
    var setupResult: SessionSetupResult = .success
    var previewLayer : AVCaptureVideoPreviewLayer!

    var strcam = "back"


    var isFlashOn = false
    var resultGetFrameList : SelfieFrameListRootClass? = nil

    enum SessionSetupResult {
        case success
        case notAuthorized
        case configurationFailed
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLayout()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let currentCameraInput: AVCaptureInput = session.inputs.first else {
            return
        }
        
        session.removeInput(currentCameraInput)
        configsession()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: -Custom Access Methods
    func setLayout()  {
        
        viewCam.translatesAutoresizingMaskIntoConstraints = true
        viewCam.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        
        self.colFrameListBottom.constant = 80

//        self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")
//        self.headerView.lblHeader.text = "Selfie"

//        colFrameList.dataSource = self
//        colFrameList.delegate = self
        
        imgFrame.contentMode = .scaleToFill
        imgFrame.clipsToBounds = true
//        imgFrame.image = #imageLiteral(resourceName: "imgSelfieFrame")
        
        btnFlash.addTarget(self, action: #selector(btnFlashOnOrOff), for: .touchUpInside)
        
        btnBack.addTarget(self, action: #selector(btnLeftBtnTapped), for: .touchUpInside)
        
        
        let imgBackBtn = UIImage(named: "imgBack")?.withRenderingMode(.alwaysTemplate)
        btnBack.setImage(imgBackBtn, for: .normal)
        btnBack.imageView?.tintColor = UIColor.white
        
        btnFrameList.setTitle("FrameList", for: .normal)
        btnFrameList.titleLabel?.font = UIFont(name: FONT.Bold, size: 20.0)
        btnFrameList.setTitleColor(UIColor.white, for: .normal)
        btnFrameList.addTarget(self, action: #selector(btnFrameListTapped), for: .touchUpInside)
        
        configsession()
        
        getSelfiFrameList()
    }
    
    //MARK: -Button Clicked Events
    @objc func btnLeftBtnTapped() {
        self.navigationController?.popViewController(animated: true)
    }

    
    @objc func btnFrameListTapped(){
        if self.colFrameListBottom.constant == 80{
            UIView.animate(withDuration: 4.0) {
                self.colFrameListBottom.constant = 0
            }
        }
        else{
            UIView.animate(withDuration: 4.0) {
                self.colFrameListBottom.constant = 80
            }
        }
    }
    
    @IBAction func btnclickCapture(_ sender: Any) {
        
        let photoSettings = AVCapturePhotoSettings()
        if self.videoDeviceInput.device.isFlashAvailable {
            if isFlashOn{
                photoSettings.flashMode = .on
            }
            else{
                photoSettings.flashMode = .off
            }
        }
        photoSettings.isHighResolutionPhotoEnabled = true
//        if self.videoDeviceInput.device.isFlashAvailable {
//            photoSettings.flashMode = .auto
//        }
        
        if let firstAvailablePreviewPhotoPixelFormatTypes = photoSettings.availablePreviewPhotoPixelFormatTypes.first {
            photoSettings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String: firstAvailablePreviewPhotoPixelFormatTypes]
        }
        
        photoOutput.capturePhoto(with: photoSettings, delegate: self)
    }
    
    func configsession()
    {
        
        session.beginConfiguration()
        session.sessionPreset = AVCaptureSession.Preset.photo
        // Add video input.
        do {
            var defaultVideoDevice: AVCaptureDevice?
            
            // Choose the back dual camera if available, otherwise default to a wide angle camera.
            let dualCameraDeviceType: AVCaptureDevice.DeviceType
            if #available(iOS 11, *) {
                dualCameraDeviceType = .builtInDualCamera
            } else {
                dualCameraDeviceType = .builtInDuoCamera
            }
            if strcam == "back"
            {
                if let dualCameraDevice = AVCaptureDevice.default(dualCameraDeviceType, for: AVMediaType.video, position: .back) {
                    defaultVideoDevice = dualCameraDevice
                } else if let backCameraDevice = AVCaptureDevice.default(AVCaptureDevice.DeviceType.builtInWideAngleCamera, for: AVMediaType.video, position: .back) {
                    // If the back dual camera is not available, default to the back wide angle camera.
                    defaultVideoDevice = backCameraDevice
                }
            }
            else{
                if let frontCameraDevice = AVCaptureDevice.default(AVCaptureDevice.DeviceType.builtInWideAngleCamera, for: AVMediaType.video, position: .front) {
                    /*
                     In some cases where users break their phones, the back wide angle camera is not available.
                     In this case, we should default to the front wide angle camera.
                     */
                    defaultVideoDevice = frontCameraDevice
                }
                
            }
            
            
            let videoDeviceInput = try AVCaptureDeviceInput(device: defaultVideoDevice!)
            
            if session.canAddInput(videoDeviceInput) {
                session.addInput(videoDeviceInput)
                self.videoDeviceInput = videoDeviceInput
                
                
                
                
                
                
            } else {
                print("Could not add video device input to the session")
                setupResult = .configurationFailed
                session.commitConfiguration()
                return
            }
        } catch {
            print("Could not create video device input: \(error)")
            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        
        // Add photo output.
        if session.canAddOutput(photoOutput) {
            session.addOutput(photoOutput)
            self.previewLayer = AVCaptureVideoPreviewLayer(session: self.session)
            self.previewLayer.frame = self.viewCam.bounds
            self.previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            self.viewCam.layer.addSublayer(self.previewLayer)
            
            photoOutput.isHighResolutionCaptureEnabled = true
            photoOutput.isLivePhotoCaptureEnabled = photoOutput.isLivePhotoCaptureSupported
            
        } else {
            print("Could not add photo output to the session")
            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        
        session.commitConfiguration()
        self.session.startRunning()
    }

    
    @IBAction func btnclickSelfieMode(_ sender: Any) {
        guard let currentCameraInput: AVCaptureInput = session.inputs.first else {
            return
        }
        
        session.removeInput(currentCameraInput)
        
        if strcam == "back"
        {
            strcam = "front"
            configsession()
            
        }else{
            strcam = "back"
            configsession()
        }
    }
    
    @objc func btnFlashOnOrOff()  {
        if isFlashOn{
            isFlashOn = false
            btnFlash.setImage(#imageLiteral(resourceName: "imgFlashOff"), for: .normal)
        }
        else{
            btnFlash.setImage(#imageLiteral(resourceName: "imgFlashOn"), for: .normal)
            isFlashOn = true
        }
    }
    
    //MARK: -API Call
    func getSelfiFrameList() {
        SelfieModel.sharedModel().getSelfiFrameList(conference_id: "1", { (response) in
            
            if response.code == 1{
                self.resultGetFrameList = response
                if (self.resultGetFrameList?.data.count)! > 0{
                    self.imgFrame.sd_setImage(with: URL(string: (self.resultGetFrameList?.data[0].frameImage)! ), placeholderImage: UIImage(named: ""))
                }
                self.colFrameList.dataSource = self
                self.colFrameList.delegate = self
                self.colFrameList.reloadData()
            }
            
        }) { (sender, request, json, error) in
            
        }
    }
    
    
    //MARK: -Collection View Delegate Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.resultGetFrameList?.data.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let objSelfiFrameListColCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelfiFrameListColCell", for: indexPath) as! SelfiFrameListColCell
        
        objSelfiFrameListColCell.imgFrame.contentMode = .scaleAspectFill
        objSelfiFrameListColCell.imgFrame.clipsToBounds = true
        objSelfiFrameListColCell.imgFrame.backgroundColor = UIColor.clear
        
        objSelfiFrameListColCell.imgFrame.sd_setImage(with: URL(string: (self.resultGetFrameList?.data[indexPath.row].thumbImg)! ), placeholderImage: UIImage(named: "imgLoginLogo"))

        return objSelfiFrameListColCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: colFrameList.frame.size.height, height: colFrameList.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 10, 0, 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        imgFrame.sd_setImage(with: URL(string: (self.resultGetFrameList?.data[indexPath.row].frameImage)! ), placeholderImage: UIImage(named: ""))
    }
}

extension SelfieViewController: AVCapturePhotoCaptureDelegate {
    
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if let error = error {
            print("Error capturing photo: \(error)")
        } else {
            if let sampleBuffer = photoSampleBuffer, let previewBuffer = previewPhotoSampleBuffer, let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
                
                if let image = UIImage(data: dataImage) {
                    imgv.image = image
                }
            }
        }
        
    }
    
    @available(iOS 11.0, *)
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let data = photo.fileDataRepresentation(),
            let image =  UIImage(data: data)  else {
                return
        }
        
        var bottomImage = UIImage(named: "bottom.png")
        var topImage = UIImage(named: "imgSelfieFrame")
        print(topImage?.size.width,topImage?.size.height)
        //imgv.image = image
        var size = CGSize(width: viewCam.frame.size.width, height: viewCam.frame.size.height)
        UIGraphicsBeginImageContext(size)
        
        let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        image.draw(in: areaSize)
        
        
        topImage!.draw(in: areaSize, blendMode: .normal, alpha: 1.0)
        
        var newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        
        // imgv.image = newImage
        let third_view = self.storyboard?.instantiateViewController(withIdentifier: "MySelfieViewController") as! MySelfieViewController
        third_view.imgSelfie = newImage
        third_view.isFromMenu = false
        self.navigationController?.pushViewController(third_view, animated: true)
        
        
    }
    
    
}


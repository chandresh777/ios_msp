//
//  AttendeesViewController.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class AttendeesViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var imgvAds: UIImageView!
    @IBOutlet weak var tblAttendies: UITableView!
    var attendesList:AttendeesRoot!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")
         imgvAds.image = UIImage.animatedImage(withAnimatedGIFURL: URL(string: Gifurlads)!)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.imgvAds.addGestureRecognizer(tap)
        self.imgvAds.isUserInteractionEnabled = true
        setLayoutMethod()
        getAttendeesList()
    }
    //MARK: - TapGesture
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
//        let objCommanWebViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as! CommanWebViewController
//        objCommanWebViewController.strNavTitle = "Ads"
//        objCommanWebViewController.strUrl = GifurladsRedirect
//        self.navigationController?.pushViewController(objCommanWebViewController, animated: true)
        if let url = URL(string: GifurladsRedirect) {
            UIApplication.shared.open(url, options: [:]) }
        
    }

    
    func getAttendeesList()
    {
        
        AttendeesModel.sharedModel().getattendes(conference_id: AppDelegate.sharedInstance().conference.data[0].iD, { (response) in
            if response.code == 1{
                self.attendesList = response
               
                self.tblAttendies.dataSource = self
                self.tblAttendies.delegate = self
                self.tblAttendies.reloadData()
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.getAttendeesList()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return FontSizeFunc(size: 80)
    //    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return attendesList.data.count
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AttendiesListCell
        
        cell.btnMoreOpsn.addTarget(self, action:#selector(btnMoreOptionClicked), for: .touchUpInside)
        cell.lblTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 16.0))
        cell.lblCategory.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        cell.lblTitle.textColor = colorset.blacktextcolor
        cell.lblCategory.textColor = colorset.blacktextcolor
        
        
        cell.lblTitle.text = String(format: "%@ %@",attendesList.data[indexPath.row].firstName,attendesList.data[indexPath.row].lastName )
        cell.lblCategory.text = attendesList.data[indexPath.row].catName
        
        cell.selectionStyle = .none
        return cell
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    //MARK:- Button Action Method
    @objc func btnMoreOptionClicked(sender: UIButton) {
        
    }
    
    override func btnLeftBtnTapped() {
        slideMenuController()?.toggleLeft()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "Attendees"
    }
    
    //MARK: -Button Clicked Events
    
    
}

//
//  EntertainmentViewController.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class EntertainmentViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var tblEntertainment : UITableView!
    var objEntertainmentList : EntertainmentRootClass? = nil
     @IBOutlet weak var imgvAds: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgvAds.image = UIImage.animatedImage(withAnimatedGIFURL: URL(string: Gifurlads)!)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.imgvAds.addGestureRecognizer(tap)
        self.imgvAds.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
        self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")
        setLayoutMethod()
    }

    //MARK: - TapGesture
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
//        let objCommanWebViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as! CommanWebViewController
//        objCommanWebViewController.strNavTitle = "Ads"
//        objCommanWebViewController.strUrl = GifurladsRedirect
//        self.navigationController?.pushViewController(objCommanWebViewController, animated: true)
        if let url = URL(string: GifurladsRedirect) {
            UIApplication.shared.open(url, options: [:]) }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }

    //MARK: -Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (objEntertainmentList?.data.count)!
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AbstactListCell", for: indexPath) as! AbstactListCell
        
        cell.lblTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 16.0))
        cell.lblDate.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
        cell.lblTitle.textColor = colorset.blacktextcolor
        cell.lblDate.textColor = colorset.blacktextcolor
        
        cell.lblTitle.text = objEntertainmentList?.data[indexPath.row].title
        
        
        
        cell.lblDate.text = "\(stringdateFormate(strdate: (objEntertainmentList?.data[indexPath.row].date)!)) at \(stringTimeFormate(strdate: (objEntertainmentList?.data[indexPath.row].time)!))"
        
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let abstactDetailVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "EntertainmentDetailViewController") as! EntertainmentDetailViewController
        abstactDetailVC.selectedIndex = indexPath.row
        abstactDetailVC.objEntertainmentDetailData = objEntertainmentList
        self.navigationController?.pushViewController(abstactDetailVC, animated: true)
    }
    
    
    func stringdateFormate(strdate:String)->String
    {
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: strdate)!
        dateFormatter.dateFormat = "dd MMM yyyy"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    func stringTimeFormate(strdate:String)->String
    {
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "HH:mm"
        let date = dateFormatter.date(from: strdate)
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date!)
        return dateString
    }
    
    //MARK: - API Call
    
    func getEntertainmentList() {
        EntertainmentModel().getEntertainmentList(conference_id: AppDelegate.sharedInstance().conference.data[0].iD,{ (response) in
            if response.code == 1{
                self.objEntertainmentList = response
                self.tblEntertainment.dataSource = self
                self.tblEntertainment.delegate = self
                self.tblEntertainment.reloadData()
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.getEntertainmentList()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    

    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "Entertainment"
        getEntertainmentList()
    }

    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        slideMenuController()?.toggleLeft()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
//  BaseViewController.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    var headerView:HeaderView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setBasicLayout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: -Custom Access Methods
    func setBasicLayout() {
        headerView = Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)![0] as! HeaderView
        if UIScreen.main.bounds.size.height >= 812.0
        {
            headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 90)
            headerView.viewStatusBarHeight.constant = 40
        }
        else
        {
            headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 70)
            headerView.viewStatusBarHeight.constant = 20
        }
        
        headerView.backgroundColor = .clear
        
        headerView.viewStatusBar.backgroundColor = colorset.statusBarColor
        
        headerView.viewNavigation.backgroundColor = colorset.navigationBar
        
        
        headerView.lblHeader.textColor = colorset.blacktextcolor
        
        headerView.lblHeader.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 20.0))
        
        headerView.btnLeftBtn.addTarget(self, action: #selector(btnLeftBtnTapped), for: .touchUpInside)
        headerView.btnRightBtn.addTarget(self, action: #selector(btnRightBtnTapped), for: .touchUpInside)
        headerView.btnRightBtn.isHidden = true
        headerView.btnLeftBtn.isHidden = true
        
        self.view.addSubview(headerView)
    }
    
    func methodToSetNavigationBtn(strLeftImg:String, strRightImg:String) {
        let imgLeft = UIImage(named:strLeftImg) //?.withRenderingMode(.alwaysTemplate)
        headerView.btnLeftBtn.setImage(imgLeft, for: .normal)
        //        headerView.btnLeftBtn.tintColor = colorset.imgTintColor
        
        headerView.btnLeftBtn.isHidden = false
        
        if strRightImg != ""{
            headerView.btnRightBtn.isHidden = false
            let imgRight = UIImage(named:strRightImg)  //?.withRenderingMode(.alwaysTemplate)
            headerView.btnRightBtn.setImage(imgRight, for: .normal)
            //            headerView.btnRightBtn.tintColor = colorset.imgTintColor
        }
        else{
            headerView.btnRightBtn.isHidden = true
        }
    }
    
    @objc func btnLeftBtnTapped() {
        
    }
    @objc func btnRightBtnTapped() {
        
    }
}

//
//  HeaderView.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class HeaderView: UIView {

    @IBOutlet weak var btnLeftBtn: UIButton!
    @IBOutlet weak var btnRightBtn: UIButton!

    @IBOutlet weak var viewStatusBarHeight: NSLayoutConstraint!
    @IBOutlet weak var viewStatusBar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var viewNavigation: UIView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

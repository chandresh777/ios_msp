//
//  PresenterListVC.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
class PresenterListCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewSep: UIView!
}

class PresenterListVC: BaseViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblPresenterList: UITableView!
    @IBOutlet weak var imgvAds: UIImageView!
    var speakerList:SpeakerListRoot!
    var sperkertemp:[SpeakerListData] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")
getSpeakerList()
        setLayoutMethod()
         imgvAds.image = UIImage.animatedImage(withAnimatedGIFURL: URL(string: Gifurlads)!)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.imgvAds.addGestureRecognizer(tap)
        self.imgvAds.isUserInteractionEnabled = true
    }
    
    //MARK: - TapGesture
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
//        let objCommanWebViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as! CommanWebViewController
//        objCommanWebViewController.strNavTitle = "Ads"
//        objCommanWebViewController.strUrl = GifurladsRedirect
//        self.navigationController?.pushViewController(objCommanWebViewController, animated: true)
        if let url = URL(string: GifurladsRedirect) {
            UIApplication.shared.open(url, options: [:]) }
        
    }
    
func getSpeakerList()
{
    SpeakerModel.sharedModel().getspeakerList(conference_id: AppDelegate.sharedInstance().conference.data[0].iD, { (response) in
        if response.code == 1{
            self.speakerList = response
            self.sperkertemp = self.speakerList.data
            self.tblPresenterList.dataSource = self
            self.tblPresenterList.delegate = self
            self.tblPresenterList.reloadData()
        }
        else{
            self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                
                }, nil])
        }
        
    }) { (request, sender, json, error) in
        Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
            
            switch selectedIndex {
            case CONSTANTs.TWO: //for retry
                
                //Handle action
                self.getSpeakerList()
                
                break
                
            default:
                
                // ignore case
                
                break
            }
            
        })
    }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "Speakers"
        self.searchBar.tintColor = UIColor.black
        searchBar.translatesAutoresizingMaskIntoConstraints = true
        searchBar.frame = CGRect(x: searchBar.frame.origin.x, y: self.headerView.frame.origin.y + self.headerView.frame.size.height + 5, width: screenWidth, height: searchBar.frame.size.height)
        tblPresenterList.backgroundColor = .clear
        tblPresenterList.separatorStyle = .none
       

        searchBar.barTintColor = colorset.navigationBar
        searchBar.backgroundColor = colorset.navigationBar
        searchBar.backgroundImage = UIImage()
        searchBar.showsCancelButton = true

        
        for view in (searchBar.subviews.last?.subviews)! {
            if view is UITextField{
                (view as! UITextField).font = UIFont(name: FONT.Regular, size: 16.0)
                (view as! UITextField).textColor = colorset.lightBlackTextColor
            }
            else if view is UIButton{
                (view as! UIButton).titleLabel?.font = UIFont(name: FONT.Regular, size: 14.0)
                (view as! UIButton).setTitleColor(colorset.lightBlackTextColor, for: .normal)
            }
            
        }

        searchBar.showsCancelButton = false

        searchBar.placeholder = "Search"
        searchBar.delegate = self
        
    }
    
    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        slideMenuController()?.toggleLeft()
    }

    //MARK: -Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return speakerList.data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let objPresenterListCell = tableView.dequeueReusableCell(withIdentifier: "PresenterListCell") as! PresenterListCell
        

        objPresenterListCell.lblTitle.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 18.0))
        objPresenterListCell.lblTitle.text = "Fraser Davidson"
        
        objPresenterListCell.viewSep.backgroundColor = colorset.sepretorColor
        
        objPresenterListCell.lblTitle.text = String(format: "%@ %@", speakerList.data[indexPath.row].firstName,speakerList.data[indexPath.row].lastName)
        
        
        objPresenterListCell.selectionStyle = .none
        
        return objPresenterListCell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objSpeakerDetailVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "SpeakerDetailViewController") as! SpeakerDetailViewController
        objSpeakerDetailVC.struserId = speakerList.data[indexPath.row].iD
        self.navigationController?.pushViewController(objSpeakerDetailVC, animated: true)
        
//        let objSpeakerDetailVC = StoryBoard.main.instantiateViewController(withIdentifier: "OrganizationDetailViewController") as! OrganizationDetailViewController
//        self.navigationController?.pushViewController(objSpeakerDetailVC, animated: true)

    }
    
    //MARK: -Searchbar Delegate Methods
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        print(searchBar.text)
        print(searchText)
        if searchText != ""
        {
            
            speakerList.data = sperkertemp
          
                speakerList.data =
                    speakerList.data.filter { ($0.firstName).localizedCaseInsensitiveContains(searchText)  }
                
            
            tblPresenterList.reloadData()
        }
        else {
            speakerList.data = sperkertemp
                tblPresenterList.reloadData()
            }
        
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true

    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {

    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        speakerList.data = sperkertemp
        tblPresenterList.reloadData()
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    

}

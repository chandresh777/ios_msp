//
//  LoginVC.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
import MaterialTextField

class LoginViewController: BaseViewController,UITextFieldDelegate {
   
    @IBOutlet weak var btncheckbox: UIButton!
    @IBOutlet weak var btnTermCondition: UIButton!
    
    @IBOutlet weak var txtNumber: MFTextField!
    @IBOutlet weak var btnLogin: btnThemClass!
    @IBOutlet weak var lblTitle: UILabel!
    var resultOfSendOtp : SendOtpData? = nil
    
    var resultOfLoginResponse : LoginRootClass? = nil

    @IBOutlet weak var btnSkip: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtNumber.delegate = self
        headerView.lblHeader.text = "Login"
        setlayout()
        addToolBar(textField: txtNumber)
        
        self.btnTermCondition .addTarget(self, action: #selector(opentermCondition), for: .touchUpInside)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func opentermCondition(){
        let objCommanWebViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as! CommanWebViewController
        objCommanWebViewController.strNavTitle = "Terms And Condition"
        objCommanWebViewController.strUrl = "http://www.rssdi2018ahmedabad.com/privacy-policy/"
        self.navigationController?.pushViewController(objCommanWebViewController, animated: true)
    }
    
    //MARK: -Custom Access Methods
    func setlayout()
    {
       
        self.txtNumber.tintColor = colorset.textFieldLine
        
        self.txtNumber.textColor = colorset.blacktextcolor
        
        self.txtNumber.defaultPlaceholderColor = colorset.textFieldLine
        
        self.txtNumber.placeholderAnimatesOnFocus = true;
        
      // UIFontDescriptor * fontDescriptor = [self.textField2.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold];
       // UIFont *font = [UIFont fontWithDescriptor:fontDescriptor size:self.textField2.font.pointSize];
        
        txtNumber.attributedPlaceholder = NSAttributedString(string: "PHONE NUMBER", attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: FontSizeFunc(size: 16))])
        
      //  self.textField2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Attributed placeholder" attributes:@{NSFontAttributeName:font}];
        
        
        lblTitle.font = UIFont.systemFont(ofSize: FontSizeFunc(size: 13.0))
        txtNumber.font = UIFont.systemFont(ofSize: FontSizeFunc(size: 22.0))
        btnTermCondition.titleLabel?.font = UIFont.systemFont(ofSize: FontSizeFunc(size: 16.0))
         btnSkip.titleLabel?.font = UIFont.systemFont(ofSize: FontSizeFunc(size: 16.0))
    btnSkip.titleLabel?.textColor = colorset.buttonBGcolor
    }
    
    @IBAction func btnclickLogin(_ sender: Any) {
        self.view.endEditing(true)
        if txtNumber.text == ""
        {
            self.popupAlert(title: AppName.appName, message: ValidationMsg.addAddressPhoneNum, actionTitles: ["Ok"], actions:[{action1 in

                }, nil])
        }
            
        if btncheckbox.isSelected == false{
            self.popupAlert(title: AppName.appName, message: ValidationMsg.selecttermsandconditions, actionTitles: ["Ok"], actions:[{action1 in
                
                }, nil])
        }
        else{
            if  self.txtNumber.text! == "9558411351"{
                self.callVerifyOtp()
            }
            else{
                sendOtpApi()
            }
//            callLoginAPI()
        }
        
//        let objNumberVerifyVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "NumberVerifyViewController") as! NumberVerifyViewController
        //self.navigationController?.pushViewController(objNumberVerifyVC, animated: true)

    }
    
    
    func Login()
    {
        print("login")
    }
    
    //MARK: - Textfield delegate method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 10
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnclickCheckBox(_ sender: Any) {
        btncheckbox.isSelected = !btncheckbox.isSelected
    }
    
    //MARK: -API Call
//    func callLoginAPI() {
//        LoginFlowModel.sharedModel().callLoginAPI(mobile_no: txtNumber.text!, { (response) in
//            if response.code == 1{
//                self.resultOfLoginResponse = response
//                if self.resultOfLoginResponse?.data.isProfileFilled == "1"
//                {
//                Utility.moveToHomeViewCotroller()
//                }
//                else{
//                    let objNumberVerifyVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
//                    objNumberVerifyVC.strpresent = "no"
//                    AppDelegate.sharedInstance().appfistTimeLogin = "yes"
//
//                    self.navigationController?.pushViewController(objNumberVerifyVC, animated: true)
//                }
//
//            }
//
//        }) { (request, sender, json, error) in
//            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
//
//                switch selectedIndex {
//                case CONSTANTs.TWO: //for retry
//
//                    //Handle action
//                    self.callLoginAPI()
//
//                    break
//
//                default:
//
//                    // ignore case
//
//                    break
//                }
//
//            })
//        }
//    }
    
    
    //MARK: -API Call
    func sendOtpApi() {
        LoginFlowModel.sharedModel().callSendOtpApi(mobile_no: txtNumber.text!, { (response) in
            if response.code == 1{
                self.resultOfSendOtp = response
                let objNumberVerifyVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "NumberVerifyViewController") as! NumberVerifyViewController
                objNumberVerifyVC.phoneNo = self.txtNumber.text!
                self.navigationController?.pushViewController(objNumberVerifyVC, animated: true)
                
            }else {
                
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
                
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.sendOtpApi()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    
    func callVerifyOtp() {
        LoginFlowModel.sharedModel().verifyCode(mobile_no: txtNumber.text!,otp_number: "12345", { (response) in
            if response.code == 1{
                self.resultOfLoginResponse = response
                if self.resultOfLoginResponse?.data.isProfileFilled == "1"
                {
                    Utility.moveToHomeViewCotroller()
                }
                else{
                    let objNumberVerifyVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                    objNumberVerifyVC.strpresent = "no"
                    AppDelegate.sharedInstance().appfistTimeLogin = "yes"
                    
                    self.navigationController?.pushViewController(objNumberVerifyVC, animated: true)
                }
                
            }else {
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
                
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.callVerifyOtp()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }

    @IBAction func btnclickSkip(_ sender: Any) {
        Utility.moveToHomeViewCotroller()
    }
    
}
extension UIViewController {
    func addToolBar(textField: UITextField){
        var toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        var doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
       // var cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "cancelPressed")
        var spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
       
        textField.inputAccessoryView = toolBar
    }
    @objc func donePressed(){
        view.endEditing(true)
    }
    func cancelPressed(){
        view.endEditing(true) // or do something
    }
    
    
}

//
//  DayAtGlanceViewController.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class DayAtGlanceViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tblDayOfGlance: UITableView!
    @IBOutlet weak var imgvAds: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")
        
        setLayoutMethod()
        tblDayOfGlance.dataSource = self
        tblDayOfGlance.delegate = self
         imgvAds.image = UIImage.animatedImage(withAnimatedGIFURL: URL(string: Gifurlads)!)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.imgvAds.addGestureRecognizer(tap)
        self.imgvAds.isUserInteractionEnabled = true
        
    }
    //MARK: - TapGesture
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
//        let objCommanWebViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as! CommanWebViewController
//        objCommanWebViewController.strNavTitle = "Ads"
//        objCommanWebViewController.strUrl = GifurladsRedirect
//        self.navigationController?.pushViewController(objCommanWebViewController, animated: true)
        if let url = URL(string: GifurladsRedirect) {
            UIApplication.shared.open(url, options: [:]) }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 20
    }
    
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! dayOfGlanceCell
        
        cell.lblTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 17.0))
        cell.lblDesc.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 15.0))
        cell.lblTitle.textColor = colorset.blacktextcolor
        cell.lblDesc.textColor = colorset.lightBlackTextColor
        
        cell.lblTitle.text = "Title 1"
        cell.lblDesc.text = "New subscribers only. Plan automatically renews after trial.Group FaceTime will be available in iOS 12 later this year through a software update.Some AR apps shown may not be available in all regions or all languages.FaceTime will be available in iOS 12 later this year through a software update.Some AR apps shown may not be available in all regions or all languages.FaceTime will be available in iOS 12 later this year through a software update.Some AR apps shown may not be available in all regions or all languages."
        
        return cell
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "Day At Glance"
    }
    
    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        slideMenuController()?.toggleLeft()
    }
    
}

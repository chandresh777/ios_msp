//
//  QuizViewController.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class QuizViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")

        setLayoutMethod()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    //MARK:- TableView dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 10
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! QuizTableViewCell
        cell.selectionStyle = .none
        cell.lblName.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 17.0))
        cell.lblLocation.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 15.0))
        cell.lblDate.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 15.0))
        cell.lblName.textColor = colorset.blacktextcolor
        cell.lblLocation.textColor = colorset.lightBlackTextColor
        cell.lblDate.textColor = colorset.lightBlackTextColor
        cell.lblName.text = "Quiz A"
        cell.lblLocation.text = "Wrexham"
        cell.lblDate.text = "03 NOV 2018"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "QuizDetailViewController") as! QuizDetailViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }

    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "Quiz"
    }
    
    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        slideMenuController()?.toggleLeft()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

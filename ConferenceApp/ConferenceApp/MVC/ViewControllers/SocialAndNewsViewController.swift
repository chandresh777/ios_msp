//
//  SocialAndNewsViewController.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class SocialAndNewsCell:UITableViewCell{
    @IBOutlet weak var viewTop:UIView!
    @IBOutlet weak var viewDetail:UIView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var imgNext:UIImageView!
}

class SocialAndNewsViewController: BaseViewController , UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblSocialMediaList: UITableView!
    
    let arrSocial = ["Twitter", "Facebook", "Instagram", "Youtube"]
    @IBOutlet weak var imgvAds: UIImageView!
    var objSocialMediaData : getSocialMediaRootClass? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.methodToSetNavigationBtn(strLeftImg: "imgMenu", strRightImg: "")
        
        setLayoutMethod()
        imgvAds.image = UIImage.animatedImage(withAnimatedGIFURL: URL(string: Gifurlads)!)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.imgvAds.addGestureRecognizer(tap)
        self.imgvAds.isUserInteractionEnabled = true
    }
    //MARK: - TapGesture
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
//        let objCommanWebViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as! CommanWebViewController
//        objCommanWebViewController.strNavTitle = "Ads"
//        objCommanWebViewController.strUrl = GifurladsRedirect
//        self.navigationController?.pushViewController(objCommanWebViewController, animated: true)
        if let url = URL(string: GifurladsRedirect) {
            UIApplication.shared.open(url, options: [:]) }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: -Custom Access Methods
    func setLayoutMethod()  {
        self.headerView.lblHeader.text = "We are here"
        
        tblSocialMediaList.translatesAutoresizingMaskIntoConstraints = true
        tblSocialMediaList.frame = CGRect(x: tblSocialMediaList.frame.origin.x, y: self.headerView.frame.origin.y + self.headerView.frame.size.height, width: screenWidth, height: screenHeight - self.headerView.frame.size.height)
        
        getSocialMediaList()
        tblSocialMediaList.backgroundColor = .clear
        tblSocialMediaList.separatorStyle = .none
    }
    
    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        slideMenuController()?.toggleLeft()
    }
    
    
    //MARK: - API Call
    
    func getSocialMediaList() {
        GetSocialMediaModel().getSocialMediaURL(conference_id: AppDelegate.sharedInstance().conference.data[0].iD, {(response) in
            if response.code == 1{
                self.objSocialMediaData = response
                self.tblSocialMediaList.dataSource = self
                self.tblSocialMediaList.delegate = self
                self.tblSocialMediaList.reloadData()
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.getSocialMediaList()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    
    //MARK: -Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return arrSocial.count
        return (self.objSocialMediaData?.data.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellAboutConfCell = tableView.dequeueReusableCell(withIdentifier: "SocialAndNewsCell") as! SocialAndNewsCell
        
        cellAboutConfCell.viewTop.backgroundColor = colorset.whitetextColor
        
        cellAboutConfCell.viewDetail.backgroundColor = colorset.navigationBar
        
        cellAboutConfCell.lblTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        cellAboutConfCell.lblTitle.textColor = colorset.blacktextcolor
        //        cellAboutConfCell.lblTitle.text = arrSocial[indexPath.row]
//        var str: String = (self.objSocialMediaData?.data[indexPath.row].fieldName)!
//        str.removeLast(4)
        var str : String = ""
        if (self.objSocialMediaData?.data[indexPath.row].fieldName .isEqual(string: "facebook_url"))!{
            str = "Facebook"
        }
        if (self.objSocialMediaData?.data[indexPath.row].fieldName .isEqual(string: "twitter_url"))!{
            str = "Twitter"
        }
        if (self.objSocialMediaData?.data[indexPath.row].fieldName .isEqual(string: "instagram_url"))!{
            str = "Instagram"
        }
        if (self.objSocialMediaData?.data[indexPath.row].fieldName .isEqual(string: "linkedin_url"))!{
            str = "Linkedin"
        }
        
        cellAboutConfCell.lblTitle.text = str
        
        
        cellAboutConfCell.imgNext.image =  UIImage(named: "imgrightarrow")
        cellAboutConfCell.imageView?.contentMode = .scaleAspectFit
        
        cellAboutConfCell.selectionStyle = .none
        return cellAboutConfCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let url = URL(string: (objSocialMediaData?.data[indexPath.row].fieldValue)!) {
            UIApplication.shared.open(url, options: [:]) }
//        let indexPathCurrent = tblSocialMediaList.indexPathForSelectedRow
//
//        let currentCell = tblSocialMediaList.cellForRow(at: indexPathCurrent!) as! SocialAndNewsCell
//
//
//        let objCommanWebViewController = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "CommanWebViewController") as! CommanWebViewController
//        objCommanWebViewController.isFromMenu = false
////        objCommanWebViewController.strNavTitle = (objSocialMediaData?.data[indexPath.row].fieldName)!
//        objCommanWebViewController.strNavTitle = currentCell.lblTitle.text!
//        objCommanWebViewController.strUrl = objSocialMediaData?.data[indexPath.row].fieldValue
//        self.navigationController?.pushViewController(objCommanWebViewController, animated: true)
    }
    
}

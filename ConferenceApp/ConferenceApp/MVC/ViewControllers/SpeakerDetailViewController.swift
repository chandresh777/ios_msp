//
//  SpeakerDetailViewController.swift
//  ConferenceApp
//
//  Created by msp on 26/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class SpeakerDetailCell:UITableViewCell{
    @IBOutlet weak var lblDetail:UILabel!
    @IBOutlet weak var imgDetail:UIImageView!
}

class SpeakerSessionListCell:UITableViewCell{
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var imgSessionLogo:UIImageView!
    
    @IBOutlet weak var viewTopSep: UIView!
    @IBOutlet weak var viewBottomSep: UIView!
    
}

class SpeakerDetailViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate,UITextViewDelegate {
    @IBOutlet weak var lblAddNotes: UILabel!
    
    @IBOutlet weak var imgvBookmark: UIImageView!
    
    @IBOutlet weak var ctlAddnotes: UIControl!
    @IBOutlet weak var ctlBookMark: UIControl!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var tblSessionList: UITableView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewSeprater: UIView!

   
    @IBOutlet weak var lblBookmark: UILabel!
    
   var customviewNotes:CustomViewAddNotes!
    var struserId:String!
    var spekerDeatil:SpeakerDetailRoot!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSeprater.backgroundColor = colorset.sepretorColor
        customviewNotes = Bundle.main.loadNibNamed("CustomViewAddNotes", owner: self, options: nil)![0] as! CustomViewAddNotes
        customviewNotes.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        self.view.addSubview(customviewNotes)
        customviewNotes.isHidden = true
        customviewNotes.btnCancel.addTarget(self, action: #selector(btnclickCancel(_:)), for: .touchUpInside)
        customviewNotes.btnOkey.addTarget(self, action: #selector(btnclickOkey(_:)), for: .touchUpInside)
        customviewNotes.txtView.delegate = self
        // Do any additional setup after loading the view.
        
        self.methodToSetNavigationBtn(strLeftImg: "imgBack", strRightImg: "")

        setLayout()
        getSpeakerDetail()
    }
    func getSpeakerDetail()
    {
        SpeakerModel.sharedModel().getspeakerDetail(conference_id:AppDelegate.sharedInstance().conference.data[0].iD,speaker_id:struserId , user_id: AppDelegate.sharedInstance().userId, { (response) in
            if response.code == 1{
                self.spekerDeatil = response
                self.setData()
                self.tblSessionList.dataSource = self
                self.tblSessionList.delegate = self
                self.tblSessionList.reloadData()
                
            }
            else{
                self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                    
                    }, nil])
            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.getSpeakerDetail()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: -Button Clicked Events
    override func btnLeftBtnTapped() {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK: -Custom Access Method
    func setLayout() {
        self.headerView.lblHeader.text = "Speaker Detail"
        
        lblTopTitle.translatesAutoresizingMaskIntoConstraints = true
        lblTopTitle.frame = CGRect(x: 0, y: self.headerView.frame.origin.y + self.headerView.frame.size.height, width: screenWidth, height: lblTopTitle.frame.size.height)
        
        lblTopTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 30.0))
        
        ctlBookMark.backgroundColor = colorset.drawerBackground
        ctlAddnotes.backgroundColor = colorset.drawerBackground
        
      
        
      
      
        
        lblBookmark.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 16.0))
        lblAddNotes.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 16.0))
        
        
        lblBookmark.textColor = colorset.whitetextColor
        lblAddNotes.textColor = colorset.whitetextColor
        
       
        tblSessionList.backgroundColor = .clear
        tblSessionList.separatorStyle = .none
    }
    
    func setData()
    {
        
        lblTopTitle.text = String(format: "%@ %@", spekerDeatil.data.firstName,spekerDeatil.data.lastName)
        lblTopTitle.backgroundColor = colorset.navigationBar
       
        if  spekerDeatil.data.speaker_liked == "0"
        {
            self.imgvBookmark.image =  UIImage(named: "imgBookMark")
        }
        else{
            self.imgvBookmark.image = UIImage(named: "bookmarked_icon")
        }
        if spekerDeatil.data.notes == ""
        {
            customviewNotes.txtView.text = "Add notes"
            customviewNotes.txtView.textColor = UIColor.lightGray
        }
        else{
            customviewNotes.txtView.text  = spekerDeatil.data.notes
        }
       
        
    }
    //MARK: -Tableview Delegate Methods
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return spekerDeatil.data.sessionlist.count + 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < 3{
            return UITableViewAutomaticDimension
        }
        else{
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < 3{
            let objSpeakerDetailCell = tableView.dequeueReusableCell(withIdentifier: "SpeakerDetailCell") as! SpeakerDetailCell
            
            //objSpeakerDetailCell.imgDetail.backgroundColor = colorset.themeColor
            
            objSpeakerDetailCell.lblDetail.font = UIFont(name: FONT.Regular, size: FontSizeFunc(size: 16.0))
            if indexPath.row == 0
            {
              if spekerDeatil.data.address == ""
              {
                objSpeakerDetailCell.imgDetail.image = UIImage(named: "")
              }
              else{
                objSpeakerDetailCell.imgDetail.image = UIImage(named: "loction")
                }
            
            objSpeakerDetailCell.lblDetail.text = spekerDeatil.data.address ?? ""
            }
            else if indexPath.row == 1
            {
              objSpeakerDetailCell.imgDetail.image = UIImage(named: "email_icon")
                 objSpeakerDetailCell.lblDetail.text = spekerDeatil.data.email ?? ""
            }else{
               objSpeakerDetailCell.imgDetail.image = UIImage(named: "phone_icon")
                objSpeakerDetailCell.lblDetail.text = spekerDeatil.data.mobileNo ?? ""
            }
            objSpeakerDetailCell.imgDetail.contentMode = .scaleAspectFit
                objSpeakerDetailCell.lblDetail.numberOfLines = 0
          
            objSpeakerDetailCell.lblDetail.textColor = colorset.lightBlackTextColor
            
            objSpeakerDetailCell.selectionStyle = .none
    
            return objSpeakerDetailCell
        }
        
        let objSpeakerSessionListCell = tableView.dequeueReusableCell(withIdentifier: "SpeakerSessionListCell") as! SpeakerSessionListCell
        
        objSpeakerSessionListCell.viewTopSep.backgroundColor = colorset.sepretorColor
        objSpeakerSessionListCell.viewBottomSep.backgroundColor = colorset.sepretorColor
        objSpeakerSessionListCell.viewBottomSep.isHidden = true
        
        objSpeakerSessionListCell.lblTitle.font = UIFont(name: FONT.Bold, size: FontSizeFunc(size: 18.0))
        objSpeakerSessionListCell.lblTitle.text = spekerDeatil.data.sessionlist[indexPath.row-3].name
        objSpeakerSessionListCell.lblTitle.textColor = colorset.lightBlackTextColor
         objSpeakerSessionListCell.imgSessionLogo.contentMode = .scaleAspectFit
        objSpeakerSessionListCell.imgSessionLogo.image = UIImage(named: "Speaker_icon")
        //objSpeakerSessionListCell.imgSessionLogo.backgroundColor = colorset.themeColor
        
        objSpeakerSessionListCell.selectionStyle = .none
        return objSpeakerSessionListCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row >= 3
        {
            let sessionDetailVC = StoryBoard.mainStoryBoard.instantiateViewController(withIdentifier: "SessionDetailViewController") as! SessionDetailViewController
            sessionDetailVC.strsessionID = spekerDeatil.data.sessionlist[indexPath.row-3].iD
            self.navigationController?.pushViewController(sessionDetailVC, animated: true)
        }
    }
    @IBAction func btnclickBookMark(_ sender: Any) {
        
        if spekerDeatil.data.speaker_liked == "0"
        {
            callsetBookmark(islike: "1")
        }
        else{
            callsetBookmark(islike: "0")
        }
        
        
    }
    func callsetBookmark(islike:String)
    {
        if getUserProfileDataList() == nil
        {
            self.popupAlert(title: AppName.appName, message: ValidationMsg.loginmsg, actionTitles: ["Ok"], actions:[{action1 in
                
                }, nil])
            
        }
        else{
           
        
            SpeakerModel.sharedModel().setBookmarkSpeaker(user_id:(getUserProfileDataList()?.iD)! , speaker_id: struserId, conference_id: AppDelegate.sharedInstance().conference.data[0].iD, is_like: islike, { (response) in
                if response.code == 1{
                    
                    if islike == "0"
                    {
                        self.spekerDeatil.data.speaker_liked = "0"
                        self.imgvBookmark.image =  UIImage(named: "imgBookMark")
                        
                    }
                    else {
                         self.spekerDeatil.data.speaker_liked = "1"
                        self.imgvBookmark.image = UIImage(named: "bookmarked_icon")
                        
                    }
                    
                }
                else{
                    self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                        
                        }, nil])
                }
                
            }) { (request, sender, json, error) in
                Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                    
                    switch selectedIndex {
                    case CONSTANTs.TWO: //for retry
                        
                        //Handle action
                        self.callsetBookmark(islike: islike)
                        
                        break
                        
                    default:
                        
                        // ignore case
                        
                        break
                    }
                    
                })
            }
        }
        
    }
    
    //MARK: - Add notes
   func  callAdnotes()
   {
    SpeakerModel.sharedModel().addnotesSpeaker(user_id: (getUserProfileDataList()?.iD)!, speaker_id: struserId, conference_id: AppDelegate.sharedInstance().conference.data[0].iD, note_id: spekerDeatil.data.note_id, note: customviewNotes.txtView.text ?? "", { (response) in
        if response.code == 1{
            
            self.customviewNotes.isHidden = true
            self.customviewNotes.txtView.resignFirstResponder()
            
        }
        else{
            self.popupAlert(title: AppName.appName, message: response.msg, actionTitles: ["Ok"], actions:[{action1 in
                
                }, nil])
        }
        
    }) { (request, sender, json, error) in
        Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
            
            switch selectedIndex {
            case CONSTANTs.TWO: //for retry
                
                //Handle action
                self.callAdnotes()
                
                break
                
            default:
                
                // ignore case
                
                break
            }
            
        })
    }
   
  
    }

    @IBAction func btnclickAddNotes(_ sender: Any) {
       customviewNotes.isHidden = false
    }
    //MARK: - Add notes
    @objc func btnclickCancel(_ sender:UIButton)
    {
        self.customviewNotes.isHidden = true
        self.customviewNotes.txtView.resignFirstResponder()
    }
    @objc func btnclickOkey(_ sender:UIButton)
    {
        if customviewNotes.txtView.text == "Add notes" || customviewNotes.txtView.text == ""
        {
            self.popupAlert(title: AppName.appName, message: ValidationMsg.addNotes, actionTitles: ["Ok"], actions:[{action1 in
                
                }, nil])
            
        }
        else
        {
            callAdnotes()
        }
        
        
      
        
    }
    //MARK:- textview delegate method
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add notes"
            textView.textColor = UIColor.lightGray
        }
    }
    
}

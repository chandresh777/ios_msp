//
//	RootClass.swift
//
//	Create by msp on 31/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SpeakerListRoot : NSObject{

	var code : Int!
	var data : [SpeakerListData]!
	var msg : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		code = json["code"].intValue
		data = [SpeakerListData]()
		let dataArray = json["data"].arrayValue
		for dataJson in dataArray{
			let value = SpeakerListData(fromJson: dataJson)
			data.append(value)
		}
		msg = json["msg"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	

}

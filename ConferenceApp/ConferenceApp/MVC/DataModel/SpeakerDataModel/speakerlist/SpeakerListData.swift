//
//	Data.swift
//
//	Create by msp on 31/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SpeakerListData : NSObject{

	var iD : String!
	var address : String!
	var catName : String!
	var categoryId : String!
	var email : String!
	var firstName : String!
	var gender : String!
	var lastName : String!
	var mobileNo : String!
	var profileImage : String!
	var specializeId : String!
	var specializeName : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		iD = json["ID"].stringValue
		address = json["address"].stringValue
		catName = json["cat_name"].stringValue
		categoryId = json["category_id"].stringValue
		email = json["email"].stringValue
		firstName = json["first_name"].stringValue
		gender = json["gender"].stringValue
		lastName = json["last_name"].stringValue
		mobileNo = json["mobile_no"].stringValue
		profileImage = json["profile_image"].stringValue
		specializeId = json["specialize_id"].stringValue
		specializeName = json["specialize_name"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	

}

//
//	Sessionlist.swift
//
//	Create by msp on 1/11/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SpeakerSessionlist : NSObject{

	var descriptionField : String!
	var name : String!
	var youtubeUrl : String!
    var iD : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		descriptionField = json["description"].stringValue
		name = json["name"].stringValue
		youtubeUrl = json["youtube_url"].stringValue
        iD = json["ID"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	

}

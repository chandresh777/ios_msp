//
//	Data.swift
//
//	Create by msp on 1/11/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SpeakerDetailData : NSObject{

	var iD : String!
	var address : String!
	var catName : String!
	var categoryId : String!
	var email : String!
	var firstName : String!
	var gender : String!
	var lastName : String!
	var mobileNo : String!
	var profileImage : String!
	var sessionlist : [SpeakerSessionlist]!
	var specializeId : String!
	var specializeName : String!
    var speaker_liked : String!
    var notes : String!
    var note_id : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		iD = json["ID"].stringValue
		address = json["address"].stringValue
		catName = json["cat_name"].stringValue
		categoryId = json["category_id"].stringValue
		email = json["email"].stringValue
		firstName = json["first_name"].stringValue
		gender = json["gender"].stringValue
		lastName = json["last_name"].stringValue
		mobileNo = json["mobile_no"].stringValue
		profileImage = json["profile_image"].stringValue
		sessionlist = [SpeakerSessionlist]()
		let sessionlistArray = json["sessionlist"].arrayValue
		for sessionlistJson in sessionlistArray{
			let value = SpeakerSessionlist(fromJson: sessionlistJson)
			sessionlist.append(value)
		}
		specializeId = json["specialize_id"].stringValue
		specializeName = json["specialize_name"].stringValue
        
        speaker_liked = json["speaker_liked"].stringValue
        notes = json["notes"].stringValue
        note_id = json["note_id"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	

}

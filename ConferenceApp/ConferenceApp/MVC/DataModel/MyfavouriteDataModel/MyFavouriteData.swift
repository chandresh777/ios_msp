//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class MyFavouriteData : NSObject{

	var sessionList : [MyfavouriteSessionList]!
	var speakersList : [MyfavouriteSpeakersList]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		sessionList = [MyfavouriteSessionList]()
		let sessionListArray = json["session_list"].arrayValue
		for sessionListJson in sessionListArray{
			let value = MyfavouriteSessionList(fromJson: sessionListJson)
			sessionList.append(value)
		}
		speakersList = [MyfavouriteSpeakersList]()
		let speakersListArray = json["speakers_list"].arrayValue
		for speakersListJson in speakersListArray{
			let value = MyfavouriteSpeakersList(fromJson: speakersListJson)
			speakersList.append(value)
		}
	}

	

}

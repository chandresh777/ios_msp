//
//	RootClass.swift
//
//	Create by msp on 30/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class ConferenceRoot : NSObject{

	var code : Int!
	var data : [ConferenceData]!
	var msg : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		code = json["code"].intValue
		data = [ConferenceData]()
		let dataArray = json["data"].arrayValue
		for dataJson in dataArray{
			let value = ConferenceData(fromJson: dataJson)
			data.append(value)
		}
		msg = json["msg"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	

}

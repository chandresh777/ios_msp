//
//	Data.swift
//
//	Create by msp on 29/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class ProfileCategoryData : NSObject, NSCoding{

	var iD : String!
	var catName : String!
	var createdAt : String!
	var isDoctor : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		iD = json["ID"].stringValue
		catName = json["cat_name"].stringValue
		createdAt = json["created_at"].stringValue
		isDoctor = json["is_doctor"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if iD != nil{
			dictionary["ID"] = iD
		}
		if catName != nil{
			dictionary["cat_name"] = catName
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if isDoctor != nil{
			dictionary["is_doctor"] = isDoctor
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         iD = aDecoder.decodeObject(forKey: "ID") as? String
         catName = aDecoder.decodeObject(forKey: "cat_name") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         isDoctor = aDecoder.decodeObject(forKey: "is_doctor") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if iD != nil{
			aCoder.encode(iD, forKey: "ID")
		}
		if catName != nil{
			aCoder.encode(catName, forKey: "cat_name")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if isDoctor != nil{
			aCoder.encode(isDoctor, forKey: "is_doctor")
		}

	}

}

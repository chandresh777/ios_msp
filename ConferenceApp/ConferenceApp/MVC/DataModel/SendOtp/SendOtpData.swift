//
//	RootClass.swift
//
//	Create by chandresh patel on 5/11/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SendOtpData : NSObject{

	var code : Int!
	var data : String!
	var msg : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		code = json["code"].intValue
		data = json["data"].stringValue
		msg = json["msg"].stringValue
	}


}

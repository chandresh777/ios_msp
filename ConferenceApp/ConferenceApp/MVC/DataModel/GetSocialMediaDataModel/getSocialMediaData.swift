//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class getSocialMediaData : NSObject, NSCoding{

	var fieldName : String!
	var fieldValue : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		fieldName = json["field_name"].stringValue
		fieldValue = json["field_value"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if fieldName != nil{
			dictionary["field_name"] = fieldName
		}
		if fieldValue != nil{
			dictionary["field_value"] = fieldValue
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         fieldName = aDecoder.decodeObject(forKey: "field_name") as? String
         fieldValue = aDecoder.decodeObject(forKey: "field_value") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if fieldName != nil{
			aCoder.encode(fieldName, forKey: "field_name")
		}
		if fieldValue != nil{
			aCoder.encode(fieldValue, forKey: "field_value")
		}

	}

}

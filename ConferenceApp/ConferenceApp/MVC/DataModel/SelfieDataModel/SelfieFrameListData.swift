//
//	SelfieFrameListData.swift
//
//	Create by msp on 12/11/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SelfieFrameListData : NSObject, NSCoding{

	var iD : String!
	var conferenceId : String!
	var frameImage : String!
	var thumbImg : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		iD = json["ID"].stringValue
		conferenceId = json["conference_id"].stringValue
		frameImage = json["frame_image"].stringValue
		thumbImg = json["thumb_img"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if iD != nil{
			dictionary["ID"] = iD
		}
		if conferenceId != nil{
			dictionary["conference_id"] = conferenceId
		}
		if frameImage != nil{
			dictionary["frame_image"] = frameImage
		}
		if thumbImg != nil{
			dictionary["thumb_img"] = thumbImg
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         iD = aDecoder.decodeObject(forKey: "ID") as? String
         conferenceId = aDecoder.decodeObject(forKey: "conference_id") as? String
         frameImage = aDecoder.decodeObject(forKey: "frame_image") as? String
         thumbImg = aDecoder.decodeObject(forKey: "thumb_img") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if iD != nil{
			aCoder.encode(iD, forKey: "ID")
		}
		if conferenceId != nil{
			aCoder.encode(conferenceId, forKey: "conference_id")
		}
		if frameImage != nil{
			aCoder.encode(frameImage, forKey: "frame_image")
		}
		if thumbImg != nil{
			aCoder.encode(thumbImg, forKey: "thumb_img")
		}

	}

}

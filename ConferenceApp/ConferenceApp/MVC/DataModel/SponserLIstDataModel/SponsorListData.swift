//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SponsorListData : NSObject{

	var gOLD : [SponsorListGOLD]!
	var pLATINIUM : [SponsorListGOLD]!
	var sILVER : [SponsorListGOLD]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		gOLD = [SponsorListGOLD]()
		let gOLDArray = json["GOLD"].arrayValue
		for gOLDJson in gOLDArray{
			let value = SponsorListGOLD(fromJson: gOLDJson)
			gOLD.append(value)
		}
		pLATINIUM = [SponsorListGOLD]()
		let pLATINIUMArray = json["PLATINIUM"].arrayValue
		for pLATINIUMJson in pLATINIUMArray{
			let value = SponsorListGOLD(fromJson: pLATINIUMJson)
			pLATINIUM.append(value)
		}
		sILVER = [SponsorListGOLD]()
		let sILVERArray = json["SILVER"].arrayValue
		for sILVERJson in sILVERArray{
			let value = SponsorListGOLD(fromJson: sILVERJson)
			sILVER.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	

}

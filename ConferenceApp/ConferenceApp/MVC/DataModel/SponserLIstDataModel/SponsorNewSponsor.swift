//
//	Sponsor.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SponsorNewSponsor : NSObject, NSCoding{

	var iD : String!
	var address : String!
	var catName : String!
	var categoryId : String!
	var categoryName : String!
	var email : String!
	var firstName : String!
	var gender : String!
	var height : String!
	var lastName : String!
	var mobileNo : String!
	var profileImage : String!
	var specializeId : String!
	var specializeName : String!
	var weight : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		iD = json["ID"].stringValue
		address = json["address"].stringValue
		catName = json["cat_name"].stringValue
		categoryId = json["category_id"].stringValue
		categoryName = json["category_name"].stringValue
		email = json["email"].stringValue
		firstName = json["first_name"].stringValue
		gender = json["gender"].stringValue
		height = json["height"].stringValue
		lastName = json["last_name"].stringValue
		mobileNo = json["mobile_no"].stringValue
		profileImage = json["profile_image"].stringValue
		specializeId = json["specialize_id"].stringValue
		specializeName = json["specialize_name"].stringValue
		weight = json["weight"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if iD != nil{
			dictionary["ID"] = iD
		}
		if address != nil{
			dictionary["address"] = address
		}
		if catName != nil{
			dictionary["cat_name"] = catName
		}
		if categoryId != nil{
			dictionary["category_id"] = categoryId
		}
		if categoryName != nil{
			dictionary["category_name"] = categoryName
		}
		if email != nil{
			dictionary["email"] = email
		}
		if firstName != nil{
			dictionary["first_name"] = firstName
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if height != nil{
			dictionary["height"] = height
		}
		if lastName != nil{
			dictionary["last_name"] = lastName
		}
		if mobileNo != nil{
			dictionary["mobile_no"] = mobileNo
		}
		if profileImage != nil{
			dictionary["profile_image"] = profileImage
		}
		if specializeId != nil{
			dictionary["specialize_id"] = specializeId
		}
		if specializeName != nil{
			dictionary["specialize_name"] = specializeName
		}
		if weight != nil{
			dictionary["weight"] = weight
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         iD = aDecoder.decodeObject(forKey: "ID") as? String
         address = aDecoder.decodeObject(forKey: "address") as? String
         catName = aDecoder.decodeObject(forKey: "cat_name") as? String
         categoryId = aDecoder.decodeObject(forKey: "category_id") as? String
         categoryName = aDecoder.decodeObject(forKey: "category_name") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         firstName = aDecoder.decodeObject(forKey: "first_name") as? String
         gender = aDecoder.decodeObject(forKey: "gender") as? String
         height = aDecoder.decodeObject(forKey: "height") as? String
         lastName = aDecoder.decodeObject(forKey: "last_name") as? String
         mobileNo = aDecoder.decodeObject(forKey: "mobile_no") as? String
         profileImage = aDecoder.decodeObject(forKey: "profile_image") as? String
         specializeId = aDecoder.decodeObject(forKey: "specialize_id") as? String
         specializeName = aDecoder.decodeObject(forKey: "specialize_name") as? String
         weight = aDecoder.decodeObject(forKey: "weight") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if iD != nil{
			aCoder.encode(iD, forKey: "ID")
		}
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if catName != nil{
			aCoder.encode(catName, forKey: "cat_name")
		}
		if categoryId != nil{
			aCoder.encode(categoryId, forKey: "category_id")
		}
		if categoryName != nil{
			aCoder.encode(categoryName, forKey: "category_name")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if firstName != nil{
			aCoder.encode(firstName, forKey: "first_name")
		}
		if gender != nil{
			aCoder.encode(gender, forKey: "gender")
		}
		if height != nil{
			aCoder.encode(height, forKey: "height")
		}
		if lastName != nil{
			aCoder.encode(lastName, forKey: "last_name")
		}
		if mobileNo != nil{
			aCoder.encode(mobileNo, forKey: "mobile_no")
		}
		if profileImage != nil{
			aCoder.encode(profileImage, forKey: "profile_image")
		}
		if specializeId != nil{
			aCoder.encode(specializeId, forKey: "specialize_id")
		}
		if specializeName != nil{
			aCoder.encode(specializeName, forKey: "specialize_name")
		}
		if weight != nil{
			aCoder.encode(weight, forKey: "weight")
		}

	}

}

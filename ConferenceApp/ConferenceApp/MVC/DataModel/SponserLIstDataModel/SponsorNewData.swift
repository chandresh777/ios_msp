//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SponsorNewData : NSObject, NSCoding{

	var category : String!
	var sponsors : [SponsorNewSponsor]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		category = json["category"].stringValue
		sponsors = [SponsorNewSponsor]()
		let sponsorsArray = json["sponsors"].arrayValue
		for sponsorsJson in sponsorsArray{
//            let value = SponsorNewData(fromJson: dataJson)
//            data.append(value)
            let value = SponsorNewSponsor(fromJson: sponsorsJson)
            sponsors.append(value)
//            sponsors.append((sponsorsJson.stringValue as AnyObject) as! SponsorNewSponsor)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if category != nil{
			dictionary["category"] = category
		}
		if sponsors != nil{
			dictionary["sponsors"] = sponsors
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         category = aDecoder.decodeObject(forKey: "category") as? String
         sponsors = aDecoder.decodeObject(forKey: "sponsors") as? [SponsorNewSponsor]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if category != nil{
			aCoder.encode(category, forKey: "category")
		}
		if sponsors != nil{
			aCoder.encode(sponsors, forKey: "sponsors")
		}

	}

}

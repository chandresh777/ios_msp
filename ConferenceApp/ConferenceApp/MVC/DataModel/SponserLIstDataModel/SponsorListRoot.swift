//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SponsorListRoot : NSObject{

	var code : Int!
	var data : SponsorListData!
	var msg : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		code = json["code"].intValue
		let dataJson = json["data"]
		if !dataJson.isEmpty{
			data = SponsorListData(fromJson: dataJson)
		}
		msg = json["msg"].stringValue
	}

	

}

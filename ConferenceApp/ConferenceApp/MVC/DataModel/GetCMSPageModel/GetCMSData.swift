//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class GetCMSData : NSObject, NSCoding{

	var aboutConference : String!
	var address : String!
	var appTips : String!
	var confrenceCommittee : String!
	var confrenceSecretariat : String!
	var dataProtection : String!
	var deadline : String!
	var disclaimer : String!
	var historyConference : String!
	var historyOrganisation : String!
	var legal : String!
	var localTips : String!
	var termsCondition : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		aboutConference = json["about_conference"].stringValue
		address = json["address"].stringValue
		appTips = json["app_tips"].stringValue
		confrenceCommittee = json["confrence_committee"].stringValue
		confrenceSecretariat = json["confrence_secretariat"].stringValue
		dataProtection = json["data_protection"].stringValue
		deadline = json["deadline"].stringValue
		disclaimer = json["disclaimer"].stringValue
		historyConference = json["history_conference"].stringValue
		historyOrganisation = json["history_organisation"].stringValue
		legal = json["legal"].stringValue
		localTips = json["local_tips"].stringValue
		termsCondition = json["terms_condition"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if aboutConference != nil{
			dictionary["about_conference"] = aboutConference
		}
		if address != nil{
			dictionary["address"] = address
		}
		if appTips != nil{
			dictionary["app_tips"] = appTips
		}
		if confrenceCommittee != nil{
			dictionary["confrence_committee"] = confrenceCommittee
		}
		if confrenceSecretariat != nil{
			dictionary["confrence_secretariat"] = confrenceSecretariat
		}
		if dataProtection != nil{
			dictionary["data_protection"] = dataProtection
		}
		if deadline != nil{
			dictionary["deadline"] = deadline
		}
		if disclaimer != nil{
			dictionary["disclaimer"] = disclaimer
		}
		if historyConference != nil{
			dictionary["history_conference"] = historyConference
		}
		if historyOrganisation != nil{
			dictionary["history_organisation"] = historyOrganisation
		}
		if legal != nil{
			dictionary["legal"] = legal
		}
		if localTips != nil{
			dictionary["local_tips"] = localTips
		}
		if termsCondition != nil{
			dictionary["terms_condition"] = termsCondition
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         aboutConference = aDecoder.decodeObject(forKey: "about_conference") as? String
         address = aDecoder.decodeObject(forKey: "address") as? String
         appTips = aDecoder.decodeObject(forKey: "app_tips") as? String
         confrenceCommittee = aDecoder.decodeObject(forKey: "confrence_committee") as? String
         confrenceSecretariat = aDecoder.decodeObject(forKey: "confrence_secretariat") as? String
         dataProtection = aDecoder.decodeObject(forKey: "data_protection") as? String
         deadline = aDecoder.decodeObject(forKey: "deadline") as? String
         disclaimer = aDecoder.decodeObject(forKey: "disclaimer") as? String
         historyConference = aDecoder.decodeObject(forKey: "history_conference") as? String
         historyOrganisation = aDecoder.decodeObject(forKey: "history_organisation") as? String
         legal = aDecoder.decodeObject(forKey: "legal") as? String
         localTips = aDecoder.decodeObject(forKey: "local_tips") as? String
         termsCondition = aDecoder.decodeObject(forKey: "terms_condition") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if aboutConference != nil{
			aCoder.encode(aboutConference, forKey: "about_conference")
		}
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if appTips != nil{
			aCoder.encode(appTips, forKey: "app_tips")
		}
		if confrenceCommittee != nil{
			aCoder.encode(confrenceCommittee, forKey: "confrence_committee")
		}
		if confrenceSecretariat != nil{
			aCoder.encode(confrenceSecretariat, forKey: "confrence_secretariat")
		}
		if dataProtection != nil{
			aCoder.encode(dataProtection, forKey: "data_protection")
		}
		if deadline != nil{
			aCoder.encode(deadline, forKey: "deadline")
		}
		if disclaimer != nil{
			aCoder.encode(disclaimer, forKey: "disclaimer")
		}
		if historyConference != nil{
			aCoder.encode(historyConference, forKey: "history_conference")
		}
		if historyOrganisation != nil{
			aCoder.encode(historyOrganisation, forKey: "history_organisation")
		}
		if legal != nil{
			aCoder.encode(legal, forKey: "legal")
		}
		if localTips != nil{
			aCoder.encode(localTips, forKey: "local_tips")
		}
		if termsCondition != nil{
			aCoder.encode(termsCondition, forKey: "terms_condition")
		}

	}

}

//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class EntertainmentRootClass : NSObject{

	var code : Int!
	var data : [EntertainmentData]!
	var msg : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		code = json["code"].intValue
		data = [EntertainmentData]()
		let dataArray = json["data"].arrayValue
		for dataJson in dataArray{
			let value = EntertainmentData(fromJson: dataJson)
			data.append(value)
		}
		msg = json["msg"].stringValue
	}


}

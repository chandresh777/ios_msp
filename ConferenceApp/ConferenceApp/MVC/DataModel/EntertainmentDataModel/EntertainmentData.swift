//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class EntertainmentData : NSObject{

	var date : String!
	var descriptionField : String!
	var time : String!
	var title : String!
	var venue : String!
    var image : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		date = json["date"].stringValue
		descriptionField = json["description"].stringValue
		time = json["time"].stringValue
		title = json["title"].stringValue
		venue = json["venue"].stringValue
        image = json["image"].stringValue


	}


}

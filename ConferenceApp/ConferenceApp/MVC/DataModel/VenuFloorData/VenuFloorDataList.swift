//
//	Data.swift
//
//	Create by chandresh patel on 13/11/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class VenuFloorDataList : NSObject{

	var dayAtGlance : String!
	var floorPlanOne : String!
	var floorPlanTwo : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		dayAtGlance = json["day_at_glance"].stringValue
		floorPlanOne = json["floor_plan_one"].stringValue
		floorPlanTwo = json["floor_plan_two"].stringValue
	}

}

//
//	Stepcalorie.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class DashboardStepcalorie : NSObject{

	var calorie : String!
	var steps : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		calorie = json["calorie"].stringValue
		steps = json["steps"].stringValue
	}

	

}

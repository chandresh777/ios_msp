//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class DashBoardRoot : NSObject{

	var code : Int!
	var data : DashboardData!
	var msg : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		code = json["code"].intValue
		let dataJson = json["data"]
		if !dataJson.isEmpty{
			data = DashboardData(fromJson: dataJson)
		}
		msg = json["msg"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	
}

//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class DashboardData : NSObject{

	var advertiseBanner : String!
	var redirectUrl : String!
	var stepcalorie : [DashboardStepcalorie]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		advertiseBanner = json["advertise_banner"].stringValue
		redirectUrl = json["redirect_url"].stringValue
		stepcalorie = [DashboardStepcalorie]()
		let stepcalorieArray = json["stepcalorie"].arrayValue
		for stepcalorieJson in stepcalorieArray{
			let value = DashboardStepcalorie(fromJson: stepcalorieJson)
			stepcalorie.append(value)
		}
	}

	
}

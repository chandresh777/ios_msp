//
//  SessionBookMarkRoot.swift
//  ConferenceApp
//
//  Created by msp on 31/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import Foundation
import SwiftyJSON
class SessionBookMarkRoot: NSObject {
    var code : Int!
  
    var msg : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        code = json["code"].intValue
       
        msg = json["msg"].stringValue
    }
}

//
//  ConferenceDateListRoot.swift
//  ConferenceApp
//
//  Created by msp on 31/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import Foundation
import SwiftyJSON
class ConferenceDateListRoot: NSObject {
    var code : Int!
    var data : sessionDateData!
    var msg : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        code = json["code"].intValue
        let dataJson = json["data"]
        if !dataJson.isEmpty{
            data = sessionDateData(fromJson: dataJson)
        }
        msg = json["msg"].stringValue
    }
}

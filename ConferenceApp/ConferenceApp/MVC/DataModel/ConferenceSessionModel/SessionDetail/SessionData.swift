//
//	Data.swift
//
//	Create by msp on 31/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SessionData : NSObject
{

	var iD : String!
	var conferenceId : String!
	var descriptionField : String!
	var endTime : String!
	var file : String!
	var hallName : String!
	var moderatorIds : String!
	var moderatorLists : [ModeratorList]!
	var name : String!
	var sessionDate : String!
	var speakerIds : String!
	var speakerLists : [ModeratorList]!
	var startTime : String!
	var youtubeUrl : String!
    var is_liked : String!
    var note_id : String!
    var notes : String!
    var thumb_youtube_img : String!
	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		iD = json["ID"].stringValue
		conferenceId = json["conference_id"].stringValue
		descriptionField = json["description"].stringValue
		endTime = json["end_time"].stringValue
		file = json["file"].stringValue
		hallName = json["hall_name"].stringValue
		moderatorIds = json["moderator_ids"].stringValue
		moderatorLists = [ModeratorList]()
		let moderatorListsArray = json["moderator_lists"].arrayValue
		for moderatorListsJson in moderatorListsArray{
			let value = ModeratorList(fromJson: moderatorListsJson)
			moderatorLists.append(value)
		}
		name = json["name"].stringValue
		sessionDate = json["session_date"].stringValue
		speakerIds = json["speaker_ids"].stringValue
		speakerLists = [ModeratorList]()
		let speakerListsArray = json["speaker_lists"].arrayValue
		for speakerListsJson in speakerListsArray{
			let value = ModeratorList(fromJson: speakerListsJson)
			speakerLists.append(value)
		}
		startTime = json["start_time"].stringValue
		
        is_liked = json["is_liked"].stringValue
        note_id = json["note_id"].stringValue
        notes = json["notes"].stringValue
        thumb_youtube_img = json["thumb_youtube_img"].stringValue
        youtubeUrl = json["youtube_url"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	

}

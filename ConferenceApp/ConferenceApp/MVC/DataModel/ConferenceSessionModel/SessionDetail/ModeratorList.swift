//
//	ModeratorList.swift
//
//	Create by msp on 31/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class ModeratorList : NSObject{

	var iD : String!
	var firstName : String!
	var lastName : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		iD = json["ID"].stringValue
		firstName = json["first_name"].stringValue
		lastName = json["last_name"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	

}

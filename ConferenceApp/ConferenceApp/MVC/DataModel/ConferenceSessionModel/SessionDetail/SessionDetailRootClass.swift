//
//	RootClass.swift
//
//	Create by msp on 31/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SessionDetailRootClass : NSObject{

	var code : Int!
	var data : SessionData!
	var msg : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		code = json["code"].intValue
		let dataJson = json["data"]
		if !dataJson.isEmpty{
			data = SessionData(fromJson: dataJson)
		}
		msg = json["msg"].stringValue
	}

	
	
}

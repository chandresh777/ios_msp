//
//  SessionAddNoteDataModel.swift
//  ConferenceApp
//
//  Created by msp on 01/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
import SwiftyJSON
class SessionAddNoteDataModel: NSObject {
    var code : Int!
  
    var msg : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        code = json["code"].intValue
       
        msg = json["msg"].stringValue
    }

}

//
//	Session.swift
//
//	Create by msp on 31/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Session : NSObject{

	var descriptionField : String!
	var endTime : String!
	var name : String!
	var sessionDate : String!
	var startTime : String!
    var iD:String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        iD = json["ID"].stringValue
		descriptionField = json["description"].stringValue
		endTime = json["end_time"].stringValue
		name = json["name"].stringValue
		sessionDate = json["session_date"].stringValue
		startTime = json["start_time"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	

}

//
//	Data.swift
//
//	Create by msp on 31/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class sessionListData : NSObject{

	var iD : String!
	var hallname : String!
	var session : [Session]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		iD = json["ID"].stringValue
		hallname = json["hallname"].stringValue
		session = [Session]()
		let sessionArray = json["session"].arrayValue
		for sessionJson in sessionArray{
			let value = Session(fromJson: sessionJson)
			session.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	

}

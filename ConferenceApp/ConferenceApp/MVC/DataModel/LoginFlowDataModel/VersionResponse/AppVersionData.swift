//
//	AppVersionData.swift
//
//	Create by msp on 29/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class AppVersionData : NSObject, NSCoding{

	var appConfig : AppVersionAppConfig!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		let appConfigJson = json["AppConfig"]
		if !appConfigJson.isEmpty{
			appConfig = AppVersionAppConfig(fromJson: appConfigJson)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if appConfig != nil{
			dictionary["AppConfig"] = appConfig.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         appConfig = aDecoder.decodeObject(forKey: "AppConfig") as? AppVersionAppConfig

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if appConfig != nil{
			aCoder.encode(appConfig, forKey: "AppConfig")
		}

	}

}

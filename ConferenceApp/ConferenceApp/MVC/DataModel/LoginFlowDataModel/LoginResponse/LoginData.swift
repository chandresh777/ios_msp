//
//	LoginData.swift
//
//	Create by msp on 29/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class LoginData : NSObject, NSCoding{

	var iD : String!
	var address : String!
	var catName : String!
	var categoryId : String!
	var email : String!
	var firstName : String!
	var gender : String!
	var isProfileFilled : String!
	var lastName : String!
	var mobileNo : String!
	var serverTime : String!
	var specializeId : String!
	var specializeName : String!
    var profileImage : String!
     var weight : String!
     var height : String!
    var selected_digital :String!
    
   


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		iD = json["ID"].stringValue
		address = json["address"].stringValue
        catName = json["cat_name"].stringValue
		categoryId = json["category_id"].stringValue
		email = json["email"].stringValue
		firstName = json["first_name"].stringValue
		gender = json["gender"].stringValue
		isProfileFilled = json["is_profile_filled"].stringValue
		lastName = json["last_name"].stringValue
		mobileNo = json["mobile_no"].stringValue
		serverTime = json["server_time"].stringValue
		specializeId = json["specialize_id"].stringValue
		specializeName = json["specialize_name"].stringValue
        profileImage = json["profile_image"].stringValue
        weight = json["weight"].stringValue
        height = json["height"].stringValue
         selected_digital = json["selected_digital"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if iD != nil{
			dictionary["ID"] = iD
		}
		if address != nil{
			dictionary["address"] = address
		}
		if catName != nil{
			dictionary["cat_name"] = catName
		}
		if categoryId != nil{
			dictionary["category_id"] = categoryId
		}
		if email != nil{
			dictionary["email"] = email
		}
		if firstName != nil{
			dictionary["first_name"] = firstName
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if isProfileFilled != nil{
			dictionary["is_profile_filled"] = isProfileFilled
		}
		if lastName != nil{
			dictionary["last_name"] = lastName
		}
		if mobileNo != nil{
			dictionary["mobile_no"] = mobileNo
		}
		if serverTime != nil{
			dictionary["server_time"] = serverTime
		}
		if specializeId != nil{
			dictionary["specialize_id"] = specializeId
		}
		if specializeName != nil{
			dictionary["specialize_name"] = specializeName
		}
        if profileImage != nil{
            dictionary["profile_image"] = profileImage
        }
        if weight != nil{
            dictionary["weight"] = weight
        }
        if height != nil{
            dictionary["height"] = height
        }
        
        if selected_digital != nil{
            dictionary["selected_digital"] = selected_digital
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         iD = aDecoder.decodeObject(forKey: "ID") as? String
         address = aDecoder.decodeObject(forKey: "address") as? String
         catName = aDecoder.decodeObject(forKey: "cat_name") as? String
         categoryId = aDecoder.decodeObject(forKey: "category_id") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         firstName = aDecoder.decodeObject(forKey: "first_name") as? String
         gender = aDecoder.decodeObject(forKey: "gender") as? String
         isProfileFilled = aDecoder.decodeObject(forKey: "is_profile_filled") as? String
         lastName = aDecoder.decodeObject(forKey: "last_name") as? String
         mobileNo = aDecoder.decodeObject(forKey: "mobile_no") as? String
         serverTime = aDecoder.decodeObject(forKey: "server_time") as? String
         specializeId = aDecoder.decodeObject(forKey: "specialize_id") as? String
         specializeName = aDecoder.decodeObject(forKey: "specialize_name") as? String
        profileImage = aDecoder.decodeObject(forKey: "profile_image") as? String
         weight = aDecoder.decodeObject(forKey: "weight") as? String
         height = aDecoder.decodeObject(forKey: "height") as? String
        selected_digital = aDecoder.decodeObject(forKey: "selected_digital") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if iD != nil{
			aCoder.encode(iD, forKey: "ID")
		}
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if catName != nil{
			aCoder.encode(catName, forKey: "cat_name")
		}
		if categoryId != nil{
			aCoder.encode(categoryId, forKey: "category_id")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if firstName != nil{
			aCoder.encode(firstName, forKey: "first_name")
		}
		if gender != nil{
			aCoder.encode(gender, forKey: "gender")
		}
		if isProfileFilled != nil{
			aCoder.encode(isProfileFilled, forKey: "is_profile_filled")
		}
		if lastName != nil{
			aCoder.encode(lastName, forKey: "last_name")
		}
		if mobileNo != nil{
			aCoder.encode(mobileNo, forKey: "mobile_no")
		}
		if serverTime != nil{
			aCoder.encode(serverTime, forKey: "server_time")
		}
		if specializeId != nil{
			aCoder.encode(specializeId, forKey: "specialize_id")
		}
		if specializeName != nil{
			aCoder.encode(specializeName, forKey: "specialize_name")
		}
        if profileImage != nil{
            aCoder.encode(profileImage, forKey: "profile_image")
        }
        if weight != nil{
            aCoder.encode(weight, forKey: "weight")
        }
        if height != nil{
            aCoder.encode(height, forKey: "height")
        }
        
        if selected_digital != nil{
            aCoder.encode(selected_digital, forKey: "selected_digital")
        }

	}

}

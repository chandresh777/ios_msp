//
//  ExSlideMenuController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 11/11/15.
//  Copyright © 2015 Yuji Hato. All rights reserved.
//

import UIKit

class ExSlideMenuController : SlideMenuController {

    override func isTagetViewController() -> Bool {
        if let vc = UIApplication.topViewController() {
            if vc is HomeVC ||
                vc is ConferenceDayViewController ||
                vc is PresenterListVC ||
                vc is AbstractViewController ||
                vc is ExhibitorsViewController ||
                vc is DayAtGlanceViewController ||
                vc is SocialAndNewsViewController ||
                vc is AttendeesViewController ||
                vc is MySelfieViewController ||
                vc is EntertainmentViewController ||
                vc is AboutConferenceViewController ||
                vc is QuizViewController ||
                vc is NotificationViewController ||
                vc is DigitalWalkViewController ||
                vc is CommitteMemberViewController ||
                vc is ProfileViewController
                {
                return true
                }
        }
        return false
    }
    
    override func track(_ trackAction: TrackAction) {
        switch trackAction {
        case .leftTapOpen:
            debugPrint("TrackAction: left tap open.")
        case .leftTapClose:
            debugPrint("TrackAction: left tap close.")
        case .leftFlickOpen:
            debugPrint("TrackAction: left flick open.")
        case .leftFlickClose:
            debugPrint("TrackAction: left flick close.")
        case .rightTapOpen:
            debugPrint("TrackAction: right tap open.")
        case .rightTapClose:
            debugPrint("TrackAction: right tap close.")
        case .rightFlickOpen:
            debugPrint("TrackAction: right flick open.")
        case .rightFlickClose:
            debugPrint("TrackAction: right flick close.")
        }   
    }
}

//
//  AppDelegate.swift
//  ConferenceApp
//
//  Created by msp on 25/10/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit
import UserNotifications
import CoreMotion
import Dispatch

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    private let activityManager = CMMotionActivityManager()
    private let pedometer = CMPedometer()
    private var shouldStartUpdating: Bool = false
    private var startDate: Date? = nil
    var caloriesBurned : Double = 0.0
    var strStepCount : String = "0"
    var strCaloriesBurn : String = "0.00"
    var window: UIWindow?
    static var appDelegate: AppDelegate? = nil
    var indexpathOfDrawerSel:IndexPath!

    var qrcodeval:String!
    var conference:ConferenceRoot!
    var strattachimgorNot:String!
    var sopnseorData:GetAdsSponserRootClass!
    var appfistTimeLogin = "no"
    var userId = ""
    
    var resultOfAppVersion : AppVersionRootClass? = nil

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // AppDelegate
        print("chandresh")
        
        // Override point for customization after application launch.
        
//        if getUserProfileDataList() != nil
//        {
//            Utility.moveToHomeViewCotroller()
//
//        }
//        else{
//           Utility.moveToLoginViewController()
//        }
  //  Utility.moveToHomeViewCotroller()
  
        for family in UIFont.familyNames {
            
            let sName: String = family as String
            print("family: \(sName)")
            
            for name in UIFont.fontNames(forFamilyName: sName) {
                print("name: \(name as String)")
            }
        }

        setDataInNSUser("2sfsdfrytjnnvbnvyfgjhjrrrjrtjytj" as AnyObject, key: UserDefaultKey.AppDevice_Token)
        
        registerForPushNotifications()

        indexpathOfDrawerSel = IndexPath()

//        callAppVersionAPI()
        self.didTapStartButton()

        Utility.moveToSplashViewController()
        guard let startDate = startDate else { return true }
        updateStepsCountLabelUsing(startDate: startDate)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

   
    //MARK:- Step count methods
    func didTapStartButton() {
        shouldStartUpdating = !shouldStartUpdating
        shouldStartUpdating ? (onStart()) : (onStop())
        
    }
    private func onStart() {
        startDate = Date()
        checkAuthorizationStatus()
        startUpdating()
    }
    
    private func onStop() {
        startDate = nil
        stopUpdating()
    }
    
    private func startUpdating() {
        //        if txtHeight.text == "" || txtWight.text == ""{
        //            lblBurnCalories.text = "Please enter your height and weight"
        //            return
        //        }
        
        if CMMotionActivityManager.isActivityAvailable() {
            //    self.caloriesBurned = 0.0
            //            self.lblCalrieVal.text = "0.00"
            //            self.lblStepCountVal.text = "0"
            //            startTrackingActivityType()
        } else {
            //            activityTypeLabel.text = "Not available"
        }
        
        if CMPedometer.isStepCountingAvailable() {
            startCountingSteps()
        } else {
            //            lblStepCountVal.text = "0"
        }
    }
    
    private func checkAuthorizationStatus() {
        if #available(iOS 11.0, *) {
            switch CMMotionActivityManager.authorizationStatus() {
            case CMAuthorizationStatus.denied:
                onStop()
            //            lblStepCountVal.text = "0"
            default:break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func stopUpdating() {
        //        txtWight.isUserInteractionEnabled = true
        //        txtHeight.isUserInteractionEnabled = true
        
       strCaloriesBurn = String(format: "%.2f", self.caloriesBurned)
        
        activityManager.stopActivityUpdates()
        pedometer.stopUpdates()
        pedometer.stopEventUpdates()
        NotificationCenter.default.post(name: Notification.Name("updateValueWalk"), object: nil)

    }
    
    private func on(error: Error) {
        //handle error
    }
    
    private func updateStepsCountLabelUsing(startDate: Date) {
        let date = Date()
        let cal = Calendar(identifier: .gregorian)
        let newDate = cal.startOfDay(for: date)
        pedometer.queryPedometerData(from: newDate, to: Date()) {
            [weak self] pedometerData, error in
            if let error = error {
                self?.on(error: error)
            } else if let pedometerData = pedometerData {
                DispatchQueue.main.async {
                    self?.strStepCount = String(describing: pedometerData.numberOfSteps)
                    NotificationCenter.default.post(name: Notification.Name("updateValueWalk"), object: nil)

                }
            }
        }
//        pedometer.startUpdates(from: newDate) { (data: CMPedometerData!, error) -> Void in
//            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                if(error == nil){
//                    self?.strStepCount = String(describing: pedometerData.numberOfSteps)
//                    NotificationCenter.default.post(name: Notification.Name("updateValueWalk"), object: nil)
//                }
//            })
//        }
    }
    
    private func startCountingSteps() {
        let date = Date()
        let cal = Calendar(identifier: .gregorian)
        let newDate = cal.startOfDay(for: date)
        pedometer.startUpdates(from: newDate) {
            [weak self] pedometerData, error in
            guard let pedometerData = pedometerData, error == nil else { return }
            
            DispatchQueue.main.async {
                let height = Double(6)
                let weight = Double(75)
                
                let caloriesBurnedPerMile : Double = 0.57 * (weight * 2.20462)
                let your_strip : Double = (30.48 * height) * 0.415
                let steps_in_1_mile : Double = 160934.4 / your_strip
                let conversationFactor : Double = caloriesBurnedPerMile / steps_in_1_mile
                self!.caloriesBurned = pedometerData.numberOfSteps.doubleValue * conversationFactor
                self?.strCaloriesBurn = String(format: "%.2f", self!.caloriesBurned) //"\(self!.caloriesBurned)"
                self?.strStepCount = pedometerData.numberOfSteps.stringValue
                NotificationCenter.default.post(name: Notification.Name("updateValueWalk"), object: nil)

            }
        }
    }
    //MARK: -Custom Access Methods
    class func sharedInstance() -> AppDelegate {
        if appDelegate == nil {
            appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        }
        return appDelegate!
    }


    //MARK: -API Call
    func callAppVersionAPI() {
        LoginFlowModel.sharedModel().callAppVersionAPI(device: "2", { (response) in
            if response.code == 1{
                self.resultOfAppVersion = response
                
                if getUserProfileDataList() != nil
                {
                    if getUserProfileDataList()?.isProfileFilled == "0" {
                        
                        Utility.moveToProfileController()
                    }else {
                        
                        Utility.moveToHomeViewCotroller()
                        
                    }
                }
                else{
                    Utility.moveToLoginViewController()
                }
                
            }
            //            }
            
        }) { (request, sender, json, error) in
            Utility.showAlertWithError(error: error! as NSError, selectedIndex: { (selectedIndex) in
                
                switch selectedIndex {
                case CONSTANTs.TWO: //for retry
                    
                    //Handle action
                    self.callAppVersionAPI()
                    
                    break
                    
                default:
                    
                    // ignore case
                    
                    break
                }
                
            })
        }
    }

}


extension AppDelegate {
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                [weak self] (granted, error) in
                debugPrint("Permission granted: \(granted)")
                
                guard granted else {
                    debugPrint("Please enable \"Notifications\" from App Settings.")
                    self?.showPermissionAlert()
                    return
                }
                
                self?.getNotificationSettings()
            }
        } else {
            let settings = UIUserNotificationSettings(types: [.alert, .sound, .badge], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    @available(iOS 10.0, *)
    func getNotificationSettings() {
        
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            debugPrint("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        debugPrint("Device Token: \(token)")
        setDataInNSUser(token as AnyObject, key: UserDefaultKey.AppDevice_Token)
        debugPrint(getDeviceToken() ?? "")
        //UserDefaults.standard.set(token, forKey: DEVICE_TOKEN)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        debugPrint("Failed to register: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        // If your app was running and in the foreground
        // Or
        // If your app was running or suspended in the background and the user brings it to the foreground by tapping the push notification
        
        debugPrint("didReceiveRemoteNotification /(userInfo)")
        
        guard let dict = userInfo["aps"]  as? [String: Any], let msg = dict ["alert"] as? String else {
            debugPrint("Notification Parsing Error")
            return
        }
    }
    
    func showPermissionAlert() {
        let alert = UIAlertController(title: "WARNING", message: "Please enable access to Notifications in the Settings app.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) {[weak self] (alertAction) in
            self?.gotoAppSettings()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    private func gotoAppSettings() {
        
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.openURL(settingsUrl)
        }
    }
    
    

}

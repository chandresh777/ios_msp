//
//  AlertViewClass.swift
//  MVCStruDemo
//
//  Created by msp on 27/02/18.
//  Copyright © 2018 msp. All rights reserved.
//
protocol alertprotocol {
    func printcall()
}
import UIKit

class AlertViewClass: UIViewController {
    var delegatecall:alertprotocol!
    var ActionBlockKey: UInt8 = 0
    static var alert : AlertViewClass? = nil
    
   
    
    class func shared() -> AlertViewClass
    {
        /*!
         *sharedModel of LaunchModel class.
         */
        if alert == nil
        {
            alert = AlertViewClass()
        }
        return alert!
    }
    
    func setSimpleAlertViewControllerShow(messageTitle:String , messageDetails: String , viewController:UIViewController ,complition:@escaping()->Void){
        let alert = UIAlertController (title: messageTitle, message: messageDetails, preferredStyle: .alert)
        let btnOK = UIAlertAction (title: "OK", style: UIAlertActionStyle.default){
            UIAlertAction in
            complition()
        }
        alert.addAction(btnOK)
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func alerViewSimpMessageShow(messageTitle:String, messageDetails:String,okButtonTitle:String, cancelButtonTitle:String, viewController:UIViewController,complition:@escaping()->Void){
        let alert = UIAlertController (title:messageTitle, message: messageDetails, preferredStyle:.alert)
        let okAction = UIAlertAction(title: okButtonTitle, style: UIAlertActionStyle.default) {
        UIAlertAction in
//        NSLog("OK Pressed")
            complition()
    }
        let cancelAction = UIAlertAction(title:cancelButtonTitle, style: UIAlertActionStyle.cancel) {
        UIAlertAction in
//        NSLog("Cancel Pressed")
    }
        alert.addAction(okAction)
        alert.addAction(cancelAction)

        viewController.present(alert, animated: true, completion: nil)
    }
    

    
    
    @objc func btnOKClick(){
        
        delegatecall.printcall()
    }
    
    // Shows alert with yes no button
   func showAlert(title: String, msg: String, vc: UIViewController, positiveActionHandler: ((UIAlertAction) -> Swift.Void)?, negativeActionHandler: ((UIAlertAction) -> Swift.Void)?) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let positiveAction = UIAlertAction(title: "Ok", style: .destructive, handler: positiveActionHandler)
        alertController.addAction(positiveAction)

        let negativeAction = UIAlertAction(title: "Cancel", style: .cancel, handler: negativeActionHandler)
        alertController.addAction(negativeAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
}

extension UIButton{
    private func actionHandleBlock(action:(() -> Void)? = nil) {
        struct __ {
            static var action :(() -> Void)?
        }
        if action != nil {
            __.action = action
        } else {
            __.action?()
        }
    }
    private func actionHandleBlockCancel(action:(() -> Void)? = nil) {
        struct __ {
            static var action :(() -> Void)?
        }
        if action != nil {
            __.action = action
        } else {
            __.action?()
        }
    }
    @objc private func triggerActionHandleBlock() {
        self.actionHandleBlock()
    }
    @objc private func triggerActionHandleBlockCancel() {
        self.actionHandleBlockCancel()
    }
    
    func actionHandle(controlEvents control :UIControlEvents, ForAction action:@escaping () -> Void) {
        self.actionHandleBlock(action: action)
        self.addTarget(self, action:#selector(triggerActionHandleBlock), for: control)
    }
    func actionHandleCancel(controlEvents control :UIControlEvents, ForAction action:@escaping () -> Void) {
        self.actionHandleBlockCancel(action: action)
        self.addTarget(self, action:#selector(triggerActionHandleBlockCancel), for: control)
    }
}

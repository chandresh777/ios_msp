//
//  CustomViewAddNotes.swift
//  ConferenceApp
//
//  Created by msp on 01/11/18.
//  Copyright © 2018 msp. All rights reserved.
//

import UIKit

class CustomViewAddNotes: UIView {
    @IBOutlet weak var lblAlertTitle: UILabel!
    @IBOutlet weak var lblAlertMessage: UILabel!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOkey: UIButton!
    @IBOutlet weak var viewAlert:UIView!
    @IBOutlet weak var viewBleare:UIView!
    @IBOutlet weak var viewOfButton:UIView!
    
   
    @IBOutlet weak var viewTxtFieldSep: UIView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func draw(_ rect: CGRect) {
        
        lblAlertTitle.font = UIFont(name: FONT.Bold, size: 20.0)
        lblAlertTitle.textColor = colorset.blacktextcolor
        
        
        txtView.textColor = colorset.lightBlackTextColor
       txtView.layer.borderWidth = 1.0
        txtView.layer.borderColor = colorset.sepretorColor.cgColor
        txtView.layer.cornerRadius = 5.0
        txtView.layer.masksToBounds = true
        
        // UIFontDescriptor * fontDescriptor = [self.textField2.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold];
        // UIFont *font = [UIFont fontWithDescriptor:fontDescriptor size:self.textField2.font.pointSize];
        
       
        
        // UIFontDescriptor * fontDescriptor = [self.textField2.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold];
        // UIFont *font = [UIFont fontWithDescriptor:fontDescriptor size:self.textField2.font.pointSize];
        
        btnOkey.backgroundColor = colorset.drawerBackground
        btnOkey.setTitleColor(UIColor.white, for: .normal)
        btnOkey.titleLabel?.font = UIFont(name: FONT.Bold, size: 16.0)
        //  btnOkey.setTitle("Submit", for: .normal)
        btnOkey.layer.cornerRadius = 4.0
        btnOkey.clipsToBounds = true
        
        btnCancel.backgroundColor = UIColor.clear
        btnCancel.setTitleColor(colorset.drawerBackground, for: .normal)
        btnCancel.titleLabel?.font = UIFont(name: FONT.Bold, size: 16.0)
        //   btnCancel.setTitle("Cancel", for: .normal)
        btnCancel.layer.cornerRadius = 4.0
        btnCancel.clipsToBounds = true
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = colorset.drawerBackground.cgColor
        
        
        // viewTxtFieldSep.backgroundColor = colorset.sepretorColor
        
        
    }

}
